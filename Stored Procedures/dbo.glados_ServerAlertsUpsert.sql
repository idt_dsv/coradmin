SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[glados_ServerAlertsUpsert]
	@Provider nvarchar(128),
	@Name nvarchar(128),
	@FailDateTime datetime = null,
	@ErrorInfo nvarchar(max) = null,
	@ErrorLevel char(4) = 'INFO'
 as
 begin
	if @FailDateTime is null
		select @FailDateTime = getdate()

	if exists (select 1 from dbo.glados_ServerAlerts where [Name] = @Name)
	begin
		update dbo.glados_ServerAlerts
		set LastFailure = @FailDateTime,
			ErrorInfo = @ErrorInfo,
			IsCurrent = 1,
			DateUpdated = current_timestamp,
			ErrorLevel = @ErrorLevel
		where Provider = @Provider
		and [Name] = @Name;
	end
	else
	begin
		insert into dbo.glados_ServerAlerts (Provider, Name, FirstFailure, LastFailure, ErrorInfo, ErrorLevel, IsCurrent)
		values (@Provider, @Name, @FailDateTime, @FailDateTime, @ErrorInfo, @ErrorLevel, 1);			
	end
end
	


GO
GRANT EXECUTE ON  [dbo].[glados_ServerAlertsUpsert] TO [IDT-CORALVILLE\srv_PRODSQL3]
GO
