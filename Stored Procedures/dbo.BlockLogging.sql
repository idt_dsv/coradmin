SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create proc [dbo].[BlockLogging]
	@WaitTimeThreshold int = 250 -- default 250 miliseconds
as

set nocount on

/****************************************************************************
Created by: Andrew Gould
Created On: 8/24/2011

Purpose:
	This proc trolls sysprocesses looking for processes that have been
	blocked for longer than the passed waittime threshold.

Objects:
	master.sys.sysprocesses
	[Admin].dbo.BlockLog

****************************************************************************/

-----------------------------------------------------------------------------
-- Create temporary holding table.
-----------------------------------------------------------------------------
create table #BlockLog
	(spid int
	, hostname varchar(500)
	, loginame varchar(500)
	, program_name varchar(500)
	, database_name varchar(500)
	, blocked int
	, waittime bigint
	, lastwaittype varchar(100)
	, waitresource varchar(100)
	, open_tran int
	, physical_io int
	, memusage int
	, login_time datetime
	, last_batch datetime
	, sql_handle binary(20)
	, SQL_COMMAND text
	, BLOCKER_spid int
	, BLOCKER_hostname varchar(500)
	, BLOCKER_loginame varchar(500)
	, BLOCKER_program_name varchar(500)
	, BLOCKER_database_name varchar(500)
	, BLOCKER_blocked int
	, BLOCKER_waittime bigint
	, BLOCKER_lastwaittype varchar(100)
	, BLOCKER_waitresource varchar(100)
	, BLOCKER_open_tran int
	, BLOCKER_physical_io int
	, BLOCKER_memusage int
	, BLOCKER_login_time datetime
	, BLOCKER_last_batch datetime
	, BLOCKER_sql_handle binary(20)
	, BLOCKER_SQL_COMMAND text)

-----------------------------------------------------------------------------
-- Load any processes that are blocked and have been waiting longer than
-- the provided threshold.
-----------------------------------------------------------------------------
insert into #BlockLog
	(spid
	, hostname
	, loginame
	, program_name
	, database_name
	, blocked
	, waittime
	, lastwaittype
	, waitresource
	, open_tran
	, physical_io
	, memusage
	, login_time
	, last_batch
	, sql_handle
	, BLOCKER_spid
	, BLOCKER_hostname
	, BLOCKER_loginame
	, BLOCKER_program_name
	, BLOCKER_database_name
	, BLOCKER_blocked
	, BLOCKER_waittime
	, BLOCKER_lastwaittype
	, BLOCKER_waitresource
	, BLOCKER_open_tran
	, BLOCKER_physical_io
	, BLOCKER_memusage
	, BLOCKER_login_time
	, BLOCKER_last_batch
	, BLOCKER_sql_handle)
select Blocked.spid
	, Blocked.hostname
	, Blocked.loginame
	, Blocked.program_name
	, db_name(Blocked.dbid) as database_name
	, Blocked.blocked
	, Blocked.waittime
	, Blocked.lastwaittype
	, Blocked.waitresource
	, Blocked.open_tran
	, Blocked.physical_io
	, Blocked.memusage
	, Blocked.login_time
	, Blocked.last_batch
	, Blocked.sql_handle
	, Blocker.spid as BLOCKER_spid
	, Blocker.hostname as BLOCKER_hostname
	, Blocker.loginame as BLOCKER_loginame
	, Blocker.program_name as BLOCKER_program_name
	, db_name(Blocker.dbid) as BLOCKER_database_name
	, Blocker.blocked as BLOCKER_blocked
	, Blocker.waittime as BLOCKER_waittime
	, Blocker.lastwaittype as BLOCKER_lastwaittype
	, Blocker.waitresource as BLOCKER_waitresource
	, Blocker.open_tran as BLOCKER_open_tran
	, Blocker.physical_io as BLOCKER_physical_io
	, Blocker.memusage as BLOCKER_memusage
	, Blocker.login_time as BLOCKER_login_time
	, Blocker.last_batch as BLOCKER_last_batch
	, Blocker.sql_handle as BLOCKER_sql_handle
from master.sys.sysprocesses as Blocked with (nolock)
inner join master.sys.sysprocesses as Blocker with (nolock)
	on Blocked.blocked = Blocker.spid
where Blocked.waittime >= @WaitTimeThreshold

-----------------------------------------------------------------------------
-- Update SQL Text for BLOCKED command.
-----------------------------------------------------------------------------
update #BlockLog
set SQL_COMMAND = T.text
from #BlockLog as BL
cross apply master.sys.dm_exec_sql_text(BL.sql_handle) as T

-----------------------------------------------------------------------------
-- Update SQL Text for BLOCKER command.
-----------------------------------------------------------------------------
update #BlockLog
set BLOCKER_SQL_COMMAND = T.text
from #BlockLog as BL
cross apply master.sys.dm_exec_sql_text(BL.BLOCKER_sql_handle) as T

-----------------------------------------------------------------------------
-- Load final record set into permanent logging table.
-----------------------------------------------------------------------------
insert into [Admin].dbo.BlockLog
	(spid
	, hostname
	, loginame
	, program_name
	, database_name
	, blocked
	, waittime
	, lastwaittype
	, waitresource
	, open_tran
	, physical_io
	, memusage
	, login_time
	, last_batch
	, sql_handle
	, SQL_COMMAND
	, BLOCKER_spid
	, BLOCKER_hostname
	, BLOCKER_loginame
	, BLOCKER_program_name
	, BLOCKER_database_name
	, BLOCKER_blocked
	, BLOCKER_waittime
	, BLOCKER_lastwaittype
	, BLOCKER_waitresource
	, BLOCKER_open_tran
	, BLOCKER_physical_io
	, BLOCKER_memusage
	, BLOCKER_login_time
	, BLOCKER_last_batch
	, BLOCKER_sql_handle
	, BLOCKER_SQL_COMMAND
	, WaitTimeThreshold)
select spid
	, hostname
	, loginame
	, program_name
	, database_name
	, blocked
	, waittime
	, lastwaittype
	, waitresource
	, open_tran
	, physical_io
	, memusage
	, login_time
	, last_batch
	, sql_handle
	, SQL_COMMAND
	, BLOCKER_spid
	, BLOCKER_hostname
	, BLOCKER_loginame
	, BLOCKER_program_name
	, BLOCKER_database_name
	, BLOCKER_blocked
	, BLOCKER_waittime
	, BLOCKER_lastwaittype
	, BLOCKER_waitresource
	, BLOCKER_open_tran
	, BLOCKER_physical_io
	, BLOCKER_memusage
	, BLOCKER_login_time
	, BLOCKER_last_batch
	, BLOCKER_sql_handle
	, BLOCKER_SQL_COMMAND
	, @WaitTimeThreshold
from #BlockLog

-----------------------------------------------------------------------------
-- Clean up.
-----------------------------------------------------------------------------
drop table #BlockLog
-----------------------------------------------------------------------------
-----------------------------------------------------------------------------
GO
