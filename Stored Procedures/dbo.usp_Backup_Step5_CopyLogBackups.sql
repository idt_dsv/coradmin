SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[usp_Backup_Step5_CopyLogBackups]
	@Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 10/23/2009

Purpose: 
	Copies the backup file its remote host and renames the file appending the
	backup date to it.
	
Revisions:
	10/27/2009 - Andrew Gould
		Added transaction log copy to process.
		Added better error handling.
	10/30/2009 - Andrew Gould
		Removed XCopy and Rename commands in favor of ESEUtil due to ESEUtil's
		dramaticaly improved file copy speeds.
	12/09/2009 - Andrew Gould
		Updated copy process to first ping the remote host.  If it can't be
		reached load the commands into the PendingCommand table to be
		executed later.
	12/14/2009 - Andrwe Gould
		Add check for "File Exists" error.  If the file already exists remove
		it from the PendingCommand and move on with out throwing an error.
	04/28/2011 - Travis Tittle
		Added Double Brackets to ensure entire Database Name is obtained in build
		Added a check in Log Backup Copy, where it also checks for newly added
		Full Backup files.
		Added step to copy Differential backups, along with new Full Backups.
		Added step in copy where if the file isn't successfully copied to nearstore,
		it is thrown in the Pending Table and attempted again and again.  If the file
		is tried over 3 times, an ErrorLog entry occurs.  Cuts down on minor connection
		issues.
	06/16/2011 - Travis Tittle
		Changed the way files are copied to Nearstore from ESEUtil to XCOPY.  This removed
		a large portion of this step.
	06/23/2011 - Travis Tittle
		Incorporated GLaDOS Alerting.
	11/21/2011 - Travis Tittle
		XCOPY change to add /d and remove /m switch.  This will remove the archive attribute portion.
		Changed XCOPY backup strategy to only backup files associated with BackupType.  If new backup 
		files are created, process will kick off separate unscheduled job to move files.
	12/13/2011 - Travis Tittle
		Added ReadOnly option.	
	02/24/2012 - Travis Tittle
		Copy portion of the Backup Solution has been decoupled, and is now its own process/job.	
	04/03/2012 - Travis Tittle
		Removed XCopy as the copy process of choice, and have switched to RoboCopy.	
	02/06/2014 - Travis Tittle
		Removed Secondary Path call, as was not used and not well documented.	
	07/17/2014 - John Wood Added commands to copy .snk and .SQL 
To Do:
	None
	
Notes:

   
   ROBOCOPY Switches used in code, and what they do:
      /E - Copy Subfolders, including Empty Subfolders.
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp tables.
--------------------------------------------------------------------------------
	
create table #FileCopy
	(FileCopyID int identity(1,1)
	, RoboCopyCommand varchar(1000))
	
create table #CommandOutput
	(OutputText varchar(1000),
	 Command varchar(500))

--------------------------------------------------------------------------------
-- Declare variables.
--------------------------------------------------------------------------------
declare @Subject2 varchar(100)
   , @LocalPath varchar(100)
    , @RemotePath varchar(100)
    , @Start int
	, @End int
	, @CopyCommand varchar(500)
	, @FailDateTime datetime
    , @Provider varchar(50)
	
declare @Today datetime
select @Today = getdate()

select @FailDateTime = GETDATE()
select @Provider = 'BackupProvider'

select @Subject2 = 'Unable to Copy Files to the Remote Backup Location'
	
select @LocalPath = BackupPath
from ADMIN.dbo.Backup_Paths with (nolock)
where Location = 'Local'
	and IsDefault = 1

select @RemotePath = BackupPath
from ADMIN.dbo.Backup_Paths with (nolock)
where Location = 'Remote'
	and IsDefault = 1

--------------------------------------------------------------------------------
-- Build copy commands and walk through #FileCopy, attempting to copy all new backups to a remote host.
--------------------------------------------------------------------------------


	insert into #FileCopy
		(RoboCopyCommand)
	select 'xp_cmdshell '
			+ '''c:\windows\system32\ROBOCOPY.exe '
			+ @LocalPath
			+ ' '
			+ @RemotePath
			+ ' *.trn' 
			+ ' /E'''
	
	insert into #FileCopy
		(RoboCopyCommand)
	select 'xp_cmdshell '
			+ '''c:\windows\system32\ROBOCOPY.exe '
			+ @LocalPath
			+ ' '
			+ @RemotePath
			+ ' *.snk' 
			+ ' /E /purge '''
	
	insert into #FileCopy
		(RoboCopyCommand)
	select 'xp_cmdshell '
			+ '''c:\windows\system32\ROBOCOPY.exe '
			+ @LocalPath
			+ ' '
			+ @RemotePath
			+ ' *.sql' 
			+ ' /E /purge'''


-------------------------------------------------------------------------------
select @Start = min(FileCopyID) from #FileCopy with (nolock)
select @End = max(FileCopyID) from #FileCopy with (nolock)

	while @Start <= @End
 begin
		select @CopyCommand = RoboCopyCommand
		from #FileCopy with (nolock)
		where FileCopyID = @Start
		
		insert into #CommandOutput
			(OutputText)
		exec (@CopyCommand)
		
select @Start = @Start + 1

 end      
--------------------------------------------------------------------------------
-- If Operation was unsuccessful, then email DataServices.
--------------------------------------------------------------------------------
if (select count(*)	from #CommandOutput 
	where OutputText like '%File not Found%'
	or OutputText like '%Invalid drive specification%') >= 1
begin
    if exists (select * from Admin.dbo.glados_ServerAlerts where [ErrorInfo] = @Subject2)
    begin
       update Admin.dbo.glados_ServerAlerts
       set LastFailure = @FailDateTime,
           DateUpdated = @FailDateTime
       where Provider = @Provider
       and ErrorInfo = @Subject2
    end
    else 
    begin
       insert into dbo.glados_ServerAlerts
       (Provider, 
        Name, 
        FirstFailure, 
        LastFailure, 
        ErrorInfo, 
        ErrorLevel, 
        IsCurrent, 
        DateCreated, 
        DateUpdated)
       values
       (@Provider,
        'The RoboCopy Log process failed.  Next attempt in 15 minutes',
        @FailDateTime,
        @FailDateTime,
        @Subject2,
        'WARN',
        1,
        @FailDateTime,
        @FailDateTime);
             
    end
end
else
begin
delete dbo.glados_ServerAlerts
	output 
		deleted.ServerAlertID, 		
		deleted.provider,
		deleted.Name,
		deleted.FirstFailure,
		deleted.LastFailure,
		deleted.ErrorInfo,
		deleted.ErrorLevel,
		deleted.IsCurrent,
		deleted.DateCreated,
		deleted.DateUpdated
	into dbo.glados_ServerAlertsArchive
	from dbo.glados_ServerAlerts al
	where al.Provider= @Provider
	and al.ErrorInfo = @Subject2;
		
	print 'The RoboCOPY File Transfer was successful'
end		
--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #FileCopy
drop table #CommandOutput
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------		

GO
