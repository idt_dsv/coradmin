SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE proc [dbo].[usp_Backup_Step3_BackupReadOnly]
	@Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 12/13/2011

Purpose: 
	Backup each read only database to its nearstore only.
	
Revisions:
		05-03-12 - Added the Make Remote Read Only Directory part of the process to this step to avoid remote location					   being down bringing down local backups.
	02/06/2014 - Travis Tittle
		Removed Compression flag, as New Build Doc requires Compression already be set in sys.configurations.  Also fixed issue with missing criteria to backup RO database.
To Do:
	None
	
Notes:
	For a read only database to be listed to be backed up the following 3 conditions
	must be met.
	1.) It must not be flagged to be backed up as part of full backup schedule
	 (DontBackup = 1)
	2.) It must be ONLINE.
	3.) It must have a ReadOnlyBackupFile.
	4.) It must be part of BackupGroup 2.
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp table.
--------------------------------------------------------------------------------

create table #LastKnownROBackups
	(DatabaseID int identity(1,1),
	  DatabaseName		varchar(500)
	, LastFullBackup	datetime
	, BackupFile		varchar(500)
	, BackupPath		varchar(max))
	
create table #MD
	(MDID int identity(1,1)
	, MDCommand varchar(500))

create table #Directories
	(DirID int identity(1,1)
	, DirCommand varchar(1000)
	, FileLocation varchar(100))

create table #ROBackupsOnDisk
	(FileID int identity(1,1)
	, FileName varchar(max))

create table #ROBackupReset
	(DatabaseID int identity(1,1)
	, DatabaseName varchar(500))

create table #Backup
	(BackupID int identity(1,1)
	, BackupType varchar(10)
	, DatabaseID int
	, BackupCommand varchar(500)
	, VerificationCommand varchar(500))
--------------------------------------------------------------------------------
-- Declare Variables.
--------------------------------------------------------------------------------
declare @Start int  --
	, @End int  --
	, @Start2 int  --
	, @End2 int  --
	, @Start3 int  --
	, @End3 int  --
	, @DatabaseID int  --
	, @MDCommand varchar(1000)   --
	, @BackupCommand varchar(500)  --
	, @DirCommand varchar(500)  --
	, @VerificationCommand varchar(500)  --
	, @BackupError int
	, @VerificationError int
	, @Today datetime

select @BackupError = -1
select @VerificationError = -1
select @Today = getdate()

------------------------------------------------------------------------------
--Build a list of create READ ONLY directory tree commands
------------------------------------------------------------------------------

insert into #MD
	(MDCommand)
select 'xp_cmdshell ''md "' + P.BackupPath + DB.DatabaseName + '"'''
from ADMIN.dbo.Backup_Databases as DB with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on DB.ReadOnlyPathID = P.PathID
where DontBackup = 1
AND BackupGroup = 2

--------------------------------------------------------------------------------
-- Walk through #MD executing the commands.
--------------------------------------------------------------------------------	
select @Start = min(MDID) from #MD with (nolock)
select @End = max(MDID) from #MD with (nolock)

while @Start <= @End
begin
	select @MDCommand = MDCommand
	from #MD with (nolock)
	where MDID = @Start
	
	exec(@MDCommand)
	
	select @Start = @Start + 1
end

--------------------------------------------------------------------------------
--Get Last Known Backups for RO databases From MSDB
--------------------------------------------------------------------------------
insert into #LastKnownROBackups
	(DatabaseName
	, LastFullBackup)
select BS.database_name
	, max(BS.backup_start_date) as LastFullBackup
from master.sys.databases as D with (nolock)
inner join msdb.dbo.backupset as BS with (nolock)
	on D.name = BS.database_name
	and D.recovery_model_desc = BS.recovery_model collate SQL_Latin1_General_CP1_CI_AS
where D.state_desc = 'ONLINE'
	and D.is_in_standby = 0 -- Not in Standby
	and D.is_read_only = 1
	and BS.is_copy_only = 0
	and BS.type = 'D'
	and BS.backup_finish_date is not null
group by BS.database_name
	, BS.recovery_model
	
update #LastKnownROBackups
set BackupPath = BMF.physical_device_name
from #LastKnownROBackups as L with (nolock)
inner join msdb.dbo.backupset as BS with (nolock)
	on L.DatabaseName = BS.database_name
	and L.LastFullBackup = BS.backup_start_date
inner join msdb.dbo.backupmediafamily as BMF with (nolock)
	on BS.media_set_id = BMF.media_set_id
	
update #LastKnownROBackups
set BackupFile = right(BackupPath,charindex('\',reverse(BackupPath))-1)
where charindex('\',BackupPath) > 0


---------------------------------------------------
--Get current RO Backups on Disk
---------------------------------------------------

--------------------------------------------------------------------------------
-- Build dir commands for each backup path.
-- Note: It will only look through paths that aren't listed as Archived.
-- DIR Switches:
--	/N		- New long list format where filenames are on the far right.
--	/O-D	- List by files in sorted order by date.
--	/S		- Displays files in specified directory and all subdirectories.
--	/A-d	- Don't include any directories in the output.
--	/B		- Uses bare format (no heading information or summary).
--------------------------------------------------------------------------------
insert into #Directories
	(DirCommand,
	 FileLocation)
select 'xp_cmdshell ''dir /N /O-D /S /A-d /B "' + BackupPath + '"''',
      Location
from ADMIN.dbo.Backup_Paths with (nolock)
where Location = 'ReadOnly'

--------------------------------------------------------------------------------
-- Walk through #Directories, executing the dir commands.
--------------------------------------------------------------------------------	
select @Start2 = min(DirID) from #Directories with (nolock)
select @End2 = max(DirID) from #Directories with (nolock)

while @Start2 <= @End2
begin
	select @DirCommand = DirCommand
	from #Directories with (nolock)
	where DirID = @Start2
	
	insert into #ROBackupsOnDisk
		(FileName)
	exec (@DirCommand)
	
	select @Start2 = @Start2 + 1
end

----------------------------------------------
-- Delete Null Records, and any other files not considered a backup file.
----------------------------------------------
delete #ROBackupsOnDisk
where FileName IS NULL
----------------------------------------------
delete from #ROBackupsOnDisk
where FileName NOT LIKE '%.bak'

----------------------------------------------
-- Flag databases for new RO Backup if one is needed.
----------------------------------------------
insert into #ROBackupReset
   (DatabaseName)
select lkrb.DatabaseName
from #LastKnownROBackups as lkrb
left outer join #ROBackupsOnDisk as rbod
   on rbod.filename = lkrb.backuppath
where rbod.filename is null

update Admin.dbo.Backup_Databases
   set ReadOnlyBackupDate = '1900-01-01 00:00:00.000',
       ReadOnlyBackupFile = '-1'
from Admin.dbo.Backup_Databases as bd
join #ROBackupReset as robr
   on robr.DatabaseName = bd.DatabaseName

--------------------------------------------------------------------------------
-- Build out any new RO Backup command
--------------------------------------------------------------------------------
update ADMIN.dbo.Backup_Databases
set ReadOnlyBackupFile = P.BackupPath
		+ D.FolderName 
		+ '\'
		+ D.DatabaseName
		+ '_'
		-- This jumbled mess takes a date and trims of the seconds and miliseconds and removes all formating.
		+ left(replace(replace(replace(convert(varchar,@Today,121),' ',''),':',''),'-',''),12) 
		+ '.bak'
from ADMIN.dbo.Backup_Databases as D with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on D.ReadOnlyPathID = P.PathID
where D.DontBackup = 1
and D.Database_Status = 'ONLINE'
and D.BackupGroup = 2
and D.ReadOnlyBackupFile = '-1'
and D.ReadOnlyBackupDate = '1900-01-01 00:00:00.000'

insert into #Backup
	(BackupType
	, DatabaseID
	, BackupCommand
	, VerificationCommand)
select 'ReadOnly'
    , DatabaseID
	, 'BACKUP DATABASE [' 
		+ DatabaseName 
		+ '] TO DISK = '''
		+ ReadOnlyBackupFile
		+ ''''
	, 'RESTORE VERIFYONLY FROM DISK = '''
		+ ReadOnlyBackupFile
		+ ''''
from ADMIN.dbo.Backup_Databases as D with (nolock)
where DontBackup = 1
	and ReadOnlyBackupFile <> '-1'
	and Database_Status = 'ONLINE'
	and BackupGroup = 2
	and ReadOnlyBackupDate = '1900-01-01 00:00:00.000'


--------------------------------------------------------------------------------
-- ***DEBUG***
-- If debug flag is set, return the list of commands to run, but don't
-- execute them.
--------------------------------------------------------------------------------	
if @Debug = 1
begin
	select * from #LastKnownROBackups
    select * from #ROBackupsOnDisk
	select * from #ROBackupReset

	return
end

--------------------------------------------------------------------------------
-- Walk through #Backup.  Attempt to backup the database, if that works attempt
-- to verify it.  If verification passes update ADMIN.dbo.Backup_Databases
-- with the time of completion.  Else throw an error.
--------------------------------------------------------------------------------
select @Start3 = min(BackupID) from #Backup with (nolock)
select @End3= max(BackupID) from #Backup with (nolock)
	
while @Start3 <= @End3
begin
	select @DatabaseID = DatabaseID
		, @BackupCommand = BackupCommand
		, @VerificationCommand = VerificationCommand
	from #Backup with (nolock)
	where BackupID = @Start3
	
	exec (@BackupCommand)
	select @BackupError = @@error
	
	if @BackupError = 0
	begin
		exec (@VerificationCommand)
		select @VerificationError = @@error
		
		if @VerificationError = 0
		begin
			update ADMIN.dbo.Backup_Databases
			set ReadOnlyBackupDate = getdate()
			where DatabaseID = @DatabaseID
		end
		else
		begin
			insert into ADMIN.dbo.Backup_ErrorLog
				(Command
				, Error
				, ErrorDate)
			select @VerificationCommand
				, 'ERROR: ' + @VerificationError
				, @Today
		end
	end
	else
	begin
		insert into ADMIN.dbo.Backup_ErrorLog
			(Command
			, Error
			, ErrorDate)
		select @BackupCommand
			, 'ERROR: ' + @BackupError
			, @Today
	end
	
	select @Start3 = @Start3 + 1
end

--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #Backup
drop table #LastKnownROBackups
drop table #MD
drop table #Directories
drop table #ROBackupsOnDisk
drop table #ROBackupReset
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------















GO
