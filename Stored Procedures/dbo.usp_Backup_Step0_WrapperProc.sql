SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE proc [dbo].[usp_Backup_Step0_WrapperProc]
	@BackupType varchar(20) = 'LOG'
	, @Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 10/23/2009

Purpose: 
	Backup solution.
	
Revisions:
	10/27/2009 - Andrew Gould
		Added email notification of detected errors.
	05/05/2011 - Travis Tittle
		Added Differential Backup Step to the WrapperProc
	06/15/2011 - Travis Tittle
		Removed the Backup Device step.
	06/23/2011 - Travis Tittle
		Incorporated GLaDOS Alerting.
	12/13/2011 - Travis Tittle
		Added ReadOnly BackupType
	02/24/2012 - Travis Tittle
		Removed the Copy portion of the Backup Solution from the Wrapper Proc.  The copy portion will now be a separate job/process.
	03/22/2012 - Travis Tittle
		Removed the Delete portion of the Backup Solution from the Wrapper Proc.  The delete portion will now be a separate job/process.
	07/05/2012 - Travis Tittle
		Revised to have Log Backup Job skip step 2.  It will create directory only if new database is found.
To Do:
	None
	
Notes:
	See individual procs for specific notes.
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Declare variables.
--------------------------------------------------------------------------------
declare @Subject varchar(100)

declare @Today datetime
	, @ErrorCount int
	, @MaxCommandLength int
	, @MaxErrorLength int
	, @FailDateTime datetime
    , @Provider varchar(50)

select @Subject = @@servername + ': Backup Error'


select @Today = getdate()
select @FailDateTime = GETDATE()
select @Provider = 'BackupProvider'
select @ErrorCount = -1
select @MaxCommandLength = 255
select @MaxErrorLength = 255

--------------------------------------------------------------------------------
-- Execute backup procs in order.
--------------------------------------------------------------------------------
print 'Begin: usp_Backup_Step1_FindDBs'
exec ADMIN.dbo.usp_Backup_Step1_FindDBs
print 'End: usp_Backup_Step1_FindDBs'
-------------------------------------------------------
if @BackupType = 'LOG'
begin
    print 'Log Backup Job skips Step2 MakeDirectories'
end
else if @BackupType = 'READONLY'
begin
    print 'Read Only Backup Job skips Step2 MakeDirectories'
end
else if @BackupType = 'DATABASE' OR @BackupType = 'DIFFERENTIAL'
begin
	print 'Begin: usp_Backup_Step2_MakeDirectories'
    exec ADMIN.dbo.usp_Backup_Step2_MakeDirectories @Debug
    print 'End: usp_Backup_Step2_MakeDirectories'
end
else
begin
	insert into ADMIN.dbo.Backup_ErrorLog
		(Command
		, Error
		, ErrorDate)
	select 'ADMIN.dbo.usp_Backup_Step3_?'
		, 'ERROR: Unknown BackupType - ' + @BackupType
		, @Today
end
-------------------------------------------------------
if @BackupType = 'DATABASE'
begin
	print 'Begin: usp_Backup_Step3_BackupDatabases'
	exec ADMIN.dbo.usp_Backup_Step3_BackupDatabases @Debug
	print 'End: usp_Backup_Step3_BackupDatabases'
end
else if @BackupType = 'DIFFERENTIAL'
begin
	print 'Begin: usp_Backup_Step3_BackupDifferentials'
	exec ADMIN.dbo.usp_Backup_Step3_BackupDifferentials @Debug
	print 'End: usp_Backup_Step3_BackupDifferentials'
end
else if @BackupType = 'LOG'
begin
	print 'Begin: usp_Backup_Step3_BackupLogs'
	exec ADMIN.dbo.usp_Backup_Step3_BackupLogs @Debug
	print 'End: usp_Backup_Step3_BackupLogs'
end
else if @BackupType = 'READONLY'
begin
	print 'Begin: usp_Backup_Step3_BackupReadOnly'
	exec ADMIN.dbo.usp_Backup_Step3_BackupReadOnly @Debug
	print 'End: usp_Backup_Step3_BackupReadOnly'
end
else
begin
	insert into ADMIN.dbo.Backup_ErrorLog
		(Command
		, Error
		, ErrorDate)
	select 'ADMIN.dbo.usp_Backup_Step3_?'
		, 'ERROR: Unknown BackupType - ' + @BackupType
		, @Today
end

--------------------------------------------------------------------------------
-- Look for errors, if any found format the results query and email it.
--------------------------------------------------------------------------------
select @ErrorCount = count(*)
from ADMIN.dbo.Backup_ErrorLog with (nolock)
where Resolved = 0

select @MaxCommandLength = max(len(Command))
from ADMIN.dbo.Backup_ErrorLog with (nolock)
where Resolved = 0

select @MaxErrorLength = max(len(Error))
from ADMIN.dbo.Backup_ErrorLog with (nolock)
where Resolved = 0

if @ErrorCount > 0
begin
    if exists (select * from Admin.dbo.glados_ServerAlerts where [ErrorInfo] = @Subject)
    begin
       update Admin.dbo.glados_ServerAlerts
       set LastFailure = @FailDateTime,
           DateUpdated = @FailDateTime
       where Provider = @Provider
       and ErrorInfo = @Subject
    end
    else 
    begin
       insert into dbo.glados_ServerAlerts
       (Provider, 
        Name, 
        FirstFailure, 
        LastFailure, 
        ErrorInfo, 
        ErrorLevel, 
        IsCurrent, 
        DateCreated, 
        DateUpdated)
       values
       (@Provider,
        'Local Backup or Backup Delete Error.  Please see Admin.dbo.Backup_ErrorLog',
        @FailDateTime,
        @FailDateTime,
        @Subject,
        'WARN',
        1,
        @FailDateTime,
        @FailDateTime);
             
    end
end
else
begin
delete dbo.glados_ServerAlerts
	output 
		deleted.ServerAlertID, 		
		deleted.provider,
		deleted.Name,
		deleted.FirstFailure,
		deleted.LastFailure,
		deleted.ErrorInfo,
		deleted.ErrorLevel,
		deleted.IsCurrent,
		deleted.DateCreated,
		deleted.DateUpdated
	into dbo.glados_ServerAlertsArchive
	from dbo.glados_ServerAlerts al
	where al.Provider= @Provider
	and al.ErrorInfo = @Subject
end

















GO
