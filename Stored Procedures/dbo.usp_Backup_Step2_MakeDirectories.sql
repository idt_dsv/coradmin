SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO









CREATE proc [dbo].[usp_Backup_Step2_MakeDirectories]
	@Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 10/23/2009

Purpose: 
	Build a local and remote directory to hold backups for each database.
	
Revisions:
	04/28/2011 - Travis Tittle - Added double quotes to the make directory command
							  in order to capture full database name in directory
	12/13/2011 - Travis Tittle - Added ReadOnly Directory lookup and creation.
	05/03/2012 - Travis Tittle - Removed Remote Directory creation portion.

To Do:
	None
	
Notes:
	This is ran for every db regardless if the folder exists or not.  It is 
	easier to just attempt to create the folder than test for its existance.
	
	If the dir already exists an error is returned however this does not halt
	the process or cause any issues so it can safely be ignored.
------------------------------------------------------------------------------*/


---------------------------------------------------	
-- Loads every database slated to be backed up into
-- a new table.  This is used to line up the ids for
-- the while loop.
-- Note: This is ran for every db regardless if the
-- folder exists or not.  It is easier to just attempt
-- to create the folder than test for its existance.
-- As far as I'm aware the error returned for trying
-- to create a folder that already exists has zero
-- inpact on the process.
---------------------------------------------------	


--------------------------------------------------------------------------------
-- Create temp table.
--------------------------------------------------------------------------------
create table #MD
	(MDID int identity(1,1)
	, MDCommand varchar(500))

--------------------------------------------------------------------------------
-- Declare variables.
--------------------------------------------------------------------------------
declare @Start int
	, @End int
	, @MDCommand varchar(1000)

--------------------------------------------------------------------------------
-- Build a list of create LOCAL directory tree commands.
--------------------------------------------------------------------------------
insert into #MD
	(MDCommand)
select 'xp_cmdshell ''md "' + P.BackupPath + DB.DatabaseName + '"'''
from ADMIN.dbo.Backup_Databases as DB with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on DB.LocalPathID = P.PathID
where DontBackup = 0
AND BackupGroup = 1

--------------------------------------------------------------------------------
-- ***DEBUG***
-- If debug flag is set, return the list of commands to run, but don't
-- execute them.
--------------------------------------------------------------------------------	
if @Debug = 1
begin
	select * from #MD with (nolock)

	return
end

--------------------------------------------------------------------------------
-- Walk through #MD executing the commands.
--------------------------------------------------------------------------------	
select @Start = min(MDID) from #MD with (nolock)
select @End = max(MDID) from #MD with (nolock)

while @Start <= @End
begin
	select @MDCommand = MDCommand
	from #MD with (nolock)
	where MDID = @Start
	
	exec(@MDCommand)
	
	select @Start = @Start + 1
end

--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #MD
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


GO
