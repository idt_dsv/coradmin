SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE proc [dbo].[usp_Backup_Step4_DeleteOldFiles]
	@Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 10/23/2009

Purpose: 
	Build a list of every file in all backup paths that has exceeded it's keep
	alive time.
	
Revisions:
	10/27/2009 - Andrew Gould
		Added better error handling.
	12/09/2009 - Andrew Gould
		Updated delete flagging process to ignore files in PendingCommand.
	12/03/2010 - Travis Tittle
		Made step delete all files, both Local and Remote, from their directories
		if the file was older than needed.  Before the process was only deleting 
		records from Nearstore, and was skipping Local Full Transaction Logs.
	12/27/2010 - Travis Tittle
		Made revision, where the step will post errors into the dbo.Backup_ErrorLog
		table only once, and then be marked as "Resolved" in the table.  This will
		eliminate the spam we have been getting.
	04/28/2011 - Travis Tittle
		Made revision to how files are deleted.  Process will now locate and delete 
		all database files, regardless of database name (-,., space, etc.)
		Added double quotes to delete step to locate and delete file by full database name
		Added deletion of ghost files on nearstore that didn't completely copy over.
		Added catch to not delete LogShipping Nearstore T-Log Backup Files.
	06/16/2011 - Travis Tittle
		With the removal of Backup Devices, files will no longer be appended to, and instead
		will need to be removed both locally and remotely when out of date guidelines are met.
	03/22/2012 - Travis Tittle
		Added Local Backup File check, so that if a file exists locally, but doesn't exist remotely, 
		it will not delete the local copy.  This ensures all backup files make it to their Nearstore
		prior to deletion.  Also skips deleting current local full backups, as this is taken care of in step 3.
	02/05/2014 - Travis Tittle	
		Added ReadOnly Backup removals to the mix if they aren't the most recent RO Backup.  Also changed directory pull to account for directories with spaces in them. 
To Do:
	None
	
Notes:
	It will only look through paths that aren't listed as Archived.
	
	DIR Switches:
	/N		- New long list format where filenames are on the far right.
	/O-D	- List by files in sorted order by date.
	/S		- Displays files in specified directory and all subdirectories.
	/A-d	- Don't include any directories in the output.
	/B		- Uses bare format (no heading information or summary).
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp tables.
--------------------------------------------------------------------------------
create table #Directories
	(DirID int identity(1,1)
	, DirCommand varchar(1000)
	, FileLocation varchar(100))

create table #Cleanup
	(FileID int identity(1,1)
	, FileName varchar(max)
	, Suffix varchar(12)
	, FileDate datetime
	, DaysToKeepBackups int
	, DatabaseName varchar(200)
	, ToDelete bit default 0
	, IsLogShipped bit default 0
	, FileLocation varchar(100))

create table #Delete
	(DeleteID int identity(1,1)
	, DeleteCommand varchar(1000))

create table #FlipString
	(FlipText varchar(1000) null,
	 FlipDateString varchar(50) null,
	 DateString varchar(50) null,
	 FlipDatabaseString varchar(100) null,
	 DatabaseString varchar(100) null)

create table #Output
	(OutputText varchar(1000))
	
create table #LocalBackups
    (BackupID int identity(1,1),
     FileName varchar(1000) null,
     FileDate datetime null,
     DatabaseName varchar(200) null,
     DoDelete bit default 1)

--------------------------------------------------------------------------------
-- Declare variables.
--------------------------------------------------------------------------------
declare @Start int
	, @End int
	, @DirCommand varchar(1000)
	, @DeleteCommand varchar(1000)
	, @Start2 int
	, @End2 int

declare @Today datetime
select @Today = getdate()

--------------------------------------------------------------------------------
-- Build dir commands for each backup path.
-- Note: It will only look through paths that aren't listed as Archived.
-- DIR Switches:
--	/N		- New long list format where filenames are on the far right.
--	/O-D	- List by files in sorted order by date.
--	/S		- Displays files in specified directory and all subdirectories.
--	/A-d	- Don't include any directories in the output.
--	/B		- Uses bare format (no heading information or summary).
--------------------------------------------------------------------------------
insert into #Directories
	(DirCommand,
	 FileLocation)
select 'xp_cmdshell ''dir /N /O-D /S /A-d /B "' + BackupPath + '"''',
      Location
from ADMIN.dbo.Backup_Paths with (nolock)
where IsArchived = 0

--------------------------------------------------------------------------------
-- Walk through #Directories, executing the dir commands.
--------------------------------------------------------------------------------	
select @Start = min(DirID) from #Directories with (nolock)
select @End = max(DirID) from #Directories with (nolock)

while @Start <= @End
begin
	select @DirCommand = DirCommand
	from #Directories with (nolock)
	where DirID = @Start
	
	insert into #Cleanup
		(FileName)
	exec (@DirCommand)
	
	select @Start = @Start + 1
end

----------------------------------------------
-- Delete Null Records, and any other files not considered a backup file.
----------------------------------------------
DELETE #Cleanup
WHERE FileName IS NULL
----------------------------------------------
DELETE from #Cleanup
WHERE FileName NOT LIKE '%.bak'
AND FileName NOT LIKE '%.trn'
AND FileName NOT LIKE '%.dif'

----------------------------------------------
-- Differentiate what files are Local and Remote for deleting purposes later.
----------------------------------------------

UPDATE #Cleanup
   SET FileLocation = CASE WHEN FileName LIKE '\\%' THEN 'Remote' ELSE 'Local' END 

----------------------------------------------
-- Differentiate what files are Read Only backup files
----------------------------------------------
   
UPDATE #Cleanup
   SET FileLocation = 'ReadOnly'
   WHERE FileLocation = 'Remote'
   AND FileName LIKE '%ReadOnly%' 
   
----------------------------------------------
-- Remove Current Local Full Backups from Delete step, as these are deleted by Step 3.
---------------------------------------------- 
   
DELETE #Cleanup
FROM #Cleanup as c
JOIN Admin.dbo.Backup_Databases as bd with (nolock)
   ON bd.BackupFile = c.FileName
   
----------------------------------------------
--Mark old delete references within the error log as resolved, so the process will stop
--emailing old news.
----------------------------------------------
UPDATE ADMIN.dbo.Backup_ErrorLog
   SET Resolved = '1'
   WHERE del_marker = '1'

--------------------------------------------------------------------------------
-- Perform a series of updates to #Cleanup and populate #FlipString to get Date string.
--------------------------------------------------------------------------------
-- Associate each file to its path's DaysToKeepBackups value.
-- Edit #Cleanup to keep nearstore Full Backup Files for a week.
update #Cleanup
set DaysToKeepBackups = P.DaysToKeepBackups
from #Cleanup as C with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)         
	on C.FileName like P.BackupPath + '%'

--Flip String to Obtain Date Part.

INSERT INTO #FlipString
   (FlipText)
SELECT REVERSE(#Cleanup.FileName) AS FlipText
FROM #Cleanup

UPDATE #FlipString
   SET FlipDateString = (SELECT SUBSTRING (#FlipString.FlipText, 1, (PATINDEX('%[_]%',#FlipString.FlipText)-1))),
       FlipDatabaseString = (SELECT SUBSTRING (#FlipString.FlipText, 1, (PATINDEX('%\%',#FlipString.FlipText)-1)))
   FROM #FlipString
   
UPDATE #FlipString
   SET DateString = (SELECT REVERSE(#FlipString.FlipDateString)),
       DatabaseString = (SELECT REVERSE(#FlipString.FlipDatabaseString))
   FROM #FlipString
 
UPDATE #FlipString
   SET DateString = (SELECT SUBSTRING (#FlipString.DateString, 1, 12)), 
       DatabaseString = (SELECT SUBSTRING (#FlipString.DatabaseString, 1, LEN(#FlipString.DatabaseString)-17)), 
       FlipText =   (SELECT REVERSE(#FlipString.FlipText))
   FROM #FlipString

-- Update #Cleanup to add Suffix data into table, as well as DatabaseName
-- Note: This is only done for files who's last 12 characters form an integer.
-- Mark IsLogShipped if a nearstore t-log and is being logshipped.
update #Cleanup
set Suffix = #FlipString.DateString 
FROM #FlipString
where #FlipString.FlipText = #Cleanup.FileName 

update #Cleanup
set DatabaseName = #FlipString.DatabaseString
from #FlipString
where #FlipString.FlipText = #Cleanup.FileName

update #Cleanup
set IsLogShipped = bd.IsLogShipped
from ADMIN.dbo.Backup_Databases as bd with (nolock)
where bd.DatabaseName = #Cleanup.DatabaseName
and #Cleanup.FileName LIKE '%.trn'

-- Using those last 12 characters rebuild them into a date.
update #Cleanup
set FileDate = cast(substring(Suffix,1,4) 
		+ '-' 
		+ substring(Suffix,5,2) 
		+ '-'
		+ substring(Suffix,7,2)
		+ ' '
		+ substring(Suffix,9,2)
		+ ':'
		+ substring(Suffix,11,2) as datetime)
where Suffix is not null

----------------------------------------------
--Seperate out Local Backups to run through copy check.  Any backup files that exist Locally, 
--but haven't made their way to Nearstore, do not delete.
----------------------------------------------
INSERT INTO #LocalBackups
   (FileName,
     FileDate,
     DatabaseName)
SELECT FileName,
     FileDate,
     DatabaseName
FROM #Cleanup
WHERE FileLocation = 'Local'


UPDATE l
SET l.DoDelete = 0 
FROM #LocalBackups AS l
JOIN #Cleanup AS c
   ON c.FileDate = l.FileDate
   AND c.DatabaseName = l.DatabaseName
WHERE c.FileLocation = 'Remote'  


DELETE #Cleanup
FROM #Cleanup as c
JOIN #LocalBackups as l
   ON l.FileName = c.FileName
WHERE l.DoDelete = 1

----------------------------------------------
-- Remove existing most recent ReadOnly Backup files from the Delete process
----------------------------------------------

DELETE #Cleanup
FROM #Cleanup as c
JOIN Admin.dbo.Backup_Databases as bd
   ON bd.ReadOnlyBackupFile = c.FileName
WHERE FileLocation = 'ReadOnly'
-------------------------------------------------------------------------------
-- Compare the age of the file to how old files can be for that path.
-- Flag Files older than that for deletion.
-------------------------------------------------------------------------------
update #Cleanup
set ToDelete = 1
from #Cleanup as C with (nolock)
where FileDate < dateadd(dd,DaysToKeepBackups*-1,getdate())

--------------------------------------------------------------------------------
-- Build the delete commands for each file slated to be deleted.
--------------------------------------------------------------------------------
insert into #Delete
      (DeleteCommand)
select 'xp_cmdshell ''del '
      + '"'
      + FileName
      + '"'
      + ''''
from #CleanUp with (nolock)
where ToDelete = 1
and IsLogShipped <> 1

--------------------------------------------------------------------------------
-- ***DEBUG***
-- If debug flag is set, return the list of commands to run, but don't
-- execute them.
--------------------------------------------------------------------------------	
if @Debug = 1
begin
	select * from #CleanUp with (nolock)
	select * from #Delete with (nolock)

	return
end

--------------------------------------------------------------------------------
-- Walk through #Delete and delete each file in it.
--------------------------------------------------------------------------------
select @Start2 = min(DeleteID) from #Delete with (nolock)
select @End2 = max(DeleteID) from #Delete with (nolock)

while @Start2 <= @End2
begin
      select @DeleteCommand = DeleteCommand
      from #Delete with (nolock)
      where DeleteID = @Start2
      
      insert into #Output
            (OutputText)
      exec (@DeleteCommand)
      
      if (select count(*) from #Output) <> 1
      begin
            if (select count(*) from #Output where OutputText like 'Could Not Find%'
                        or OutputText like 'The process cannot access the file because it is being used by another process.%') = 0
            begin
                  insert into ADMIN.dbo.Backup_ErrorLog
                        (Command
                        , Error
                        , ErrorDate
                        , del_marker)
                  select @DeleteCommand
                        , OutputText
                        , @Today
                        , '1'
                  from #Output with (nolock)
            end
      end
      
      truncate table #Output
      
      select @Start2 = @Start2 + 1
end
--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #Directories
drop table #CleanUp
drop table #Delete
drop table #Output
drop table #FlipString
drop table #LocalBackups
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------








GO
