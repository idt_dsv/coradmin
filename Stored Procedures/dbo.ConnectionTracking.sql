SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[ConnectionTracking]
as

set nocount on

/*-----------------------------------------------------
Created By: Andrew Gould
Created On: 8/7/2009

Purpose: Monitor and log connections to SQL.

Permissions: None required this will be run by SA.

Objects:
	master.dbo.sysprocesses
	master.dbo.sysdatabases
	Admin.dbo.ConnectionTracking_Log

Revisions:
	11/2/2012 - Andrew Gould
		Added MACAddress
		Cleaned up column definitions

To Do:
-----------------------------------------------------*/

/*-----------------------------------------------------
-- Perminanent table.
create table Admin.dbo.ConnectionTracking_Log
(
	ConnectionID int identity(1,1) primary key
	, DatabaseName sysname
	, ComputerName varchar(128)
	, MACAddress varchar(12)
	, ProgramName varchar(128)
	, LoginName varchar(128)
	, FirstConnection datetime
	, LastConnection datetime
)
-----------------------------------------------------*/

-------------------------------------------------------
-- Create holding table.
-------------------------------------------------------
create table #Tracker
(
	DatabaseName sysname
	, ComputerName varchar(128)
	, MACAddress varchar(12)
	, ProgramName varchar(128)
	, LoginName varchar(128)
	, LastConnection datetime
)

-------------------------------------------------------
-- Declare variables.
-------------------------------------------------------
declare @Today datetime
select @Today = getdate()

-------------------------------------------------------
-- Insert current processes into holding table.
-------------------------------------------------------
insert into #Tracker
(
	DatabaseName
	, ComputerName
	, MACAddress
	, ProgramName
	, LoginName
	, LastConnection
)
select SD.[Name] as DatabaseName
	, SP.hostname as ComputerName
	, SP.net_address as MACAddress
	, SP.program_name as ProgramName
	, SP.loginame as LoginName
	, @Today as LastConnection
from master.dbo.sysprocesses as SP with (nolock)
inner join master.dbo.sysdatabases as SD with (nolock)
	on SP.dbid = SD.dbid
where loginame <> 'sa'
group by SD.[Name]
	, SP.hostname
	, SP.net_address
	, SP.program_name
	, SP.loginame

-------------------------------------------------------
-- Update existing connections with current date.
-------------------------------------------------------
update [Admin].dbo.ConnectionTracking_Log
	set LastConnection = @Today
from [Admin].dbo.ConnectionTracking_Log as CT with (nolock)
inner join #Tracker as T with (nolock)
	on CT.DatabaseName = T.DatabaseName
	and CT.ComputerName = T.ComputerName
	and CT.MACAddress = T.MACAddress
	and CT.ProgramName = T.ProgramName
	and CT.LoginName = T.LoginName

-------------------------------------------------------
-- Insert new connections.
-------------------------------------------------------
insert into [Admin].dbo.ConnectionTracking_Log
(
	DatabaseName
	, ComputerName
	, MACAddress
	, ProgramName
	, LoginName
	, FirstConnection
	, LastConnection
)
select T.DatabaseName
	, T.ComputerName
	, T.MACAddress
	, T.ProgramName
	, T.LoginName
	, @Today -- FirstConnection
	, T.LastConnection
from [Admin].dbo.ConnectionTracking_Log as CT with (nolock)
right outer join #Tracker as T with (nolock)
	on CT.DatabaseName = T.DatabaseName
	and CT.ComputerName = T.ComputerName
	and CT.MACAddress = T.MACAddress
	and CT.ProgramName = T.ProgramName
	and CT.LoginName = T.LoginName
where CT.ConnectionID is null

-------------------------------------------------------
-- Clean up.
-------------------------------------------------------
drop table #Tracker
-------------------------------------------------------
-------------------------------------------------------
GO
