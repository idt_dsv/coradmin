SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE proc [dbo].[usp_Backup_Step3_BackupDifferentials]
	@Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 05/06/2011

Purpose: 
	Create a Differential Backup for each database and verify.
	
Revisions:
	06/06/2011 - Travis Tittle
		Now attempting to back up only those databases with a state of ONLINE.
	06/15/2011 - Travis Tittle
		Removed reliance on Backup Device.
	12/02/2011 - Andrew Gould
		I've hacked in a full backup of master.  This will need to be cleaned up
		and a more robust solution put into place as currently the dif backup
		process won't push .bak files to nearstore and we need to maintain
		current full backups of master there in case we ever need to rebuild a server.
	12/13/2011 - Travis Tittle
		Added BackupGroup.
	02/06/2014 - Travis Tittle
		Removed Compression flag, as New Build Doc requires Compression already be set in sys.configurations.

To Do:
	None
	
Notes:
	For a Differential to be backed up the following conditions must be met.
	1.) It must have a full backup already.  If not, it creates one
	3.) It must have a differential file set (LastDiffBackupFile <> '-1')
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp table.
--------------------------------------------------------------------------------
create table #FullBackup
	(BackupID int identity(1,1)
	, BackupType varchar(10)
	, DatabaseID int
	, BackupFullCommand varchar(500)
	, VerificationFullCommand varchar(500))
	
create table #DifferentialBackup
	(BackupID int identity(1,1)
	, BackupType varchar(10)
	, DatabaseDiffID int
	, BackupDiffCommand varchar(500)
	, VerificationDiffCommand varchar(500))
	
create table #DelCommand
    (DeleteID int identity(1,1)
    , DeleteCommand varchar(300))

--------------------------------------------------------------------------------
-- Declare Variables.
--------------------------------------------------------------------------------
declare @Start int
	, @End int
	, @Start2 int
	, @End2 int
	, @StartDel int
	, @EndDel int
	, @DatabaseID int
	, @DatabaseDiffID int
	, @BackupFullCommand varchar(500)
	, @VerificationFullCommand varchar(500)
	, @BackupDiffCommand varchar(500)
	, @VerificationDiffCommand varchar(500)
	, @BackupFullError int
	, @BackupDiffError int
	, @VerificationFullError int
	, @VerificationDiffError int
	, @Today datetime
	, @DeleteCommand varchar(300)

select @BackupFullError = -1
select @VerificationFullError = -1
select @BackupDiffError = -1
select @VerificationDiffError = -1
select @Today = getdate()

--------------------------------------------------------------------------------
-- Remove Last Local Full Backup Prior to Full Backup for capacity concerns.
--------------------------------------------------------------------------------	
insert into #DelCommand
   (DeleteCommand)
select 'xp_cmdshell ''del '
      + '"'
      + D.BackupFile
      + '"'
      + ''''
from Admin.dbo.Backup_Databases as D with (nolock)
where (D.DontBackup = 0
and D.LastBackupDate < dateadd(dd,-8,@Today)
and D.Database_Status = 'ONLINE'
and D.BackupGroup = 1
and D.BackupFile <> '-1')
   or D.DatabaseName = 'master'
--------------------------------------------------------------------------------
select @StartDel = min(DeleteID) from #DelCommand with (nolock)
select @EndDel = max(DeleteID) from #DelCommand with (nolock)
	
while @StartDel <= @EndDel
begin
	select @DeleteCommand = DeleteCommand
	from #DelCommand with (nolock)
	where DeleteID = @StartDel
	
	exec (@DeleteCommand)
	
    select @StartDel = @StartDel + 1
end	
--------------------------------------------------------------------------------
-- If Full Backup has yet to be created, or if Full Backup is older than 8 days, 
-- create Full Backup prior to attempting a Differential Backup.
--------------------------------------------------------------------------------
update ADMIN.dbo.Backup_Databases
set BackupFile = P.BackupPath
		+ D.FolderName 
		+ '\'
		+ D.DatabaseName
		+ '_'
		-- This jumbled mess takes a date and trims of the seconds and miliseconds and removes all formating.
		+ left(replace(replace(replace(convert(varchar,@Today,121),' ',''),':',''),'-',''),12) 
		+ '.bak'
from ADMIN.dbo.Backup_Databases as D with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on D.LocalPathID = P.PathID
where (D.DontBackup = 0
and D.LastBackupDate < dateadd(dd,-8,@Today)
and D.Database_Status = 'ONLINE'
and D.BackupGroup = 1)
   or D.DatabaseName = 'master'

insert into #FullBackup
	(BackupType
	, DatabaseID
	, BackupFullCommand
	, VerificationFullCommand)
select 'DATABASE'
    , DatabaseID
	, 'BACKUP DATABASE [' 
		+ DatabaseName 
		+ '] TO DISK = '''
		+ BackupFile
		+ ''''
	, 'RESTORE VERIFYONLY FROM DISK = '''
		+ BackupFile
		+ ''''
from ADMIN.dbo.Backup_Databases as D with (nolock)
where (DontBackup = 0
		and BackupFile <> '-1'
		and LastBackupDate < dateadd(dd,-8,@Today)
		and Database_Status = 'ONLINE'
		and BackupGroup = 1)
/********* Hacked in a full backup of master *********/
	or DatabaseName = 'master'
/*****************************************************/
--------------------------------------------------------------------------------
-- Walk through #FullBackup.  Attempt to backup the database, if that works attempt
-- to verify it.  If verification passes update ADMIN.dbo.Backup_Databases
-- with the time of completion.  Else throw an error.
--------------------------------------------------------------------------------
select @Start = min(BackupID) from #FullBackup with (nolock)
select @End = max(BackupID) from #FullBackup with (nolock)
	
while @Start <= @End
begin
	select @DatabaseID = DatabaseID
		, @BackupFullCommand = BackupFullCommand
		, @VerificationFullCommand = VerificationFullCommand
	from #FullBackup with (nolock)
	where BackupID = @Start
	
	exec (@BackupFullCommand)
	select @BackupFullError = @@error
	
	if @BackupFullError = 0
	begin
		exec (@VerificationFullCommand)
		select @VerificationFullError = @@error
		
		if @VerificationFullError = 0
		begin
			update ADMIN.dbo.Backup_Databases
			set LastBackupDate = getdate()
			where DatabaseID = @DatabaseID
		end
		else
		begin
			insert into ADMIN.dbo.Backup_ErrorLog
				(Command
				, Error
				, ErrorDate)
			select @VerificationFullCommand
				, 'ERROR: ' + @VerificationFullError
				, @Today
		end
	end
	else
	begin
		insert into ADMIN.dbo.Backup_ErrorLog
			(Command
			, Error
			, ErrorDate)
		select @BackupFullCommand
			, 'ERROR: ' + @BackupFullError
			, @Today
	end
	
	select @Start = @Start + 1
end

--------------------------------------------------------------------------------
-- Loads backup and restore commands into #Backup for log backups.
-- Note: The database must be flagged to be backed up (DontBackup = 0)
--	It must be in full recovery mode (RecoveryModel = 'FULL')
--  And it must have a log file set (LastLogBackupFile <> '-1')
--	Model is explicitly skipped.
--------------------------------------------------------------------------------
update ADMIN.dbo.Backup_Databases
set LastDiffBackupFile = P.BackupPath
		+ D.FolderName 
		+ '\'
		+ D.DatabaseName
		+ '_'
		-- This jumbled mess takes a date and trims of the seconds and miliseconds and removes all formating.
		+ left(replace(replace(replace(convert(varchar,@Today,121),' ',''),':',''),'-',''),12) 
		+ '.dif'
from ADMIN.dbo.Backup_Databases as D with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on D.LocalPathID = P.PathID
where D.DontBackup = 0
	and D.Database_Status = 'ONLINE'
	and D.BackupGroup = 1
	and D.DatabaseName <> 'master'
	and D.LastBackupDate <> '1900-01-01 00:00:00.000'
	

insert into #DifferentialBackup
	(DatabaseDiffID
	, BackupDiffCommand
	, VerificationDiffCommand)
select DatabaseID
	, 'BACKUP DATABASE [' 
		+ DatabaseName 
		+ '] TO DISK = '''
		+ LastDiffBackupFile
		+ ''''
		+ 'WITH DIFFERENTIAL, INIT'
	, 'RESTORE VERIFYONLY FROM DISK = '''
		+ LastDiffBackupFile
		+ ''''
from ADMIN.dbo.Backup_Databases as D with (nolock)
where DontBackup = 0
	and LastDiffBackupFile <> '-1'
	and DatabaseName <> 'master'
	and Database_Status = 'ONLINE'
	and BackupGroup = 1
	and D.LastBackupDate <> '1900-01-01 00:00:00.000'

--------------------------------------------------------------------------------
-- ***DEBUG***
-- If debug flag is set, return the list of commands to run, but don't
-- execute them.
------------------------------------------------------------------------------	
if @Debug = 1
begin
	select * from #DifferentialBackup with (nolock)

	return
end

--------------------------------------------------------------------------------
-- Walk through #Backup.  Attempt to backup the database, if that works attempt
-- to verify it.  If verification passes update ADMIN.dbo.Backup_Databases
-- with the time of completion.  Else throw an error.
--------------------------------------------------------------------------------
select @Start2 = min(BackupID) from #DifferentialBackup with (nolock)
select @End2 = max(BackupID) from #DifferentialBackup with (nolock)
	
while @Start2 <= @End2
begin
	select @DatabaseDiffID = DatabaseDiffID
		, @BackupDiffCommand = BackupDiffCommand
		, @VerificationDiffCommand = VerificationDiffCommand
	from #DifferentialBackup with (nolock)
	where BackupID = @Start2
	
	exec (@BackupDiffCommand)
	select @BackupDiffError = @@error
	
	if @BackupDiffError = 0
	begin
		exec (@VerificationDiffCommand)
		select @VerificationDiffError = @@error
		
		if @VerificationDiffError = 0
		begin
			update ADMIN.dbo.Backup_Databases
			set LastDiffBackupDate = getdate()
			where DatabaseID = @DatabaseDiffID
		end
		else
		begin
			insert into ADMIN.dbo.Backup_ErrorLog
				(Command
				, Error
				, ErrorDate)
			select @VerificationDiffCommand
				, 'ERROR: ' + @VerificationDiffError
				, @Today
		end
	end
	else
	begin
		insert into ADMIN.dbo.Backup_ErrorLog
			(Command
			, Error
			, ErrorDate)
		select @BackupDiffCommand
			, 'ERROR: ' + @BackupDiffError
			, @Today
	end
	
	select @Start2 = @Start2 + 1
end

--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #FullBackup
drop table #DifferentialBackup
drop table #DelCommand
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------















GO
