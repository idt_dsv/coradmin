SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[Disks_GetSizes]
as begin
	declare @pscmd varchar(2000)
	set @pscmd = 'c:\DBAScripts\drivesize.bat ' + @@servername
	execute xp_cmdshell 'copy /Y \\prodsql10\dataservices\ScriptRepository\Get-DriveSize.ps1 C:\DBAScripts'
	execute xp_cmdshell 'copy /Y \\prodsql10\dataservices\ScriptRepository\drivesize.bat C:\DBAScripts'
	execute xp_cmdshell @pscmd
end

GO
