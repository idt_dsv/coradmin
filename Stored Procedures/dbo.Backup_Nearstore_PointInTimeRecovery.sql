SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE proc [dbo].[Backup_Nearstore_PointInTimeRecovery]

AS

SET NOCOUNT ON

/*------------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 03/22/2012

Purpose: 
	Determine all Backup Files located on nearstore are up to date and point in time recovery is as expected.
	
Revisions: 10/22/13 - tt - Resolved PITR logic.  Fixed to notice when DIFF is behind due to copy job not finishing.
	
Notes:
	
	DIR Switches:
	/N		- New long list format where filenames are on the far right.
	/O-D	- List by files in sorted order by date.
	/S		- Displays files in specified directory and all subdirectories.
	/A-d	- Don't include any directories in the output.
	/B		- Uses bare format (no heading information or summary).
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp tables.
--------------------------------------------------------------------------------
CREATE TABLE #Directories
	(DirID int identity(1,1)
	, DirCommand varchar(1000)
	, FileLocation varchar(100))

CREATE TABLE #NearstoreFiles
	(FileID int identity(1,1)
	, FileName varchar(max)
	, Suffix varchar(12)
	, FileDate datetime
	, DatabaseName varchar(200))

CREATE TABLE #FlipString
	(FlipText varchar(1000) null,
	 FlipDateString varchar(50) null,
	 DateString varchar(50) null,
	 FlipDatabaseString varchar(100) null,
	 DatabaseString varchar(100) null)

----------------------------------------------------------------------------------
---- Declare variables.
----------------------------------------------------------------------------------
DECLARE @Start int
	, @End int
	, @DirCommand varchar(1000)
	
--------------------------------------------------------------------------------
-- Load new databases into the table.
--------------------------------------------------------------------------------
INSERT INTO ADMIN.dbo.Backup_Nearstore_PITR
	(DatabaseName
	, RecoveryModel)
SELECT 
      D.[name] as DatabaseName
	, D.recovery_model_desc as RecoveryModel
FROM Master.sys.Databases as D with (nolock)
LEFT OUTER JOIN ADMIN.dbo.Backup_Nearstore_PITR as BN with (nolock)
	ON D.[name] = BN.DatabaseName
WHERE BN.DatabaseName is null
AND D.source_database_id IS NULL

--------------------------------------------------------------------------------
-- Delete Old databases from the table.
--------------------------------------------------------------------------------
DELETE Admin.dbo.Backup_Nearstore_PITR
FROM Admin.dbo.Backup_Nearstore_PITR as BN with (nolock)
LEFT OUTER JOIN master.sys.databases as D with (nolock)
   ON D.[name] = BN.DatabaseName
WHERE D.[name] is null

----------------------------------------------------------------------------------
-- If an Existing Database, Update dbo.Backup_Databases if Recovery Model has changed
-- If an Existing Database, check dbo.Backup_Databases on whether it is supposed to be backed up or not.
----------------------------------------------------------------------------------

UPDATE BN
   SET BN.RecoveryModel = D.recovery_model_desc
FROM Admin.dbo.Backup_Nearstore_PITR as BN with (nolock)
JOIN Master.sys.Databases as D with (nolock)
   ON D.Name = BN.DatabaseName
WHERE D.recovery_model_desc COLLATE SQL_Latin1_General_CP1_CI_AS <> BN.RecoveryModel

UPDATE BN
   SET BN.DontBackup = BD.DontBackup
FROM Admin.dbo.Backup_Nearstore_PITR as BN with (nolock)
JOIN Admin.dbo.Backup_Databases as BD with (nolock)
   ON BD.DatabaseName = BN.DatabaseName

--------------------------------------------------------------------------------
-- Build dir command for Nearstore path.
-- DIR Switches:
--	/N		- New long list format where filenames are on the far right.
--	/O-D	- List by files in sorted order by date.
--	/S		- Displays files in specified directory and all subdirectories.
--	/A-d	- Don't include any directories in the output.
--	/B		- Uses bare format (no heading information or summary).
--------------------------------------------------------------------------------
INSERT INTO #Directories
	(DirCommand)
SELECT 'xp_cmdshell ''dir /N /O-D /S /A-d /B ' + BackupPath + ''''
FROM ADMIN.dbo.Backup_Paths with (nolock)
WHERE Location = 'Remote'

--------------------------------------------------------------------------------
-- Walk through #Directories, executing the dir commands.
--------------------------------------------------------------------------------	
SELECT @Start = min(DirID) FROM #Directories with (nolock)
SELECT @End = max(DirID) FROM #Directories with (nolock)

WHILE @Start <= @End
BEGIN
	SELECT @DirCommand = DirCommand
	FROM #Directories with (nolock)
	WHERE DirID = @Start
	
	INSERT INTO #NearstoreFiles
		(FileName)
	EXEC (@DirCommand)
	
	SELECT @Start = @Start + 1
END

----------------------------------------------
-- Delete Null Records, and any other files not considered a backup file.
----------------------------------------------
DELETE #NearstoreFiles
WHERE FileName IS NULL
----------------------------------------------
DELETE FROM #NearstoreFiles
WHERE FileName NOT LIKE '%.bak'
AND FileName NOT LIKE '%.trn'
AND FileName NOT LIKE '%.dif'

--------------------------------------------------------------------------------
-- Perform a series of updates to #NearstoreFiles and populate #FlipString to get Date string.
--------------------------------------------------------------------------------

--Flip String to Obtain Date Part.

INSERT INTO #FlipString
   (FlipText)
SELECT REVERSE(#NearstoreFiles.FileName) AS FlipText
FROM #NearstoreFiles

UPDATE #FlipString
   SET FlipDateString = (SELECT SUBSTRING (#FlipString.FlipText, 1, (PATINDEX('%[_]%',#FlipString.FlipText)-1))),
       FlipDatabaseString = (SELECT SUBSTRING (#FlipString.FlipText, 1, (PATINDEX('%\%',#FlipString.FlipText)-1)))
   FROM #FlipString
   
UPDATE #FlipString
   SET DateString = (SELECT REVERSE(#FlipString.FlipDateString)),
       DatabaseString = (SELECT REVERSE(#FlipString.FlipDatabaseString))
   FROM #FlipString
 
UPDATE #FlipString
   SET DateString = (SELECT SUBSTRING (#FlipString.DateString, 1, 12)), 
       DatabaseString = (SELECT SUBSTRING (#FlipString.DatabaseString, 1, LEN(#FlipString.DatabaseString)-17)), 
       FlipText =   (SELECT REVERSE(#FlipString.FlipText))
   FROM #FlipString

-- Update #NearstoreFiles to add Suffix data into table, as well as DatabaseName
-- Note: This is only done for files who's last 12 characters form an integer.
UPDATE #NearstoreFiles
SET Suffix = #FlipString.DateString 
FROM #FlipString
WHERE #FlipString.FlipText = #NearstoreFiles.FileName 

UPDATE #NearstoreFiles
SET DatabaseName = #FlipString.DatabaseString
FROM #FlipString
WHERE #FlipString.FlipText = #NearstoreFiles.FileName


-- Using those last 12 characters rebuild them into a date.
UPDATE #NearstoreFiles
SET FileDate = cast(substring(Suffix,1,4) 
		+ '-' 
		+ substring(Suffix,5,2) 
		+ '-'
		+ substring(Suffix,7,2)
		+ ' '
		+ substring(Suffix,9,2)
		+ ':'
		+ substring(Suffix,11,2) as datetime)
WHERE Suffix is not null

--------------------------------------------------------------------------------
-- Get Most Recent Backup Files and their Dates to Obtain Current Point In Time Recovery
--------------------------------------------------------------------------------
UPDATE bn
   SET bn.MostRecentFull = (SELECT max(nf.FileDate)
                            FROM #NearstoreFiles as nf
                            WHERE nf.DatabaseName = bn.DatabaseName
                            and nf.FileName like '%.bak')
FROM Admin.dbo.Backup_Nearstore_PITR as bn with (nolock)
JOIN #NearstoreFiles as nf
   ON nf.DatabaseName = bn.DatabaseName

UPDATE bn
   SET bn.MostRecentDiff = (SELECT max(nf.FileDate)
                            FROM #NearstoreFiles as nf
                            WHERE nf.DatabaseName = bn.DatabaseName
                            and nf.FileName like '%.dif')
FROM Admin.dbo.Backup_Nearstore_PITR as bn with (nolock)
JOIN #NearstoreFiles as nf
   ON nf.DatabaseName = bn.DatabaseName

UPDATE bn
   SET bn.MostRecentLog = (SELECT max(nf.FileDate)
                            FROM #NearstoreFiles as nf
                            WHERE nf.DatabaseName = bn.DatabaseName
                            and nf.FileName like '%.trn')
FROM Admin.dbo.Backup_Nearstore_PITR as bn with (nolock)
JOIN #NearstoreFiles as nf
   ON nf.DatabaseName = bn.DatabaseName

UPDATE Admin.dbo.Backup_Nearstore_PITR
   SET MostRecentFull = '1900-01-01 00:00:00.000'
   WHERE MostRecentFull IS NULL
   
UPDATE Admin.dbo.Backup_Nearstore_PITR
   SET MostRecentDiff = '1900-01-01 00:00:00.000'
   WHERE MostRecentDiff IS NULL
   
UPDATE Admin.dbo.Backup_Nearstore_PITR
   SET MostRecentLog = '1900-01-01 00:00:00.000'
   WHERE MostRecentLog IS NULL
--------------------------------------------------------------------------------
-- Get Nearstore Point In Time Recovery
--------------------------------------------------------------------------------   
UPDATE bn 
   SET PointInTimeRecovery = CASE 
								WHEN MostRecentFull = '1900-01-01 00:00:00.000'
								THEN MostRecentFull --When No Fulls
								
								WHEN MostRecentFull <> '1900-01-01 00:00:00.000'
								AND MostRecentFull >= MostRecentDiff
								AND MostRecentFull >= MostRecentLog
								THEN MostRecentFull --When Full is newest
								
								WHEN MostRecentFull <> '1900-01-01 00:00:00.000'
								AND MostRecentDiff >= MostRecentFull
								AND MostRecentDiff >= MostRecentLog
								THEN MostRecentDiff --When Diff is newest
								
								WHEN MostRecentFull <> '1900-01-01 00:00:00.000'
								AND MostRecentDiff >= MostRecentFull
								AND MostRecentLog >= MostRecentDiff
								THEN MostRecentLog --When Tlog is newest and Diff is present
								
								WHEN MostRecentFull <> '1900-01-01 00:00:00.000'
								AND MostRecentDiff <= MostRecentFull
								AND MostRecentLog < dateadd(hh,27,MostRecentFull)
								THEN MostRecentLog --When Tlog is within 27hr of Bak and diff is missing or old
								
								WHEN MostRecentFull <> '1900-01-01 00:00:00.000'
								AND MostRecentDiff <= MostRecentFull
								AND MostRecentLog > dateadd(hh,27,MostRecentFull)
								THEN MostRecentFull --When Tlog isnt within 27hr of Bak and diff is missing or old
                             END
FROM Admin.dbo.Backup_Nearstore_PITR as bn with (Nolock)
WHERE DontBackup = 0

--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #Directories
drop table #NearstoreFiles
drop table #FlipString

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------











GO
