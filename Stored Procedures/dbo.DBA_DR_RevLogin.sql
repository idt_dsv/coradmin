SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DBA_DR_RevLogin] 
	AS 
/*
Process to create a self healing 'login export with passwords' process. 
John Wood 
3/16/2011
Revision 1 
Revision	Date		Reason														Who
2			4/5/2011	Add Self Healing Logic to create folderpath if not created.	John
3			7/16/2014	Change to write a local copy based on ADMIN.dbo.Backup_Paths John
4			7/28/2014	Change to more eligantly handle Time conversion.
*/
DECLARE @DateString CHAR(12)


SELECT @DateString=(REPLACE(CONVERT(CHAR(10),GetDate(),102),'.','')+REPLACE(CONVERT(CHAR(5),GETDATE(),114),':',''))

IF (SELECT Count(OBJECT_ID) FROM MASTER.sys.objects WHERE name='SP_help_revLogin' OR name='sp_hexadecimal') = 2
GOTO SCRIPTING
	BuildProcs:
	BEGIN
	DECLARE @PathCMD VARCHAR(512)
	--EXECUTE xp_cmdshell 'SQLCMD -E -i\\SQLMonitoring\DataServices\sp_help_revlogin.sql';
	SELECT @PathCMD= 'MKDIR -p '+BackupPath+'DR_Scripts' FROM [Admin].[dbo].[Backup_paths] Where Location='local'
	
	EXECUTE xp_cmdshell @PathCMD;
	END
	


SCRIPTING:
	DECLARE 
		@ServerName VARCHAR(256),
		@CMD VARCHAR (1000)
		
	SELECT @ServerName=	REPLACE(@@SERVERNAME,'\','_')
	SELECT 
			@CMD='SQLCMD -Q"sp_help_revlogin" -o '+BackupPath+'DR_Scripts\Logins'+'_'+@DateString+'.SQL'
		 FROM [Admin].[dbo].[Backup_paths] Where Location='local'
	;
	
	SELECT @CMD
	
	EXECUTE xp_cmdshell @CMD;
RETURN 


GO
