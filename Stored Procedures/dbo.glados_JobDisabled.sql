SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[glados_JobDisabled]
as

set nocount on

/*-------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 12/01/2011

Purpose:
	This proc runs locally once a night and loads all disabled jobs into
	GLaDOS as information messages.

Objects:
	msdb.dbo.sysjobs
	GLaDOS
-------------------------------------------------------------------------*/

-- Declare variables
declare @XML xml

-- Lookup all jobs that are currently disabled and build the list as XML.
select @XML =
(select 'Disabled Jobs' as Provider
	, name as Name
	, getdate() as FailDateTime
	, [description] as ErrorInfo
	, 'RPT' as ErrorLevel
from msdb.dbo.sysjobs with (nolock)
where enabled = 0
for xml raw('Alert'), root('Alerts'))

-- Flag all Disabled Jobs as no longer current.
exec [Admin].dbo.glados_ServerAlertsMarkInactive @Provider = 'Disabled Jobs'

-- Update current Disabled Jobs and insert new ones.
exec [Admin].dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML = @XML

-- Archive and remove any Disabled Jobs that are no longer current.
exec [Admin].dbo.glados_ArchiveServerAlerts @Provider = 'Disabled Jobs'
---------------------------------------------------------------------------
---------------------------------------------------------------------------
GO
