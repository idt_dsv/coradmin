SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[Glados_ErrorsWritten]
	AS
BEGIN
/*
Author:		John Wood Data Services
Reason:		Monitor errors written
Date:		4/24/2013
Notes:		Extended Events 
Reference:	http://sqlblog.com/blogs/jonathan_kehayias/archive/2010/12/13/an-xevent-a-day-13-of-31-the-system-health-session.aspx
Revision:	3
*/

SET NOCOUNT ON;

DECLARE @target_data XML
		,@ServerAlertsXml XML


EXECUTE ADMIN.dbo.glados_ServerAlertsMarkInactive 'Error Check'

SELECT 
	@target_data = CAST(t.target_data AS XML)
FROM sys.dm_xe_sessions AS s 
JOIN sys.dm_xe_session_targets AS t 
    ON t.event_session_address = s.address
WHERE s.name = 'System_health'
	AND t.target_name = 'ring_buffer'

SELECT 
    n.value('(@name)[1]', 'varchar(50)') AS event_name,
     DATEADD(hh, 
            DATEDIFF(hh, GETUTCDATE(), CURRENT_TIMESTAMP), 
            n.value('(@timestamp)[1]', 'datetime')) AS [TIMESTAMP],
    n.value('(data[@name="error"]/value)[1]', 'int') AS ERROR,
    n.value('(data[@name="severity"]/value)[1]', 'int') AS severity,
    n.value('(data[@name="message"]/value)[1]', 'varchar(max)') AS MESSAGE
INTO #events
FROM @target_data.nodes('RingBufferTarget/event') AS q(n)
WHERE n.value('(@name)[1]', 'varchar(50)')='error_reported'
	AND  n.value('(@timestamp)[1]', 'datetime') >= DATEADD(mi,-15,GETDATE());

	
SELECT @ServerAlertsXml=
	(SELECT 
	'Error Check' AS Provider
	,e.error AS Name
	,e.[TimeStamp] AS FailDateTime
	,e.Message AS ErrorInfo
	,CASE 
		WHEN e.severity =13 THEN 'Info'
		WHEN e.severity =17 THEN 'Warn'
		WHEN e.severity IN (21,22,23,24) THEN 'Err'
		ELSE 'Warn'
		END	AS ErrorLevel 
	FROM #events AS e
	WHERE e.[TIMESTAMP] >= DATEADD(mi,-15,GETDATE())
	AND (e.severity=13
		OR e.severity=17
		OR e.severity>=21)
	FOR XML RAW('Alert'), ROOT('Alerts')
	);

EXECUTE ADMIN.dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML;
EXECUTE ADMIN.dbo.glados_ArchiveServerAlerts 'Error Check';

DROP TABLE #events

END

GO
