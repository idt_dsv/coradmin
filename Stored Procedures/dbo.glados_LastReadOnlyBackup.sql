SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE proc [dbo].[glados_LastReadOnlyBackup]

as

set nocount on

/*-------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 12/22/2011

Purpose:
	This proc runs locally every night to see if we have current backups
	of all read_only databases.

Objects:
	master.sys.databases
	msdb.dbo.backupset
	Nearstore
	GLaDOS

Revisions:

-------------------------------------------------------------------------*/

---------------------------------------------------------------------------
-- Create temp tables.
---------------------------------------------------------------------------
create table #Local
	(DatabaseName		varchar(500)
	, LastFullBackup	datetime
	, BackupFile		varchar(500)
	, BackupPath		varchar(max))

create table #Remote
	(BackupFile		varchar(500)
	, BackupPath		varchar(max))

---------------------------------------------------------------------------
-- Declare and set variables.
---------------------------------------------------------------------------
declare @Start			int
	, @End				int
	, @SQL				varchar(max)
	, @Result			varchar(50)
	, @XML				xml

---------------------------------------------------------------------------
-- Get a list of all .bak files from the Read_Only path.
---------------------------------------------------------------------------
select @SQL = 'xp_cmdshell ''dir /B /N /S "' + BackupPath + '*.bak"'''
from [Admin].dbo.Backup_Paths with (nolock)
where Location = 'ReadOnly'

insert into #Remote
	(BackupPath)
exec(@SQL)

---------------------------------------------------------------------------
-- Grab the last full backup info for all read_only databases.
---------------------------------------------------------------------------
insert into #Local
	(DatabaseName
	, LastFullBackup)
select BS.database_name
	, max(BS.backup_start_date) as LastFullBackup
from master.sys.databases as D with (nolock)
inner join msdb.dbo.backupset as BS with (nolock)
	on D.name = BS.database_name
	and D.recovery_model_desc = BS.recovery_model collate SQL_Latin1_General_CP1_CI_AS
where D.state_desc = 'ONLINE'
	and D.is_in_standby = 0 -- Not in Standby
	and D.is_read_only = 1
	and BS.is_copy_only = 0
	and BS.type = 'D'
	and BS.backup_finish_date is not null
group by BS.database_name
	, BS.recovery_model

-- Load any read_only databases that have never been backed up.
insert into #Local
	(DatabaseName)
select D.name
from master.sys.databases as D with (nolock)
where D.state_desc = 'ONLINE'
	and D.is_in_standby = 0 -- Not in Standby
	and D.is_read_only = 1
	and D.name <> 'tempdb'
	and D.name not in
(
	select DatabaseName
	from #Local with (nolock)
)

-- Lookup the checkpoint_lsn of the last full backup.
-- This lookup is required as the lsn rolls over after numeric(25,0)
update #Local
set BackupPath = BMF.physical_device_name
from #Local as L with (nolock)
inner join msdb.dbo.backupset as BS with (nolock)
	on L.DatabaseName = BS.database_name
	and L.LastFullBackup = BS.backup_start_date
inner join msdb.dbo.backupmediafamily as BMF with (nolock)
	on BS.media_set_id = BMF.media_set_id

---------------------------------------------------------------------------
-- Grab the backup file name from the full path.
---------------------------------------------------------------------------
update #Remote
set BackupFile = right(BackupPath,charindex('\',reverse(BackupPath))-1)
where charindex('\',BackupPath) > 0

update #Local
set BackupFile = right(BackupPath,charindex('\',reverse(BackupPath))-1)
where charindex('\',BackupPath) > 0


select *
from #Local


---------------------------------------------------------------------------
-- Build all the missing read_only database backups into XML.
---------------------------------------------------------------------------
select @XML =
(select 'Current Read_Only Backup' as Provider
	, L.DatabaseName as Name
	, getdate() as FailDateTime
	, 'Last Full Backup: ' + convert(varchar,L.LastFullBackup,121)
		+ ' | Last Backup Path: ' + L.BackupPath as ErrorInfo
	, 'WARN' as ErrorLevel
from #Local as L with (nolock)
left outer join #Remote as R with (nolock)
	on L.BackupFile = R.BackupFile
where R.BackupFile is null
for xml raw('Alert'), root('Alerts'))

---------------------------------------------------------------------------
-- Sync up GLaDOS.
---------------------------------------------------------------------------
-- Flag all Point in Time Recovery records as no longer current.
exec [Admin].dbo.glados_ServerAlertsMarkInactive @Provider = 'Current Read_Only Backup'

-- Update current Point in Time Recovery records and insert new ones.
exec [Admin].dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML = @XML

-- Archive and remove any Point in Time Recovery records that are no longer current.
exec [Admin].dbo.glados_ArchiveServerAlerts @Provider = 'Current Read_Only Backup'

---------------------------------------------------------------------------
-- Clean up.
---------------------------------------------------------------------------
drop table #Local
drop table #Remote
---------------------------------------------------------------------------
---------------------------------------------------------------------------

GO
