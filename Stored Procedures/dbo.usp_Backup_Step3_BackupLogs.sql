SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE proc [dbo].[usp_Backup_Step3_BackupLogs]
	@Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 10/23/2009

Purpose: 
	Backup each database and verify the backup.
	
Revisions:
	10/27/2009 - Andrew Gould
		Added transaction log backups to process.
		Added better error handling.
	3/23/2011 - Barry Randall
		Added compression for 2008 R2 servers
	4/28/2011 - Travis Tittle
		Added Brackets to file build, in order to capture entire Database Name.
		Removed a REPLACE within the file build
		Added a check before the Log Backups are ran, to see if there are any databases
		present without full backups, or that have changed to a Full Recovery Mode.  
		These databases will receive a FULL backup prior to the Log Backup.
	6/06/2011 - Travis Tittle
		Now attempting to backup only those db's with a status of ONLINE.
	6/16/2011 - Travis Tittle
		Removed reliance on Backup Devices.
	12/2/2011 - Andrew Gould
		Removed the explicit ignoring of model.  While the trns will be 1k
		and contain no useful information, there is no reason to have an
		exception to FULL backups.
	12/13/2011 - Travis Tittle
		Added BackupGroup
	01/03/2012 - Travis Tittle
		Revised so new SIMPLE db's will be backed up by Log Backup step as well.  This
		was to co-exist with Point In Time tracker.
	02/21/2012 - Travis Tittle
		Added Bulk_Logged log backups
	07/05/2012 - Travis Tittle
		Added portion to Full Backups to make directory if absent.
	07/17/2012 - Travis Tittle
		Removed Full Backup portion.  This step will only create log backups if log backups are necessary.
	02/06/2014 - Travis Tittle
		Removed Compression flag, as New Build Doc requires Compression already be set in sys.configurations.  Also fixed Recovery Model changes from Full to Simple being triggered for new		fulls.
To Do:
	None
	
Notes:
	For a log to be backed up the following conditions must be met.
	1.) It must be flagged to be backed up (DontBackup = 0)
	2.) It must be in full recovery mode (RecoveryModel = 'FULL')
	3.) It must have a log file set (LastLogBackupFile <> '-1')
	4.) It must have a full backup already.

------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp table.
--------------------------------------------------------------------------------
create table #LogBackup
	(BackupID int identity(1,1)
	, BackupType varchar(10)
	, DatabaseLogID int
	, BackupLogCommand varchar(500)
	, VerificationLogCommand varchar(500))

--------------------------------------------------------------------------------
-- Declare Variables.
--------------------------------------------------------------------------------
declare @Start2 int
	, @End2 int
	, @DatabaseLogID int
	, @BackupLogCommand varchar(500)
	, @VerificationLogCommand varchar(500)
	, @BackupLogError int
	, @VerificationLogError int
	, @Today datetime

select @BackupLogError = -1
select @VerificationLogError = -1
select @Today = getdate()
	
--------------------------------------------------------------------------------
-- If Full Recovery Model and Full Backup has yet to be created, or if Database
-- has been newly changed to a Full Recovery Model, edit metadata to alert step a full needs created first.
-----------------------------------------------------
update ADMIN.dbo.Backup_Databases
   set NeedsFull = 1
from ADMIN.dbo.Backup_Databases as D with (nolock)
where (D.DontBackup = 0
and D.RecoveryModel = 'FULL'
and D.LastLogBackupFile = '-1'
and D.Database_Status = 'ONLINE'
and D.BackupGroup = 1
and D.LastBackupDate = '1900-01-01 00:00:00.000')
OR (D.DontBackup = 0
and D.RecoveryModel = 'BULK_LOGGED'
and D.LastLogBackupFile = '-1'
and D.Database_Status = 'ONLINE'
and D.BackupGroup = 1
and D.LastBackupDate = '1900-01-01 00:00:00.000')
 OR D.DontBackup = 0
 and D.RecoveryModel = 'SIMPLE'
 and D.LastLogBackupFile = '-1'  --so recovery model changes from Full to Simple dont trigger new FULL
 and D.Database_Status = 'ONLINE'
 and D.BackupGroup = 1
 and D.LastBackupDate = '1900-01-01 00:00:00.000'

--------------------------------------------------------------------------------
-- Loads backup and restore commands into #LogBackup for log backups.
-- Note: The database must be flagged to be backed up (DontBackup = 0)
--	It must be in full recovery mode (RecoveryModel = 'FULL')
--  And it must have a log file set (LastLogBackupFile <> '-1')
--------------------------------------------------------------------------------
update ADMIN.dbo.Backup_Databases
set LastLogBackupFile = P.BackupPath
		+ D.FolderName 
		+ '\'
		+ D.DatabaseName
		+ '_'
		-- This jumbled mess takes a date and trims of the seconds and miliseconds and removes all formating.
		+ left(replace(replace(replace(convert(varchar,@Today,121),' ',''),':',''),'-',''),12) 
		+ '.trn'
from ADMIN.dbo.Backup_Databases as D with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on D.LocalPathID = P.PathID
where (D.DontBackup = 0
	and D.RecoveryModel = 'FULL'
	and D.Database_Status = 'ONLINE'
	and D.BackupGroup = 1
	and D.NeedsFull = 0)
	OR (D.DontBackup = 0
	and D.RecoveryModel = 'BULK_LOGGED'
	and D.Database_Status = 'ONLINE'
	and D.BackupGroup = 1
	and D.NeedsFull = 0)

insert into #LogBackup
	(DatabaseLogID
	, BackupLogCommand
	, VerificationLogCommand)
select DatabaseID
	, 'BACKUP LOG [' 
		+ DatabaseName 
		+ '] TO DISK = '''
		+ LastLogBackupFile
		+ ''''
	, 'RESTORE VERIFYONLY FROM DISK = '''
		+ LastLogBackupFile
		+ ''''
from ADMIN.dbo.Backup_Databases as D with (nolock)
where (DontBackup = 0
	and RecoveryModel = 'FULL'
	and LastLogBackupFile <> '-1'
	and Database_Status = 'ONLINE'
	And BackupGroup = 1
	and D.NeedsFull = 0)
	OR (DontBackup = 0
	and RecoveryModel = 'BULK_LOGGED'
	and LastLogBackupFile <> '-1'
	and Database_Status = 'ONLINE'
	And BackupGroup = 1
	and D.NeedsFull = 0)

--------------------------------------------------------------------------------
-- ***DEBUG***
-- If debug flag is set, return the list of commands to run, but don't
-- execute them.
------------------------------------------------------------------------------	
if @Debug = 1
begin
	select * from #LogBackup with (nolock)

	return
end

--------------------------------------------------------------------------------
-- Walk through #Backup.  Attempt to backup the database, if that works attempt
-- to verify it.  If verification passes update ADMIN.dbo.Backup_Databases
-- with the time of completion.  Else throw an error.
--------------------------------------------------------------------------------
select @Start2 = min(BackupID) from #LogBackup with (nolock)
select @End2 = max(BackupID) from #LogBackup with (nolock)
	
while @Start2 <= @End2
begin
	select @DatabaseLogID = DatabaseLogID
		, @BackupLogCommand = BackupLogCommand
		, @VerificationLogCommand = VerificationLogCommand
	from #LogBackup with (nolock)
	where BackupID = @Start2
	
	exec (@BackupLogCommand)
	select @BackupLogError = @@error
	
	if @BackupLogError = 0
	begin
		exec (@VerificationLogCommand)
		select @VerificationLogError = @@error
		
		if @VerificationLogError = 0
		begin
			update ADMIN.dbo.Backup_Databases
			set LastLogBackupDate = getdate()
			where DatabaseID = @DatabaseLogID
		end
		else
		begin
			insert into ADMIN.dbo.Backup_ErrorLog
				(Command
				, Error
				, ErrorDate)
			select @VerificationLogCommand
				, 'ERROR: ' + @VerificationLogError
				, @Today
		end
	end
	else
	begin
		insert into ADMIN.dbo.Backup_ErrorLog
			(Command
			, Error
			, ErrorDate)
		select @BackupLogCommand
			, 'ERROR: ' + @BackupLogError
			, @Today
	end
	
	select @Start2 = @Start2 + 1
end

--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #LogBackup
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------















GO
