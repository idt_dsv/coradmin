SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[glados_ReplicationInfo]
AS 
SET NOCOUNT ON;
/*
Data Services: John Wood
CreateDate:1-11-2012
Reason: Replication monitor v2
Revision:2
To Do:

Updated to use Admin.dbo.glados_ServerAlertsUpsertXML for improved alert management.
Updated to include log agent status.
*/

CREATE TABLE #RESULTS
(
	Provider varchar(128)
	, Name varchar(128)
	, ErrorInfo varchar(max)
	, ErrorLevel char(4)
	, FailDateTime datetime
)

DECLARE @GladosUpdate XML

EXECUTE distribution.sys.sp_replmonitorrefreshjob @iterations = 1

INSERT INTO #RESULTS
(
	Provider
	, Name
	, ErrorInfo
	, ErrorLevel
	, FailDateTime
)
SELECT 
	 'Replication' AS [Provider]
	,MDA.publication  AS[Name]
	,'Agent:'+Mda.name 
	+' Status:'+CASE 
		RMD.status
		WHEN 1 THEN 'Started'
		WHEN 2 THEN 'Succeeded'
		WHEN 3 THEN 'In progress'
		WHEN 4 THEN 'Idle'
		WHEN 5 THEN 'Retrying'
		WHEN 6 THEN 'Failed'
		ELSE 'ERROR'
	END 
	+' Latency(sec):'+CAST(ISNULL(RMD.cur_latency,0) AS CHAR(5)) AS [ErrorInfo]
	,CASE 
		WHEN RMD.warning <> 0 THEN 'WARN'
		WHEN RMD.cur_latency >=300 AND RMD.cur_latency < 600 THEN 'INFO' /*5-10 min*/
		WHEN RMD.cur_latency >= 600 AND RMD.cur_latency < 1200 THEN 'WARN' /*10-20 Min*/
		WHEN RMD.cur_latency >= 1200 THEN 'ERR' /*>20 Min*/
		WHEN RMD.status =5  THEN 'INFO'
		WHEN RMD.status =6  THEN 'ERR'
		ELSE 'INFO'
	END AS [ErrorLevel]
	,GETDATE() AS [FailTime]
FROM distribution.dbo.MSdistribution_agents AS mda WITH (NOLOCK)
INNER JOIN distribution.dbo.MSreplication_monitordata AS RMD WITH (NOLOCK)
	ON  MDA.id = RMD.agent_id
WHERE 
	RMD.cur_latency >=60 
	OR RMD.status >=5 
	OR RMD.warning <> 0

UNION

SELECT 
	 'Replication' AS [Provider]
	,MLA.publisher_db  AS[Name]
	,'Agent:'+MLA.name 
	+' Status:'+CASE 
		MLH.runstatus
		WHEN 1 THEN 'Started'
		WHEN 2 THEN 'Succeeded'
		WHEN 3 THEN 'In progress'
		WHEN 4 THEN 'Idle'
		WHEN 5 THEN 'Retrying'
		WHEN 6 THEN 'Failed'
		ELSE 'ERROR'
	END AS [ErrorInfo]
	,CASE 
		WHEN MLH.runstatus = 5  THEN 'INFO'
		WHEN MLH.runstatus = 6  THEN 'ERR'
		ELSE 'INFO'
	END AS [ErrorLevel]
	,GETDATE() AS [FailTime]
FROM distribution.dbo.MSlogreader_agents AS MLA WITH (NOLOCK)
INNER JOIN distribution.dbo.MSlogreader_history AS MLH WITH (NOLOCK)
	ON MLA.id = MLH.agent_id
WHERE
	MLH.runstatus >= 5
	AND MLH.timestamp =
	(
		SELECT MAX(timestamp)
		FROM distribution.dbo.MSlogreader_history WITH (NOLOCK)
		WHERE agent_id = MLA.id
	)

SELECT @GladosUpdate =
(
	SELECT *
	FROM #RESULTS
	FOR XML RAW('Alert'), ROOT('Alerts')
)

SELECT @GladosUpdate

EXECUTE [Admin].dbo.glados_ServerAlertsMarkInactive 'Replication'
EXECUTE [Admin].dbo.glados_ServerAlertsUpsertXML @GladosUpdate
EXECUTE [Admin].dbo.glados_ArchiveServerAlerts 'Replication'

DROP TABLE #RESULTS
GO
