SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE proc [dbo].[ScriptToFile]
	@StoredProc		varchar(256)
	, @Path			varchar(150)
	, @Folder		varchar(256)
	, @FileName		varchar(150)

as
/*--------------------------------------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 4/15/2011

Purpose:
	This is a wrapper proc, that will take any stored procedure and write its results to a file based on the
	passed parameters.

Example:
	execute [Admin].dbo.usp_ScriptToFile
	@StoredProc	= '[Admin].dbo.Permissions_Script'
	, @Path		= '\\ProdSQL5\BCP_SAVE\'
	, @Folder	= '\DatabasePermissions\'
	, @FileName = 'Permissions_Script'

	This will run [Admin].dbo.usp_Permissions and save the output to:
		\\ProdSQL5\BCP_SAVE\##SERVERNAME##\DatabasePermissions\Permissions_Script_YYMMDD.sql
---------------------------------------------------------------------------------------------------------------*/

/*--------------------------------------------------------------------------------------------------------------
-- Development
declare @StoredProc	varchar(256)
	, @Path			varchar(150)
	, @Folder		varchar(256)

select @StoredProc	= '[Admin].dbo.Permissions_Script'
	, @Path			= '\\ProdSQL5\BCP_SAVE\'
	, @Folder		= '\DatabasePermissions\'
	, @FileName = 'Permissions_Script'

---------------------------------------------------------------------------------------------------------------*/

declare @ServerName	varchar(256)
	, @Date			char(8)
	, @SQL			varchar(1000)

-- Make sure the passed path and folder values lead and end with '\'
if substring(@Path,1,1) <> '\'
	select @Path = '\' + @Path

if substring(@Path,len(@Path),1) <> '\'
	select @Path = @Path + '\'

if substring(@Folder,1,1) <> '\'
	select @Folder = '\' + @Folder

if substring(@Folder,len(@Folder),1) <> '\'
	select @Folder = @Folder + '\'

select @ServerName	= replace(@@servername,'\','_')
	, @Date			= replace(substring(convert(varchar,getdate(),121),0,11),'-','') -- Turns getdate() into YYYYMMDD

-- Build the directory just in case it doesn't exist.
select @SQL = 'md ' + @Path + @ServerName + @Folder

exec xp_cmdshell @SQL

-- Run the stored proc and output the results to the file specified.
select @SQL = 'sqlcmd -S ' + @@servername + ' -Q"' + @StoredProc + '" -o ' + @Path + @ServerName + @Folder + @FileName + '_' + @Date + '.sql'

exec xp_cmdshell @SQL
-----------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------

GO
