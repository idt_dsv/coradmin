SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE proc [dbo].[Permissions_Load]
	@Debug int = 0

as

set nocount on

/*-------------------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 5/13/2011

Purpose:
	This proc decodes all permissions for every database on the server.

Objects:
	[Admin].dbo.Permissions_Server
	[Admin].dbo.Permissions_Databases
	master.sys.server_principals
	master.sys.server_permissions
	master.sys.server_role_members
	*.sys.database_principals
	*.sys.database_permissions
	*.sys.database_role_members
	*.sys.objects
	*.sys.schemas

Parameters:
	@Debug Options:
		0 - Executes as normal, this is the default setting.
		1 - Print out all dynamic sql statements for troubleshooting, doesn't execute any of them.
		2 - Executes as normal, prints out execution steps and times as well as returning all result sets.

Exclusion List:
	The following accounts are hardcoded to be excluded.

	For database user permissions:
		('dbo','guest','INFORMATION_SCHEMA','sys')
	For database level permissions:
		('dbo')
	For database role permissions:
		('dbo')

Notes:
	This process checks to see if sp_help_revlogin exists, if not it will attempt to build
	it from \\PRODSQL4\DataServices\sp_help_revlogin.sql.  Should this file move or path
	change this process will need to be updated.
-------------------------------------------------------------------------------------------*/

---------------------------------------------------------------------------------------------
-- Permanent tables.
---------------------------------------------------------------------------------------------
if (select count(*) from [Admin].sys.objects where name = 'Permissions_Server' and type = 'U') <> 1
begin
	create table [Admin].dbo.Permissions_Server
		(DisplayOrder			int
		, PermissionType		varchar(100)
		, PrincipalName			varchar(150)
		, PrincipalSID			varbinary(85)
		, Command				varchar(max)
		, LoadDate				datetime default getdate())
end

if (select count(*) from [Admin].sys.objects where name = 'Permissions_Databases' and type = 'U') <> 1
begin
	create table [Admin].dbo.Permissions_Databases
		(DisplayOrder			int
		, DatabaseName			varchar(150)
		, PermissionType		varchar(100)
		, PrincipalName			varchar(150)
		, PrincipalSID			varbinary(85)
		, Command				varchar(max)
		, LoadDate				datetime default getdate())

	create nonclustered index IX_PrincipalSID on [Admin].dbo.Permissions_Databases (PrincipalSID)
end


if (select count(*) from [Admin].sys.objects where name = 'Permissions_ActiveDirectoryGroupMembership' and type = 'U') <> 1
begin
	create table [Admin].dbo.Permissions_ActiveDirectoryGroupMembership
		(GroupName				varchar(150)
		, PrincipalName			varchar(150)
		, FullName				varchar(150)
		, ActiveDirectorySDDL	varchar(150)
		, LoadDate				datetime default getdate())
end

if (select count(*) from [Admin].sys.objects where name = 'Permissions_WindowsLogins' and type = 'U') <> 1
begin
	create table [Admin].dbo.Permissions_WindowsLogins
		(GroupName				varchar(150)
		, PrincipalName			varchar(150)
		, PrincipalSID			varbinary(85)
		, ActiveDirectorySDDL	varchar(150)
		, LoadDate				datetime default getdate())
end
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------
-- Create temp tables.
---------------------------------------------------------------------------------------------
create table #SP_Help_RevLogin
	(Command					varchar(5000))

create table #Databases
	(LoopID						int identity(1,1)
	, DatabaseName				varchar(150))

create table #PermissionTypes
	(DatabaseName				varchar(150)
	, ClassDesc					nvarchar(60))

create table #Logging
	(LogID						int identity(1,1)
	, StepName					varchar(200)
	, StartTime					datetime
	, EndTime					datetime
	, ExecutionTimeMS			int
	, RecordCount				int)

/*-- Development ----------------------------------------------------------------------------
declare @Debug int

select @Debug					= 0
-------------------------------------------------------------------------------------------*/

---------------------------------------------------------------------------------------------
-- Declare variables.
---------------------------------------------------------------------------------------------
declare @DatabaseName			varchar(150)
	, @LoadDate					datetime
	, @QuadQuote				varchar(10)
	, @SQL						varchar(max)

-- Loop variables
declare @Start					int
	, @End						int

-- Logging variables
declare @StartTime				datetime
	, @EndTime					datetime
	, @RecordCount				int

-- External script variables
declare @LocalScriptPath		varchar(100)

select @LoadDate				= getdate()

-- To surround a name in quotes using the quotename function you need to pass it '''', which
-- in dynamic sql would require eight tick marks.  For readablilty I use the variable below.
select @QuadQuote				= char(39)+char(39)+char(39)+char(39)

-- Lookup local script path.
select @LocalScriptPath	= ScriptPath
from [Admin].dbo.Script_Paths with (nolock)
where Location = 'Local'

---------------------------------------------------------------------------------------------
-- Make sure required scripts are loaded locally.
---------------------------------------------------------------------------------------------
exec [Admin].dbo.Script_Load 'Permissions_WindowsLookup.ps1'

exec [Admin].dbo.Script_Load 'sp_help_revlogin.sql'

---------------------------------------------------------------------------------------------
-- Truncate the Permissions_* tables before loading in new data.
---------------------------------------------------------------------------------------------
truncate table [Admin].dbo.Permissions_Server
truncate table [Admin].dbo.Permissions_Databases
truncate table [Admin].dbo.Permissions_ActiveDirectoryGroupMembership
truncate table [Admin].dbo.Permissions_WindowsLogins

/*******************************************************************************************/
---------------------------------------------------------------------------------------------
-- SERVER PERMISSIONS.
---------------------------------------------------------------------------------------------
/*******************************************************************************************/

-- Set Powershell to be able to execute local unsigned scripts.
exec xp_cmdshell 'powershell.exe Set-ExecutionPolicy RemoteSigned'

-- Run Permissions_WindowsLookup.ps1 powershell script to grab and decode all windows logins
-- and Active Directory groups.
select @SQL = 'xp_cmdshell ''powershell.exe ' + @LocalScriptPath + '\Permissions_WindowsLookup.ps1 '
	+ @@servername + ' ' -- Source
	+ @@servername + '''' -- Destination

if @Debug = 1
begin
	print '-- Run Permissions_WindowsLookup.ps1'
	print @SQL
	print ''
end
else
begin
	exec(@SQL)
end

-- If the server doesn't have the two stored procs for sp_help_revlogin to work run the script
-- to build them.
if (select count(*) from master.sys.objects where name='SP_help_revLogin' or name='sp_hexadecimal') <> 2
begin
	select @SQL = 'xp_cmdshell ''sqlcmd -S ' + @@servername + ' -i ' + @LocalScriptPath + '\sp_help_revlogin.sql'''

	if @Debug = 1
	begin
		print '-- Load sp_help_revlogin'
		print @SQL
		print ''
	end
	else
	begin
		exec(@SQL)
	end
end

---------------------------------------------------------------------------------------------
-- Load server logins.
---------------------------------------------------------------------------------------------
select @StartTime = getdate()

select @SQL = 'xp_cmdshell ''sqlcmd -S ' + @@servername + ' -Q "master.dbo.sp_help_revlogin"'''

if @Debug = 1
begin
	print '-- Run sp_help_revlogin'
	print @SQL
	print ''
end
else
begin
	insert into #SP_Help_RevLogin
	exec(@SQL)

	select @RecordCount = @@rowcount

	select @EndTime = getdate()
	insert into #Logging
		(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
	select 'Load server logins.'
		, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
		, @RecordCount as RecordCount
end

select @StartTime = getdate()

-- Delete junk.
delete from #SP_Help_RevLogin
where Command = ''
	or Command like '-- Login:%'

select @RecordCount = @@rowcount

select @EndTime = getdate()
insert into #Logging
	(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
select 'Delete junk.'
	, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
	, @RecordCount as RecordCount

---------------------------------------------------------------------------------------------
-- Load use and go commands.
---------------------------------------------------------------------------------------------
insert into [Admin].dbo.Permissions_Server
	(DisplayOrder
	, PermissionType
	, Command
	, LoadDate)
values (0
	, 'Header'
	, 'USE [master]'
	, @LoadDate)

---------------------------------------------------------------------------------------------
-- Add Logins.
---------------------------------------------------------------------------------------------
select @StartTime = getdate()

insert into [Admin].dbo.Permissions_Server
	(DisplayOrder
	, PermissionType
	, Command
	, LoadDate)
values (1
	, 'Header'
	, '-- Add Logins ----------------------------------------------------------------'
	, @LoadDate)

insert into [Admin].dbo.Permissions_Server
	(DisplayOrder
	, PermissionType
	, PrincipalName
	, PrincipalSID
	, Command
	, LoadDate)
select 2 as DisplayOrder
	, 'Add Logins' as PermissionType
	, Member.name as PrincipalName
	, Member.sid as PrincipalSID
	, HRL.Command
	, @LoadDate
from master.sys.server_principals as Member with (nolock)
inner join #SP_Help_RevLogin as HRL with (nolock)
	on HRL.Command like '%[[]' + Member.name + ']%'
where Member.type_desc in ('SQL_LOGIN','WINDOWS_LOGIN','WINDOWS_GROUP')

select @RecordCount = @@rowcount

select @EndTime = getdate()
insert into #Logging
	(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
select 'Add Logins.'
	, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
	, @RecordCount as RecordCount

---------------------------------------------------------------------------------------------
-- Server Permissions.
---------------------------------------------------------------------------------------------
select @StartTime = getdate()

insert into [Admin].dbo.Permissions_Server
	(DisplayOrder
	, PermissionType
	, Command
	, LoadDate)
values (3
	, 'Header'
	, '-- Server Permissions -----------------------------------------------------'
	, @LoadDate)

insert into [Admin].dbo.Permissions_Server
	(DisplayOrder
	, PermissionType
	, PrincipalName
	, PrincipalSID
	, Command
	, LoadDate)
select 4 as DisplayOrder
	, 'Server Permissions' as PermissionType
	, Member.name as PrincipalName
	, Member.sid as PrincipalSID
	, case
		when Perm.state <> 'W' then Perm.state_desc
		else 'GRANT' end
	+ ' ' + Perm.permission_name
	+ case
		when Perm.class_desc = 'SERVER' then ''
		else ' ON LOGIN::' + Slave.name end
	+ ' TO [' + Member.name + ']' collate database_default
	+ case
		when Perm.state <> 'W' then ''
		else 'WITH GRANT OPTION' end as Command
	, @LoadDate
from master.sys.server_permissions as Perm with (nolock)
inner join master.sys.server_principals as Member with (nolock)
	on Perm.grantee_principal_id = Member.principal_id
left outer join master.sys.server_principals as Slave with (nolock)
	on Perm.major_id = Slave.principal_id
where Perm.class_desc in ('SERVER','SERVER_PRINCIPAL')
	and Member.name in (select SP.PrincipalName from [Admin].dbo.Permissions_Server as SP with (nolock))

select @RecordCount = @@rowcount

select @EndTime = getdate()
insert into #Logging
	(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
select 'Server Permissions.'
	, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
	, @RecordCount as RecordCount

---------------------------------------------------------------------------------------------
-- Server Role Membership.
---------------------------------------------------------------------------------------------
select @StartTime = getdate()

insert into [Admin].dbo.Permissions_Server
	(DisplayOrder
	, PermissionType
	, Command
	, LoadDate)
values (5
	, 'Header'
	, '-- Server Role Membership ----------------------------------------------------------'
	, @LoadDate)

insert into [Admin].dbo.Permissions_Server
	(DisplayOrder
	, PermissionType
	, PrincipalName
	, PrincipalSID
	, Command
	, LoadDate)
select 6 as DisplayOrder
	, 'Server Role Membership' as PermissionType
	, Member.name as PrincipalName
	, Member.sid as PrincipalSID
	, 'EXEC sp_addsrvrolemember @rolename = '
	+ quotename(Role.name,'''')
	+ ', @loginame = '
	+ quotename(Member.name,'''') as Command
	, @LoadDate
from master.sys.server_role_members as RM with (nolock)
inner join master.sys.server_principals as Role with (nolock)
	on RM.role_principal_id = Role.principal_id
inner join master.sys.server_principals as Member with (nolock)
	on RM.member_principal_id = Member.principal_id
where Member.name in (select SP.PrincipalName from [Admin].dbo.Permissions_Server as SP with (nolock))

select @RecordCount = @@rowcount

select @EndTime = getdate()
insert into #Logging
	(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
select 'Server Role Membership.'
	, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
	, @RecordCount as RecordCount

/*******************************************************************************************/
---------------------------------------------------------------------------------------------
-- DATABASE PERMISSIONS.
---------------------------------------------------------------------------------------------
/*******************************************************************************************/

---------------------------------------------------------------------------------------------
-- Load databases to be scanned.
---------------------------------------------------------------------------------------------
select @StartTime = getdate()

-- Load list of all online databases except tempdb.
insert into #Databases
	(DatabaseName)
select '[' + name + ']'
from master.sys.databases with (nolock)
where name <> 'tempdb'
	and state_desc = 'ONLINE'

select @RecordCount = @@rowcount

select @EndTime = getdate()
insert into #Logging
	(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
select 'Load databases to be scanned.'
	, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
	, @RecordCount as RecordCount

---------------------------------------------------------------------------------------------
-- Loop through all databases.
---------------------------------------------------------------------------------------------
-- Set start and stop loop conditions.
select @Start = min(LoopID)
	, @End = max(LoopID)
from #Databases with (nolock)

while @Start <= @End
begin
	select @StartTime = getdate()

	select @DatabaseName = DatabaseName
	from #Databases with (nolock)
	where LoopID = @Start

	-- Load each databases's class_desc(s).  This serves as a lookup list to potentially reduce
	-- the number of scans required on each database.
	select @SQL =
	'select ''' + @DatabaseName + ''' as DatabaseName
	, class_desc as ClassDesc
	from ' + @DatabaseName + '.sys.database_permissions with (nolock)
	group by class_desc'

	insert into #PermissionTypes
	exec(@SQL)

	select @RecordCount = @@rowcount

	select @EndTime = getdate()
	insert into #Logging
		(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
	select 'Load ' + @DatabaseName + '''s class_desc(s).'
		, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
		, @RecordCount as RecordCount

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (0
		, @DatabaseName
		, 'Header'
		, 'USE ' + @DatabaseName + ''
		, @LoadDate)

	---------------------------------------------------------------------------------------------
	-- Add Users.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (1
		, @DatabaseName
		, 'Header'
		, '-- Add Users ----------------------------------------------------------------'
		, @LoadDate)

	select @SQL = 
	'select 2 as DisplayOrder
		, ''' + @DatabaseName + ''' as DatabaseName
		, ''Add Users'' as PermissionType
		, Member.name as PrincipalName
		, Member.sid as PrincipalSID
		, ''CREATE USER ['' + Member.name + '']'' as Command
		, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
	from ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
	where Member.type_desc in (''SQL_USER'',''WINDOWS_USER'',''WINDOWS_GROUP'')
		and Member.name not in (''dbo'',''guest'',''INFORMATION_SCHEMA'',''sys'')'

	if @Debug = 1
	begin
		print '-- Add Users'
		print @SQL
		print ''
	end
	else
	begin
		insert into [Admin].dbo.Permissions_Databases
		exec(@SQL)

		select @RecordCount = @@rowcount

		select @EndTime = getdate()
		insert into #Logging
			(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
		select 'Add users for ' + @DatabaseName + '.'
			, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
			, @RecordCount as RecordCount
	end

	---------------------------------------------------------------------------------------------
	-- Database Permissions.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (3
		, @DatabaseName
		, 'Header'
		, '-- Database Permissions -----------------------------------------------------'
		, @LoadDate)

	if (select count(*) from #PermissionTypes where DatabaseName = @DatabaseName and ClassDesc = 'DATABASE') > 0
	begin
		select @SQL =
		'select 4 as DisplayOrder
			, ''' + @DatabaseName + ''' as DatabaseName
			, ''Database Permissions'' as PermissionType
			, Member.name as PrincipalName
			, Member.sid as PrincipalSID
			, case
				when Perm.state <> ''W'' then Perm.state_desc
				else ''GRANT'' end
			+ '' '' + Perm.permission_name
			+ '' TO ['' + Member.name + '']'' collate database_default
			+ case
				when Perm.state <> ''W'' then ''''
				else ''WITH GRANT OPTION'' end as Command
			, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
		from ' + @DatabaseName + '.sys.database_permissions as Perm with (nolock)
		inner join ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
			on Perm.grantee_principal_id = Member.principal_id
		where Perm.class_desc = ''DATABASE''
			and Member.name <> ''dbo'''

		if @Debug = 1
		begin
			print '-- Database Permissions'
			print @SQL
			print ''
		end
		else
		begin
			insert into [Admin].dbo.Permissions_Databases
			exec(@SQL)

			select @RecordCount = @@rowcount

			select @EndTime = getdate()
			insert into #Logging
				(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
			select 'Database permissions for ' + @DatabaseName + '.'
				, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
				, @RecordCount as RecordCount
		end
	end

	---------------------------------------------------------------------------------------------
	-- Create Roles.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (5
		, @DatabaseName
		, 'Header'
		, '-- Create Roles -------------------------------------------------------------'
		, @LoadDate)

	select @SQL =
	'select 6 as DisplayOrder
		, ''' + @DatabaseName + ''' as DatabaseName
		, ''Create Roles'' as PermissionType
		, Member.name as PrincipalName
		, Member.sid as PrincipalSID
		, ''EXEC sp_addrole @rolename = ''
		+ quotename(Member.name,' + @QuadQuote + ')
		+ '', @ownername = ''
		+ quotename(Parent.name,' + @QuadQuote + ') as Command
		, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
	from ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
	inner join ' + @DatabaseName + '.sys.database_principals as Parent with (nolock)
		on Member.owning_principal_id = Parent.principal_id
	where Member.type_desc = ''DATABASE_ROLE''
		and Member.is_fixed_role = 0
		and Member.name <> ''public'''

	if @Debug = 1
	begin
		print '-- Create Roles'
		print @SQL
		print ''
	end
	else
	begin
		insert into [Admin].dbo.Permissions_Databases
		exec(@SQL)

		select @RecordCount = @@rowcount

		select @EndTime = getdate()
		insert into #Logging
			(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
		select 'Create roles for ' + @DatabaseName + '.'
			, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
			, @RecordCount as RecordCount
	end

	---------------------------------------------------------------------------------------------
	-- Role Membership.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (7
		, @DatabaseName
		, 'Header'
		, '-- Role Membership ----------------------------------------------------------'
		, @LoadDate)

	select @SQL =
	'select	8 as DisplayOrder
		, ''' + @DatabaseName + ''' as DatabaseName
		, ''Role Membership'' as PermissionType
		, Member.name as PrincipalName
		, Member.sid as PrincipalSID
		, ''EXEC sp_addrolemember @rolename = ''
		+ quotename(Role.name,' + @QuadQuote + ')
		+ '', @membername = ''
		+ quotename(Member.name,' + @QuadQuote + ') as Command
		, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
	from ' + @DatabaseName + '.sys.database_role_members as RM with (nolock)
	inner join ' + @DatabaseName + '.sys.database_principals as Role with (nolock)
		on RM.role_principal_id = Role.principal_id
	inner join ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
		on RM.member_principal_id = Member.principal_id
	where Member.name <> ''dbo'''

	if @Debug = 1
	begin
		print '-- Role Membership'
		print @SQL
		print ''
	end
	else
	begin
		insert into [Admin].dbo.Permissions_Databases
		exec(@SQL)

		select @RecordCount = @@rowcount

		select @EndTime = getdate()
		insert into #Logging
			(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
		select 'Role membership for ' + @DatabaseName + '.'
			, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
			, @RecordCount as RecordCount
	end

	---------------------------------------------------------------------------------------------
	-- Schema Permissions.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (9
		, @DatabaseName
		, 'Header'
		, '-- Schema Permissions -------------------------------------------------------'
		, @LoadDate)

	if (select count(*) from #PermissionTypes where DatabaseName = @DatabaseName and ClassDesc = 'SCHEMA') > 0
	begin
		select @SQL =
		'select	10 as DisplayOrder
			, ''' + @DatabaseName + ''' as DatabaseName
			, ''Schema Permissions'' as PermissionType
			, Member.name as PrincipalName
			, Member.sid as PrincipalSID
			, case
				when Perm.state <> ''W'' then Perm.state_desc
				else ''GRANT'' end
			+ '' '' + Perm.permission_name + '' ON SCHEMA::['' + S.name	+ '']''
			+ '' TO ['' + Member.name	+ '']'' collate database_default
			+ case
				when Perm.state <> ''W'' then ''''
				else ''WITH GRANT OPTION'' end as Command
			, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
		from ' + @DatabaseName + '.sys.database_permissions as Perm with (nolock)
		inner join ' + @DatabaseName + '.sys.schemas as S with (nolock)
			on Perm.major_id = S.schema_id
		inner join ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
			on Perm.grantee_principal_id = Member.principal_id
		where Perm.class_desc = ''SCHEMA'''

		if @Debug = 1
		begin
			print '-- Schema Permissions'
			print @SQL
			print ''
		end
		else
		begin
			insert into [Admin].dbo.Permissions_Databases
			exec(@SQL)

			select @RecordCount = @@rowcount

			select @EndTime = getdate()
			insert into #Logging
				(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
			select 'Schema permissions for ' + @DatabaseName + '.'
				, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
				, @RecordCount as RecordCount
		end
	end

	---------------------------------------------------------------------------------------------
	-- XML Schema Collection Permissions.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (11
		, @DatabaseName
		, 'Header'
		, '-- XML Schema Collection Permissions ----------------------------------------'
		, @LoadDate)

	if (select count(*) from #PermissionTypes where DatabaseName = @DatabaseName and ClassDesc = 'XML_SCHEMA_COLLECTION') > 0
	begin
		select @SQL =
		'select	12 as DisplayOrder
			, ''' + @DatabaseName + ''' as DatabaseName
			, ''XML Schema Collection Permissions'' as PermissionType
			, Member.name as PrincipalName
			, Member.sid as PrincipalSID
			, case
				when Perm.state <> ''W'' then Perm.state_desc
				else ''GRANT'' end
			+ '' '' + Perm.permission_name + '' ON XML SCHEMA COLLECTION::['' + S.name	+ '']'' + ''['' + X.name	+ '']''
			+ '' TO ['' + Member.name	+ '']'' collate database_default
			+ case
				when Perm.state <> ''W'' then ''''
				else ''WITH GRANT OPTION'' end as Command
			, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
		from ' + @DatabaseName + '.sys.database_permissions as Perm with (nolock)
		inner join ' + @DatabaseName + '.sys.xml_schema_collections as X with (nolock)
			on Perm.major_id = X.xml_collection_id
		inner join ' + @DatabaseName + '.sys.schemas as S with (nolock)
			on X.schema_id = S.schema_id
		inner join ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
			on Perm.grantee_principal_id = Member.principal_id
		where Perm.class_desc = ''XML_SCHEMA_COLLECTION'''

		if @Debug = 1
		begin
			print '-- XML Schema Collection Permissions'
			print @SQL
			print ''
		end
		else
		begin
			insert into [Admin].dbo.Permissions_Databases
			exec(@SQL)

			select @RecordCount = @@rowcount

			select @EndTime = getdate()
			insert into #Logging
				(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
			select 'XML Schema Collection permissions for ' + @DatabaseName + '.'
				, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
				, @RecordCount as RecordCount
		end
	end

	---------------------------------------------------------------------------------------------
	-- Object Permissions.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (13
		, @DatabaseName
		, 'Header'
		, '-- Object Permissions -------------------------------------------------------'
		, @LoadDate)

	if (select count(*) from #PermissionTypes where DatabaseName = @DatabaseName and ClassDesc = 'OBJECT_OR_COLUMN') > 0
	begin
		select @SQL =
		'select	14 as DisplayOrder
			, ''' + @DatabaseName + ''' as DatabaseName
			, ''Object Permissions'' as PermissionType
			, Member.name as PrincipalName
			, Member.sid as PrincipalSID
			, case
				when Perm.state <> ''W'' then Perm.state_desc
				else ''GRANT'' end
			+ '' '' + Perm.permission_name + '' ON ['' + S.name	+ ''].['' + O.name + '']''
			+ case
				when C.column_id is null then ''''
				else ''(['' + C.name + ''])'' end
			+ '' TO ['' + Member.name	+ '']'' collate database_default
			+ case
				when Perm.state <> ''W'' then ''''
				else ''WITH GRANT OPTION'' end as Command
			, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
		from ' + @DatabaseName + '.sys.database_permissions as Perm with (nolock)
		inner join ' + @DatabaseName + '.sys.objects as O with (nolock)
			on Perm.major_id = O.object_id
		inner join ' + @DatabaseName + '.sys.schemas as S with (nolock)
			on O.schema_id = S.schema_id
		inner join ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
			on Perm.grantee_principal_id = Member.principal_id
		left outer join ' + @DatabaseName + '.sys.columns as C with (nolock)
			on Perm.minor_id = C.column_id
			and Perm.major_id = C.object_id
		where Perm.class_desc = ''OBJECT_OR_COLUMN'''

		if @Debug = 1
		begin
			print '-- Object Permissions'
			print @SQL
			print ''
		end
		else
		begin
			insert into [Admin].dbo.Permissions_Databases
			exec(@SQL)

			select @RecordCount = @@rowcount

			select @EndTime = getdate()
			insert into #Logging
				(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
			select 'Object permissions for ' + @DatabaseName + '.'
				, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
				, @RecordCount as RecordCount
		end
	end

	---------------------------------------------------------------------------------------------
	-- Puppetmaster Permissions.
	---------------------------------------------------------------------------------------------
	select @StartTime = getdate()

	insert into [Admin].dbo.Permissions_Databases
		(DisplayOrder
		, DatabaseName
		, PermissionType
		, Command
		, LoadDate)
	values (15
		, @DatabaseName
		, 'Header'
		, '-- Puppetmaster Permissions -------------------------------------------------'
		, @LoadDate)

	if (select count(*) from #PermissionTypes where DatabaseName = @DatabaseName and ClassDesc = 'DATABASE_PRINCIPAL') > 0
	begin
		-- Permissions at the server scope can only be granted when the current database is master.
		insert into [Admin].dbo.Permissions_Databases
			(DisplayOrder
			, DatabaseName
			, PermissionType
			, Command
			, LoadDate)
		values (16
			, @DatabaseName
			, 'Header'
			, 'USE [master]'
			, @LoadDate)

		select @SQL =
		'select	17 as DisplayOrder
			, ''' + @DatabaseName + ''' as DatabaseName
			, ''Puppetmaster Permissions'' as PermissionType
			, Member.name as PrincipalName
			, Member.sid as PrincipalSID
			, case
				when Perm.state <> ''W'' then Perm.state_desc
				else ''GRANT'' end
			+ '' '' + Perm.permission_name + '' ON LOGIN::['' + Puppet.name	+ '']''
			+ '' TO ['' + Member.name	+ '']'' collate database_default
			+ case
				when Perm.state <> ''W'' then ''''
				else ''WITH GRANT OPTION'' end as Command
			, ''' + cast(@LoadDate as varchar) + ''' as LoadDate
		from ' + @DatabaseName + '.sys.database_permissions as Perm with (nolock)
		inner join ' + @DatabaseName + '.sys.database_principals as Puppet with (nolock)
			on Perm.major_id = Puppet.principal_id
		inner join ' + @DatabaseName + '.sys.database_principals as Member with (nolock)
			on Perm.grantee_principal_id = Member.principal_id
		where Perm.class_desc = ''DATABASE_PRINCIPAL'''

		if @Debug = 1
		begin
			print '-- Puppetmaster Permissions'
			print @SQL
			print ''
		end
		else
		begin
			insert into [Admin].dbo.Permissions_Databases
			exec(@SQL)

			select @RecordCount = @@rowcount

			select @EndTime = getdate()
			insert into #Logging
				(StepName, StartTime, EndTime, ExecutionTimeMS, RecordCount)
			select 'Puppetmaster permissions for ' + @DatabaseName + '.'
				, @StartTime as StartTime, @EndTime as EndTime, datediff(ms,@StartTime,@EndTime) as ExecutionTimeMS
				, @RecordCount as RecordCount
		end
	end

	-- Bump the counter to get the next database.
	select @Start = @Start + 1
end

---------------------------------------------------------------------------------------------
-- Debugging.
---------------------------------------------------------------------------------------------
if @Debug = 2
begin
	select *
	from #Logging

	select *
	from #SP_Help_RevLogin

	select *
	from #Databases

	select *
	from #PermissionTypes

	select *
	from [Admin].dbo.Permissions_Server

	select *
	from [Admin].dbo.Permissions_Databases
end

---------------------------------------------------------------------------------------------
-- Clean up.
---------------------------------------------------------------------------------------------
drop table #SP_Help_RevLogin
drop table #Databases
drop table #PermissionTypes
drop table #Logging
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
GO
