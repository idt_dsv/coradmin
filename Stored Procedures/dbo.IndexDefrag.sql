SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[IndexDefrag]
	@ParameterSetName		varchar(100)	= null
	, @DatabaseName			varchar(150)	= null
	, @ObjectName			varchar(500)	= null
	, @ExcludedDatabases	varchar(2500)	= null
	, @ExcludedObjects		varchar(max)	= null
	, @ExcludedIndexes		varchar(max)	= null
	, @ScanMode				varchar(15)		= null
	, @ForceNewScan			bit				= null
	, @MinFragmentation		smallint		= null
	, @MinPages				smallint		= null
	, @ImprovementThreshold	smallint		= null
	, @RebuildThreshold		smallint		= null
	, @RebuildIndexes		bit				= null
	, @TimeLimit			int				= null
	, @Delay				varchar(8)		= null
	, @ExecuteDays			varchar(100)	= null
	, @ExecuteTime			varchar(8)		= null
	, @Execute				bit				= null
	, @Debug				smallint		= null

as

set nocount on

/*---------------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 4-11-2011

Purpose:
	This proc scans through indexes, identifies fragmented ones and attempts to defrag them.

Variables:
	@ParameterSetName		varchar(100)	- Used to lookup sets of values from IndexDefrag_Parameters.
	, @DatabaseName			varchar(150)	- Do work against the specified database.
	, @ObjectName			varchar(500)	- Scan only the specified object.
	, @ExcludedDatabases	varchar(2500)	- Ignore all databases in this comma seperated list.
	, @ExcludedObjects		varchar(max)	- Ignore all objects in this comma seperated list.
	, @ExcludedIndexes		varchar(max)	- Ignore all indexes in this comma seperated list.
	, @ScanMode				varchar(15)		- Determines how indepth of a scan is performed.
	, @ForceNewScan			bit				- Skips already queued commands and does a fresh scan.
	, @MinFragmentation		smallint		- Ignores any indexes that have less fragmentation than this.
	, @MinPages				smallint		- Ignores any indexes that have less pages than this.
	, @ImprovementThreshold	smallint		- Determines how effective the reorganization must be in order to not trigger a rebuild.
	, @RebuildThreshold		smallint		- Determines what is the minimum fragmentation an index can have before it becomes a rebuild candidate.
	, @RebuildIndexes		bit				- Allows rebuild commands to be executed.
	, @TimeLimit			int				- The process will stop executing commands after this time in minutes has elapsed.
	, @Delay				varchar(8)		- Determines how long the server waits between command executions.
	, @ExecuteDays			varchar(100)	- Used in conjunction with @ExecuteTime to determine if now is an acceptable time to start executing commands.
	, @ExecuteTime			varchar(8)		- Used in conjunction with @ExecuteDays to determine if now is an acceptable time to start executing commands.
	, @Execute				bit				- Allows commands to be executed.
	, @Debug				smallint		- Prints out informational messages.

Objects:
	[Admin].dbo.IndexDefrag_Log
	[Admin].dbo.IndexDefrag_Parameters
	[Admin].dbo.IndexDefrag_Queue
	master.sys.dm_db_index_physical_stats

---------------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------------------
-- Permanent tables.
-----------------------------------------------------------------------------------------
create table [Admin].dbo.IndexDefrag_Parameters
	(ParameterID			int identity(1,1) primary key
	, ParameterSetName		varchar(100)
	, ExcludedDatabases		varchar(2500)
	, ExcludedObjects		varchar(max)
	, ExcludedIndexes		varchar(max)
	, MinFragmentation		smallint default 5
	, MinPages				smallint default 128
	, ImprovementThreshold	smallint default 5
	, RebuildThreshold		smallint default 30
	, TimeLimit				int default 240
	, Delay					varchar(8) default '00:00:05'
	, ExecuteDays			varchar(60)
	, ExecuteTime			varchar(8)
	, LastLoadDate			datetime default getdate())

create table [Admin].dbo.IndexDefrag_Log
	(LogID					int identity(1,1) primary key
	, DatabaseID			int
	, DatabaseName			varchar(150)
	, SchemaID				int
	, SchemaName			varchar(150)
	, ObjectID				int
	, ObjectName			varchar(150)
	, IndexID				int
	, IndexName				varchar(150)
	, CommandType			varchar(100)
	, DefragCommand			varchar(1000)
	, VerifyCommand			varchar(1000)
	, UpdateStatsCommand	varchar(1000)
	, DefragStartTime		datetime
	, DefragEndTime			datetime
	, StatsStartTime		datetime
	, StatsEndTime			datetime
	, Pages					int
	, PreFragmentation		float
	, PostFragmentation		float
	, LastLoadDate			datetime default getdate())

create table [Admin].dbo.IndexDefrag_Queue
	(QueueID				int identity(1,1) primary key
	, DatabaseID			int
	, DatabaseName			varchar(150)
	, SchemaID				int
	, SchemaName			varchar(150)
	, ObjectID				int
	, ObjectName			varchar(150)
	, IndexID				int
	, IndexName				varchar(150)
	, CommandType			varchar(100)
	, DefragCommand			varchar(1000)
	, VerifyCommand			varchar(1000)
	, UpdateStatsCommand	varchar(1000)
	, Fragmentation			float
	, Pages					int
	, FragmentationCount	int
	, LastLoadDate			datetime default getdate())
-----------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------*/

/****************************************************************************************
*****************************************************************************************
-- Step 1: Prepare temporary tables and variables.
*****************************************************************************************
****************************************************************************************/

-----------------------------------------------------------------------------------------
-- Create temporary tables.
-----------------------------------------------------------------------------------------
create table #Parameters
	(ParameterID			smallint
	, ParameterName			varchar(100)
	, ParameterValue		varchar(max))

create table #DatabaseList
	(DatabaseListID			int identity(1,1)
	, DatabaseID			int
	, DatabaseName			varchar(150))

create table #ExcludedDatabases
	(DatabaseName			varchar(150))

create table #ExcludedObjects
	(DatabaseName			varchar(150)
	, ObjectID				int
	, ObjectName			varchar(500))

create table #ExcludedIndexes
	(DatabaseName			varchar(150)
	, SchemaName			varchar(150)
	, ObjectName			varchar(150)
	, IndexName				varchar(150)
	, StringName			varchar(600)
	, Period1				smallint
	, Period2				smallint
	, Period3				smallint)

create table #ExecuteDays
	(ExecuteDay				varchar(10)
	, DayNumber				tinyint)

create table #IndexAnalysis
	(IndexAnalysisID		int identity(1,1)
	, DatabaseID			int
	, DatabaseName			varchar(150)
	, SchemaID				int
	, SchemaName			varchar(150)
	, ObjectID				int
	, ObjectName			varchar(150)
	, IndexID				int
	, IndexName				varchar(150)
	, IndexType				varchar(150)
	, AllowPageLocks		bit
	, CommandType			varchar(100)
	, DefragCommand			varchar(1000)
	, VerifyCommand			varchar(1000)
	, UpdateStatsCommand	varchar(1000)
	, PartitionNumber		int
	, Fragmentation			float
	, Pages					bigint
	, FragmentationCount	int
	, Completed				bit default 0
	, PendingRebuild		bit default 0
	, Excluded				bit default 0)

create table #PostFragmentation
	(PostFragmentation		float
	, PostPages				bigint
	, PostFragCount			int)

/*-----------------------------------------------------------------------------------------
-- Development
-----------------------------------------------------------------------------------------
declare @ParameterSetName	varchar(100)
	, @DatabaseName			varchar(150)
	, @ObjectName			varchar(500)
	, @ExcludedDatabases	varchar(2500)
	, @ExcludedObjects		varchar(max)
	, @ExcludedIndexes		varchar(max)
	, @ScanMode				varchar(15)
	, @ForceNewScan			bit
	, @MinFragmentation		smallint
	, @MinPages				smallint
	, @ImprovementThreshold	smallint
	, @RebuildThreshold		smallint
	, @RebuildIndexes		bit
	, @TimeLimit			int
	, @Delay				varchar(8)
	, @ExecuteDays			varchar(100)
	, @ExecuteTime			varchar(8)
	, @Execute				bit
	, @Debug				smallint

select @ParameterSetName	= 'Standard'
	, @DatabaseName			= null
	, @ObjectName			= null
	, @ExcludedDatabases	= null
	, @ExcludedObjects		= null
	, @ExcludedIndexes		= null
	, @ScanMode				= 'limited'
	, @ForceNewScan			= 0
	, @MinFragmentation		= 5
	, @MinPages				= 128
	, @ImprovementThreshold = 5
	, @RebuildThreshold		= 30
	, @RebuildIndexes		= 0
	, @TimeLimit			= 240
	, @Delay				= '00:00:05'
	, @ExecuteDays			= null
	, @ExecuteTime			= null
	, @Execute				= 0
	, @Debug				= 1
-----------------------------------------------------------------------------------------*/

-----------------------------------------------------------------------------------------
-- Declare and set variables.
-----------------------------------------------------------------------------------------
declare @Start				int
	, @End					int
	, @CommaPosition		int
	, @DatabaseID			int
	, @RecordCount			int
	, @SQL					varchar(max)
	, @CutOffTime			datetime
	, @IndexAnalysisID		int
	, @DefragCommand		varchar(1000)
	, @VerifyCommand		varchar(1000)
	, @UpdateStatsCommand	varchar(1000)
	, @DefragStartTime		datetime
	, @DefragEndTime		datetime
	, @StatsStartTime		datetime
	, @StatsEndTime			datetime
	, @PreFragmentation		float
	, @PostFragmentation	float
	, @PostPages			bigint
	, @PostFragCount		int
	, @DebugMessage			varchar(1000)
	, @ErrorMessage			nvarchar(2048)

select @DebugMessage		= ''

/****************************************************************************************
*****************************************************************************************
-- Step 2: Look up parameters, parse provided lists, initialize defaults for non-specified
-- variables and adjust flags as required.
*****************************************************************************************
****************************************************************************************/

-----------------------------------------------------------------------------------------
-- Load parameter set.
-----------------------------------------------------------------------------------------
-- If the specified parameter set exists load its values.
if (select count(*) from [Admin].dbo.IndexDefrag_Parameters with (nolock)
		where ParameterSetName = @ParameterSetName) = 1
begin
	if @Debug = 1
	begin
		select @DebugMessage = 'Parameters: Loading parameter set ''' + @ParameterSetName + '''.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end

	select @ExcludedDatabases	= isnull(@ExcludedDatabases, ExcludedDatabases)
		, @ExcludedObjects		= isnull(@ExcludedObjects, ExcludedObjects)
		, @ExcludedIndexes		= isnull(@ExcludedIndexes, ExcludedIndexes)
		, @MinFragmentation		= isnull(@MinFragmentation, MinFragmentation)
		, @MinPages				= isnull(@MinPages, MinPages)
		, @ImprovementThreshold	= isnull(@ImprovementThreshold, ImprovementThreshold)
		, @RebuildThreshold		= isnull(@RebuildThreshold, RebuildThreshold)
		, @TimeLimit			= isnull(@TimeLimit, TimeLimit)
		, @Delay				= isnull(@Delay, [Delay])
		, @ExecuteDays			= isnull(@ExecuteDays, ExecuteDays)
		, @ExecuteTime			= isnull(@ExecuteTime, ExecuteTime)
	from [Admin].dbo.IndexDefrag_Parameters with (nolock)
	where ParameterSetName = @ParameterSetName
end
else if @ParameterSetName is not null
begin
	select @DebugMessage = 'Error: Parameter set ''' + @ParameterSetName + ''' not found.  TIME: ' + convert(varchar(30),getdate(),121)
	raiserror(@DebugMessage,0,42) with nowait
end
else
begin
	if @Debug = 1
	begin
		select @DebugMessage = 'Parameters: No parameter set provided, loading defaults.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end

-- Initialize any variables that weren't set from the parameter list or provided at run time.
select @ForceNewScan		= isnull(@ForceNewScan, 0)
	, @ScanMode				= isnull(@ScanMode, 'limited')
	, @MinFragmentation		= isnull(@MinFragmentation, 5)
	, @MinPages				= isnull(@MinPages, 128)
	, @ImprovementThreshold = isnull(@ImprovementThreshold, 5)
	, @RebuildThreshold		= isnull(@RebuildThreshold, 30)
	, @RebuildIndexes		= isnull(@RebuildIndexes, 0)
	, @TimeLimit			= isnull(@TimeLimit, 240)
	, @Execute				= isnull(@Execute, 0)
	, @Delay				= isnull(@Delay, '00:00:05')

-- Use time limit (in minutes) to determine the system clock time to stop.
select @CutOffTime			= dateadd(minute,@TimeLimit,getdate())

if @Debug = 1
begin
	insert into #Parameters	(ParameterID, ParameterName, ParameterValue)
	select 1, '@ParameterSetName', @ParameterSetName
	union select 2, '@DatabaseName', @DatabaseName
	union select 3, '@ObjectName', @ObjectName
	union select 4, '@ExcludedDatabases', @ExcludedDatabases
	union select 5, '@ExcludedObjects', @ExcludedObjects
	union select 6, '@ExcludedIndexes', @ExcludedIndexes
	union select 7, '@ScanMode', @ScanMode
	union select 7, '@ForceNewScan', cast(@ForceNewScan as varchar)
	union select 8, '@MinFragmentation', cast(@MinFragmentation as varchar)
	union select 9, '@MinPages', cast(@MinPages as varchar)
	union select 10, '@ImprovementThreshold', cast(@ImprovementThreshold as varchar)
	union select 11, '@RebuildThreshold', cast(@RebuildThreshold as varchar)
	union select 12, '@RebuildIndexes', cast(@RebuildIndexes as varchar)
	union select 13, '@TimeLimit', cast(@TimeLimit as varchar)
	union select 14, '@Delay', @Delay
	union select 15, '@ExecuteDays', @ExecuteDays
	union select 16, '@ExecuteTime', @ExecuteTime
	union select 17, '@Execute', cast(@Execute as varchar)
	union select 18, '@Debug', cast(@Debug as varchar)

	select *
	from #Parameters with (nolock)
end

-----------------------------------------------------------------------------------------
-- Parse lists.
-----------------------------------------------------------------------------------------
-- Parse comma deliminated string of excluded database names into a temp table.
if @ExcludedDatabases is not null
begin
	select @Start = 0
		, @CommaPosition = isnull(charindex(',',@ExcludedDatabases),0)
		, @End = isnull(len(@ExcludedDatabases),0)

	while @CommaPosition > 0
	begin
		insert into #ExcludedDatabases
			(DatabaseName)
		select substring(@ExcludedDatabases,@Start,@CommaPosition-@Start)
		
		select @Start = @CommaPosition + 1
			, @CommaPosition = charindex(',',@ExcludedDatabases,@Start+1)
	end

	insert into #ExcludedDatabases
			(DatabaseName)
	select substring(@ExcludedDatabases,@Start,@End-@Start+1)
end

-- Set master and tempdb to be permanently excluded.
insert into #ExcludedDatabases
	(DatabaseName)
select 'master'
union select 'tempdb'

if @Debug = 1
begin
	select DatabaseName as ExcludedDatabases
	from #ExcludedDatabases with (nolock)
end

-- Parse comma deliminated string of excluded object names into a temp table.
if @ExcludedObjects is not null
begin
	select @Start = 0
		, @CommaPosition = isnull(charindex(',',@ExcludedObjects),0)
		, @End = isnull(len(@ExcludedObjects),0)

	while @CommaPosition > 0
	begin
		insert into #ExcludedObjects
			(ObjectName)
		select substring(@ExcludedObjects,@Start,@CommaPosition-@Start)
		
		select @Start = @CommaPosition + 1
			, @CommaPosition = charindex(',',@ExcludedObjects,@Start+1)
	end

	insert into #ExcludedObjects
			(ObjectName)
	select substring(@ExcludedObjects,@Start,@End-@Start+1)

	update #ExcludedObjects
	set DatabaseName = left(ObjectName,charindex('.',ObjectName)-1)
		, ObjectID = object_id(ObjectName)
end

if @Debug = 1
begin
	select ObjectName as ExcludedObjects
	from #ExcludedObjects with (nolock)
end

-- Parse comma deliminated string of excluded index names into a temp table.
if @ExcludedIndexes is not null
begin
	select @Start = 0
		, @CommaPosition = isnull(charindex(',',@ExcludedIndexes),0)
		, @End = isnull(len(@ExcludedIndexes),0)

	while @CommaPosition > 0
	begin
		insert into #ExcludedIndexes
			(StringName)
		select substring(@ExcludedIndexes,@Start,@CommaPosition-@Start)
		
		select @Start = @CommaPosition + 1
			, @CommaPosition = charindex(',',@ExcludedIndexes,@Start+1)
	end

	insert into #ExcludedIndexes
			(StringName)
	select substring(@ExcludedIndexes,@Start,@End-@Start+1)

	update #ExcludedIndexes
	set Period1 = charindex('.',StringName)

	update #ExcludedIndexes
	set Period2 = charindex('.',StringName,Period1+1)

	update #ExcludedIndexes
	set Period3 = charindex('.',StringName,Period2+1)

	update #ExcludedIndexes
	set DatabaseName = substring(StringName,0,Period1)
		, SchemaName = substring(StringName,Period1+1,Period2-Period1-1)
		, ObjectName = substring(StringName,Period2+1,Period3-Period2-1)
		, IndexName = substring(StringName,Period3+1,len(StringName)-Period3)
end

if @Debug = 1
begin
	select StringName as ExcludedIndexes
	from #ExcludedIndexes with (nolock)
end

-- Parse comma deliminated string of days to execute reorganization commands into a temp table.
if @ExecuteDays is not null
begin
	select @Start = 0
		, @CommaPosition = isnull(charindex(',',@ExecuteDays),0)
		, @End = isnull(len(@ExecuteDays),0)

	while @CommaPosition > 0
	begin
		insert into #ExecuteDays
			(ExecuteDay)
		select substring(@ExecuteDays,@Start,@CommaPosition-@Start)
		
		select @Start = @CommaPosition + 1
			, @CommaPosition = charindex(',',@ExecuteDays,@Start+1)
	end

	insert into #ExecuteDays
			(ExecuteDay)
	select substring(@ExecuteDays,@Start,@End-@Start+1)
end

update #ExecuteDays
set DayNumber = 
	case(ExecuteDay)
		when 'Sunday' then 1
		when 'Monday' then 2
		when 'Tuesday' then 3
		when 'Wednesday' then 4
		when 'Thursday' then 5
		when 'Friday' then 6
		when 'Saturday' then 7
		else 0
	end

-----------------------------------------------------------------------------------------
-- Passed variable verification.
-----------------------------------------------------------------------------------------
-- If a specific object is requested, load the database name and force a new scan.
if @ObjectName is not null
begin
	select @DatabaseName = left(@ObjectName,charindex('.',@ObjectName)-1)
	select @ForceNewScan = 1

	if @Debug = 1
	begin
		select @DebugMessage = 'ParameterChange: Object ''' + @ObjectName + ''' provided, looking up database name and forcing a new scan.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end

-----------------------------------------------------------------------------------------
-- Determine if we are executing any commands.
-----------------------------------------------------------------------------------------
if ((select count(*) from #ExecuteDays with (nolock) where DayNumber = datepart(weekday,getdate())) = 1
		and datediff(minute,@ExecuteTime,cast(datepart(hour,getdate()) as varchar) + ':' + cast(datepart(minute,getdate()) as varchar) + ':00') >0)
begin
	select @Execute = 1

	if @Debug = 1
	begin
		select @DebugMessage = 'ParameterChange: We''re within the maintenance window, enabling execute flag.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end

/****************************************************************************************
*****************************************************************************************
-- Step 3: Determine execution flow.
*****************************************************************************************
****************************************************************************************/

-----------------------------------------------------------------------------------------
-- If requesting a specific database that is excluded print error and bail.
-----------------------------------------------------------------------------------------
if (select count(*) from #ExcludedDatabases with (nolock) where DatabaseName = @DatabaseName) > 0
begin
	select @DebugMessage = 'Error: Specificed database ''' + @DatabaseName + ''' is included in provided exclusion list (' + @ExcludedDatabases + ').'
	raiserror(@DebugMessage,0,42) with nowait
end
-----------------------------------------------------------------------------------------
-- If we're rebuilding indexes and there are some to rebuild, load them.
-----------------------------------------------------------------------------------------
else if (@RebuildIndexes = 1
		and (
				select count(*) 
				from [Admin].dbo.IndexDefrag_Queue as IDQ with (nolock) 
				where CommandType = 'Rebuild'
					and DatabaseName = isnull(@DatabaseName, DatabaseName)
					and DatabaseName not in
						(
							select DatabaseName 
							from #ExcludedDatabases with (nolock)
						)
					and ObjectID not in
						(
							select ObjectID
							from #ExcludedObjects as EO with (nolock)
							where EO.DatabaseName = IDQ.DatabaseName
								and EO.ObjectID = IDQ.ObjectID
						)
					and IndexName not in
						(
							select IndexName
							from #ExcludedIndexes as EI with (nolock)
							where EI.DatabaseName = IDQ.DatabaseName
								and EI.SchemaName = IDQ.SchemaName
								and EI.ObjectName = IDQ.ObjectName
								and EI.IndexName = IDQ.IndexName
						)
			) > 0
	)
begin
	insert into #IndexAnalysis
		(DatabaseID
		, DatabaseName
		, SchemaID
		, SchemaName
		, ObjectID
		, ObjectName
		, IndexID
		, IndexName
		, CommandType
		, DefragCommand
		, VerifyCommand
		, UpdateStatsCommand
		, Fragmentation
		, Pages
		, FragmentationCount)
	select DatabaseID
		, DatabaseName
		, SchemaID
		, SchemaName
		, ObjectID
		, ObjectName
		, IndexID
		, IndexName
		, CommandType
		, DefragCommand
		, VerifyCommand
		, UpdateStatsCommand
		, Fragmentation
		, Pages
		, FragmentationCount
	from [Admin].dbo.IndexDefrag_Queue as IDQ with (nolock)
	where DatabaseName = isnull(@DatabaseName, DatabaseName)
		and DatabaseName not in
			(
				select DatabaseName
				from #ExcludedDatabases with (nolock)
			)
		and ObjectID not in
			(
				select ObjectID
				from #ExcludedObjects as EO with (nolock)
				where EO.DatabaseName = IDQ.DatabaseName
					and EO.ObjectID = IDQ.ObjectID
			)
		and IndexName not in
			(
				select IndexName
				from #ExcludedIndexes as EI with (nolock)
				where EI.DatabaseName = IDQ.DatabaseName
					and EI.SchemaName = IDQ.SchemaName
					and EI.ObjectName = IDQ.ObjectName
					and EI.IndexName = IDQ.IndexName
			)
		and CommandType = 'Rebuild'

	select @RecordCount = @@rowcount

	if @Debug = 1
	begin
		select @DebugMessage = 'Rebuild: Loaded ' + cast(@RecordCount as varchar) + ' pending index rebuild commands.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end
-----------------------------------------------------------------------------------------
-- If there is pending work from a previous execution load that unless a specific database
-- is requested.
-----------------------------------------------------------------------------------------
else if (@ForceNewScan = 0
		and (
				select count(*) 
				from [Admin].dbo.IndexDefrag_Queue as IDQ with (nolock) 
				where CommandType = 'Reorganize'
					and DatabaseName = isnull(@DatabaseName, DatabaseName)
					and DatabaseName not in
						(
							select DatabaseName 
							from #ExcludedDatabases with (nolock)
						)
					and ObjectID not in
						(
							select ObjectID
							from #ExcludedObjects as EO with (nolock)
							where EO.DatabaseName = IDQ.DatabaseName
								and EO.ObjectID = IDQ.ObjectID
						)
					and IndexName not in
						(
							select IndexName
							from #ExcludedIndexes as EI with (nolock)
							where EI.DatabaseName = IDQ.DatabaseName
								and EI.SchemaName = IDQ.SchemaName
								and EI.ObjectName = IDQ.ObjectName
								and EI.IndexName = IDQ.IndexName
						)
			) > 0
	)
begin
	insert into #IndexAnalysis
		(DatabaseID
		, DatabaseName
		, SchemaID
		, SchemaName
		, ObjectID
		, ObjectName
		, IndexID
		, IndexName
		, CommandType
		, DefragCommand
		, VerifyCommand
		, UpdateStatsCommand
		, Fragmentation
		, Pages
		, FragmentationCount)
	select DatabaseID
		, DatabaseName
		, SchemaID
		, SchemaName
		, ObjectID
		, ObjectName
		, IndexID
		, IndexName
		, CommandType
		, DefragCommand
		, VerifyCommand
		, UpdateStatsCommand
		, Fragmentation
		, Pages
		, FragmentationCount
	from [Admin].dbo.IndexDefrag_Queue as IDQ with (nolock)
	where DatabaseName = isnull(@DatabaseName, DatabaseName)
		and DatabaseName not in
			(
				select DatabaseName
				from #ExcludedDatabases with (nolock)
			)
		and ObjectID not in
			(
				select ObjectID
				from #ExcludedObjects as EO with (nolock)
				where EO.DatabaseName = IDQ.DatabaseName
					and EO.ObjectID = IDQ.ObjectID
			)
		and IndexName not in
			(
				select IndexName
				from #ExcludedIndexes as EI with (nolock)
				where EI.DatabaseName = IDQ.DatabaseName
					and EI.SchemaName = IDQ.SchemaName
					and EI.ObjectName = IDQ.ObjectName
					and EI.IndexName = IDQ.IndexName
			)
		and CommandType = 'Reorganize'

	select @RecordCount = @@rowcount

	if @Debug = 1
	begin
		select @DebugMessage = 'Reorganize: Loaded ' + cast(@RecordCount as varchar) + ' pending index reorganize commands.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end
-----------------------------------------------------------------------------------------
-- If no pending work is found OR a new scan is being forced.
-----------------------------------------------------------------------------------------
else
begin
	if @Debug = 1
	begin
		select @DebugMessage = 'Scan: Searching for indexes that match fragmentation requirements.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end

	-- Load requested database, or if not specificed all online, read_write databases that aren't in the exclusion list.
	insert into #DatabaseList
		(DatabaseID
		, DatabaseName)
	select database_id
		, name
	from master.sys.databases with (nolock)
	where name = isnull(@DatabaseName, name)
		and name not in (select DatabaseName from #ExcludedDatabases with (nolock))
		and state_desc = 'ONLINE'
		and is_read_only = 0

	if @Debug = 1
		select * from #DatabaseList with (nolock)

	select @Start = min(DatabaseListID)
		, @End = max(DatabaseListID)
	from #DatabaseList with (nolock)

	-- Walk through databases...
	while @Start <= @End
	begin
		select @DatabaseID = DatabaseID
			, @DatabaseName = DatabaseName
		from #DatabaseList with (nolock)
		where DatabaseListID = @Start

		if @Debug = 1
		begin
			select @DebugMessage = 'Starting scan of ' + @DatabaseName + '.  TIME: ' + convert(varchar(30),getdate(),121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		-- Load all index information for each database.
		insert into #IndexAnalysis
			(DatabaseID
			, DatabaseName
			, ObjectID
			, IndexID
			, PartitionNumber
			, Fragmentation
			, Pages
			, FragmentationCount)
		select database_id
			, @DatabaseName
			, object_id
			, index_id
			, partition_number
			, avg_fragmentation_in_percent
			, page_count
			, fragment_count
		from master.sys.dm_db_index_physical_stats(@DatabaseID,object_id(@ObjectName),null,null,@ScanMode)
		where avg_fragmentation_in_percent >= @MinFragmentation
			and index_id > 0 -- ignore heaps
			and page_count > @MinPages

		select @RecordCount = @@rowcount

		-- If any indexes matching the search critera are found, look up their names.
		if @RecordCount > 0
		begin
			select @SQL = '
			update #IndexAnalysis
			set SchemaID = S.schema_id
				, SchemaName = S.name
				, ObjectName = O.name
				, IndexName = I.name
				, IndexType = I.type_desc
				, AllowPageLocks = I.allow_page_locks
			from #IndexAnalysis as IA with (nolock)
			inner join ' + quotename(@DatabaseName) + '.sys.objects as O with (nolock)
				on IA.ObjectID = O.object_id
			inner join ' + quotename(@DatabaseName) + '.sys.schemas as S with (nolock)
				on O.schema_id = S.schema_id
			inner join ' + quotename(@DatabaseName) + '.sys.indexes as I with (nolock)
				on IA.ObjectID = I.object_id
				and IA.IndexID = I.index_id
			where DatabaseName = ' + quotename(@DatabaseName,'''')

			exec(@SQL)

			if @Debug = 1
			begin
				select @DebugMessage = 'Loaded ' + cast(@RecordCount as varchar) + ' indexes to be processed.  TIME: ' + convert(varchar(30),getdate(),121)
				raiserror(@DebugMessage,0,42) with nowait
			end
		end

		select @Start = @Start + 1
	end
	-----------------------------------------------------------------------------------------
	-- Build reorganize, verify and update stats commands.
	-----------------------------------------------------------------------------------------
	if (select count(*) from #IndexAnalysis with (nolock)) > 0
	begin
		if @Debug = 1
		begin
			select @DebugMessage = 'Building reorganize, verification and update stats commands.  TIME: ' + convert(varchar(30),getdate(),121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		update #IndexAnalysis
		set CommandType = 'Reorganize'
			, DefragCommand = 'alter index '
			+ quotename(IndexName) + ' on '
			+ quotename(DatabaseName) + '.' + quotename(SchemaName)	+ '.' + quotename(ObjectName)
			+ ' reorganize'
			, VerifyCommand =
				'select avg_fragmentation_in_percent
					, page_count
					, fragment_count
				from master.sys.dm_db_index_physical_stats(' + cast(DatabaseID as varchar)
					+ ',' + cast(ObjectID as varchar) + ',' + cast(IndexID as varchar)
					+ ',null,' + quotename(@ScanMode,'''') + ')
				where alloc_unit_type_desc = ''IN_ROW_DATA'''
			, UpdateStatsCommand = case(IndexType)
				when 'XML' then '--' + IndexName + ' is not a is not a statistics collection.  Skipping update stats command.'
				else 'update statistics '
					+ quotename(DatabaseName) + '.' + quotename(SchemaName) + '.' + quotename(ObjectName)
					+ ' ' + quotename(IndexName) end
	end

	-----------------------------------------------------------------------------------------
	-- Remove any queued commands where the current fragmentation is now lower.
	-- I.e. something else has caused the index to have less fragmentaion.
	-----------------------------------------------------------------------------------------
	delete from [Admin].dbo.IndexDefrag_Queue
	from #IndexAnalysis as IA with (nolock)
	inner join [Admin].dbo.IndexDefrag_Queue as IDQ with (nolock)
		on IA.DatabaseID = IDQ.DatabaseID
		and IA.SchemaID = IDQ.SchemaID
		and IA.ObjectID = IDQ.ObjectID
		and IA.IndexID = IDQ.IndexID
	where IA.Fragmentation < IDQ.Fragmentation

	select @RecordCount = @@rowcount

	if (@RecordCount > 0 and @Debug = 1)
	begin
		select @DebugMessage = 'Removed ' + cast(@RecordCount as varchar) + ' queued commands.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end

	-----------------------------------------------------------------------------------------
	-- Set PendingRebuild flag under one of two conditions.  There is already a rebuild command
	-- waiting in the queue, or the index doesn't allow page locks.
	-----------------------------------------------------------------------------------------
	-- Indexes have a pending rebuild command.
	update #IndexAnalysis
	set PendingRebuild = 1
	from #IndexAnalysis as IA with (nolock)
	inner join [Admin].dbo.IndexDefrag_Queue as IDQ with (nolock)
		on IA.DatabaseID = IDQ.DatabaseID
		and IA.SchemaID = IDQ.SchemaID
		and IA.ObjectID = IDQ.ObjectID
		and IA.IndexID = IDQ.IndexID
	where IDQ.CommandType = 'Rebuild'

	select @RecordCount = @@rowcount

	if (@RecordCount > 0 and @Debug = 1)
	begin
		select @DebugMessage = 'Flagged ' + cast(@RecordCount as varchar) + ' indexes that have a queued rebuild command.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end

	-- Indexes that have page level locking disabled and thus require a rebuild to defragment.
	update #IndexAnalysis
	set CommandType = 'Rebuild'
		, DefragCommand = replace(DefragCommand,'reorganize','rebuild')
		, PendingRebuild = 1
	where AllowPageLocks = 0

	select @RecordCount = @@rowcount

	if (@RecordCount > 0 and @Debug = 1)
	begin
		select @DebugMessage = 'Flagged ' + cast(@RecordCount as varchar) + ' indexes that have to be rebuilt do to not allowing page locks.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end

	-----------------------------------------------------------------------------------------
	-- Set Excluded flag for objects that have been listed as excluded from this run.
	-----------------------------------------------------------------------------------------
	update #IndexAnalysis
	set Excluded = 1
	from #IndexAnalysis as IA with (nolock)
	inner join #ExcludedObjects as EO with (nolock)
		on IA.DatabaseName = EO.DatabaseName
		and IA.ObjectID = EO.ObjectID

	select @RecordCount = @@rowcount

	if (@RecordCount > 0 and @Debug = 1)
	begin
		select @DebugMessage = 'Flagged ' + cast(@RecordCount as varchar) + ' records that were in the Object Exclusion List.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end

	-----------------------------------------------------------------------------------------
	-- Set Excluded flag for indexes that have been listed as excluded from this run.
	-----------------------------------------------------------------------------------------
	update #IndexAnalysis
	set Excluded = 1
	from #IndexAnalysis as IA with (nolock)
	inner join #ExcludedIndexes as EI with (nolock)
		on IA.DatabaseName = EI.DatabaseName
		and IA.SchemaName = EI.SchemaName
		and IA.ObjectName = EI.ObjectName
		and IA.IndexName = EI.IndexName

	select @RecordCount = @@rowcount

	if (@RecordCount > 0 and @Debug = 1)
	begin
		select @DebugMessage = 'Flagged ' + cast(@RecordCount as varchar) + ' records that were in the Index Exclusion List.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end

-- Display all prep info.
if @Debug = 1
	select * from #IndexAnalysis with (nolock) order by FragmentationCount desc

/****************************************************************************************
*****************************************************************************************
-- Step 4: Do the work.
*****************************************************************************************
****************************************************************************************/

-----------------------------------------------------------------------------------------
-- Start executing defrag commands.
-----------------------------------------------------------------------------------------
if @Execute = 1
begin
	if @Debug = 1
	begin
		select @DebugMessage = 'Execution: Start running the defrag commands.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end

	-----------------------------------------------------------------------------------------
	-- While there is work to be done and we have time left to do it, deragment indexes.
	-----------------------------------------------------------------------------------------
	while ((getdate() < @CutOffTime)
			and (
					select count(*)
					from #IndexAnalysis with (nolock)
					where (Completed = 0
							and PendingRebuild = 0
							and Excluded = 0)
				) > 0
			)
	begin
		select top 1 @IndexAnalysisID = IndexAnalysisID
			, @DefragCommand = DefragCommand
			, @VerifyCommand = VerifyCommand
			, @UpdateStatsCommand = UpdateStatsCommand
			, @PreFragmentation = Fragmentation
		from #IndexAnalysis with (nolock)
		where Completed = 0
			and PendingRebuild = 0
			and Excluded = 0
		order by FragmentationCount desc

		select @DefragStartTime = getdate()

		if @Debug = 1
		begin
			select @DebugMessage = 'Starting Defragmentation: ' + @DefragCommand + '.  TIME: ' + convert(varchar(30),@DefragStartTime,121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		begin try
			exec(@DefragCommand)
		end try
		begin catch
			select @ErrorMessage = error_message()

			exec [Admin].dbo.glados_ServerAlertsUpsert 
				@Provider = 'IndexDefrag'
				, @Name = @DefragCommand
				, @FailDateTime = @DefragStartTime
				, @ErrorInfo = @ErrorMessage
				, @ErrorLevel = 'WARN'
		end catch

		select @DefragEndTime = getdate()

		select @StatsStartTime = @DefragEndTime

		if @Debug = 1
		begin
			select @DebugMessage = 'Updating Statistics: ' + @UpdateStatsCommand + '.  TIME: ' + convert(varchar(30),@StatsStartTime,121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		begin try
			exec(@UpdateStatsCommand)
		end try
		begin catch
			select @ErrorMessage = error_message()

			exec [Admin].dbo.glados_ServerAlertsUpsert 
				@Provider = 'IndexDefrag'
				, @Name = @UpdateStatsCommand
				, @FailDateTime = @StatsStartTime
				, @ErrorInfo = @ErrorMessage
				, @ErrorLevel = 'WARN'
		end catch

		select @StatsEndTime = getdate()

		-- Collect fragmentation information after reorganization has finished.
		if @Debug = 1
		begin
			select @DebugMessage = 'Verifying: Checking how effective ' + @DefragCommand + ' was.  TIME: ' + convert(varchar(30),getdate(),121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		begin try
			insert into #PostFragmentation
			exec(@VerifyCommand)

			-- Currently only using post fragmentation these additional variables are
			-- in place incase we want to start logging additional post information.
			select @PostFragmentation = PostFragmentation
					, @PostPages = PostPages
					, @PostFragCount = PostFragCount
			from #PostFragmentation with (nolock)
		end try
		begin catch
			select @ErrorMessage = error_message()

			exec [Admin].dbo.glados_ServerAlertsUpsert 
				@Provider = 'IndexDefrag'
				, @Name = @VerifyCommand
				, @FailDateTime = @StatsEndTime
				, @ErrorInfo = @ErrorMessage
				, @ErrorLevel = 'WARN'

			-- Since the verification process failed, set post fragmentation to -100.
			-- This will prevent the command from being flagged as rebuild and write
			-- and obviously wrong value to the log table.
			select @PostFragmentation = -100
		end catch

		truncate table #PostFragmentation

		-- Log the defragmentation statistics.
		if @Debug = 1
		begin
			select @DebugMessage = 'Logging: Recording execution stats of ' + @DefragCommand + '.  TIME: ' + convert(varchar(30),getdate(),121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		insert into [Admin].dbo.IndexDefrag_Log
			(DatabaseID
			, DatabaseName
			, SchemaID
			, SchemaName
			, ObjectID
			, ObjectName
			, IndexID
			, IndexName
			, CommandType
			, DefragCommand
			, VerifyCommand
			, UpdateStatsCommand
			, DefragStartTime
			, DefragEndTime
			, StatsStartTime
			, StatsEndTime
			, Pages
			, PreFragmentation
			, PostFragmentation)
		select DatabaseID
			, DatabaseName
			, SchemaID
			, SchemaName
			, ObjectID
			, ObjectName
			, IndexID
			, IndexName
			, CommandType
			, DefragCommand
			, VerifyCommand
			, UpdateStatsCommand
			, @DefragStartTime
			, @DefragEndTime
			, @StatsStartTime
			, @StatsEndTime
			, Pages
			, @PreFragmentation
			, @PostFragmentation
		from #IndexAnalysis with (nolock)
		where IndexAnalysisID = @IndexAnalysisID

		-----------------------------------------------------------------------------------------
		-- If the index didn't improve by the requested amount and it still has a fragmentation
		-- higher than the specified threshold, create the rebuild command and queue it.
		-----------------------------------------------------------------------------------------
		if ((@PreFragmentation - @PostFragmentation < @ImprovementThreshold)
				and @PostFragmentation > @RebuildThreshold)
		begin
			if @Debug = 1
			begin
				select @DebugMessage = 'Rebuild Index: ' + @DefragCommand + ' wasn''t effective enough.  Queuing index rebuild command.  TIME: '
					+ convert(varchar(30),getdate(),121)
				raiserror(@DebugMessage,0,42) with nowait
			end

			insert into [Admin].dbo.IndexDefrag_Queue
				(DatabaseID
				, DatabaseName
				, SchemaID
				, SchemaName
				, ObjectID
				, ObjectName
				, IndexID
				, IndexName
				, CommandType
				, DefragCommand
				, VerifyCommand
				, UpdateStatsCommand
				, Fragmentation
				, Pages
				, FragmentationCount)
			select DatabaseID
				, DatabaseName
				, SchemaID
				, SchemaName
				, ObjectID
				, ObjectName
				, IndexID
				, IndexName
				, 'Rebuild'
				, replace(@DefragCommand,'reorganize','rebuild')
				, VerifyCommand
				, UpdateStatsCommand
				, @PostFragmentation
				, @PostPages
				, @PostFragCount
			from #IndexAnalysis with (nolock)
			where IndexAnalysisID = @IndexAnalysisID
		end

		-- Mark the command as completed.
		if @Debug = 1
		begin
			select @DebugMessage = 'Command Complete: ' + @DefragCommand + ' finished.  TIME: ' + convert(varchar(30),getdate(),121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		update #IndexAnalysis
		set Completed = 1
		where IndexAnalysisID = @IndexAnalysisID

		-- Give the server a breather.
		if @Debug = 1
		begin
			select @DebugMessage = 'Waiting: Giving the server a breather for ' + cast(@Delay as varchar) + ' seconds.  TIME: '
				+ convert(varchar(30),getdate(),121)
			raiserror(@DebugMessage,0,42) with nowait
		end

		waitfor delay @Delay
	end

	-----------------------------------------------------------------------------------------
	-- Remove commands that have been completed from the queue.
	-----------------------------------------------------------------------------------------
	delete from [Admin].dbo.IndexDefrag_Queue
	from #IndexAnalysis as IA with (nolock)
	inner join [Admin].dbo.IndexDefrag_Queue as IRQ with (nolock)
		on IA.DatabaseID = IRQ.DatabaseID
		and IA.SchemaID = IRQ.SchemaID
		and IA.ObjectID = IRQ.ObjectID
		and IA.IndexID = IRQ.IndexID
		and IA.CommandType = IRQ.CommandType
	where IA.Completed = 1

	select @RecordCount = @@rowcount

	if (@RecordCount > 0 and @Debug = 1)
	begin
		select @DebugMessage = 'Clearing Commands: ' + cast(@RecordCount as varchar) + ' commands were completed and removed from the queue.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end
else
begin
	if @Debug = 1
	begin
		select @DebugMessage = 'Not Executing: Set @Execute = 1 to run the defrag commands.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end

/****************************************************************************************
*****************************************************************************************
-- Step 5: Log work that wasn't finished and clean up temporary objects.
*****************************************************************************************
****************************************************************************************/

-----------------------------------------------------------------------------------------
-- Load any indexes that weren't able to be completed in the time frame to the queue table
-- to be run on next execution.
-----------------------------------------------------------------------------------------
if (select count(*) from #IndexAnalysis with (nolock) where Completed = 0) > 0
begin
	insert into [Admin].dbo.IndexDefrag_Queue
		(DatabaseID
		, DatabaseName
		, SchemaID
		, SchemaName
		, ObjectID
		, ObjectName
		, IndexID
		, IndexName
		, CommandType
		, DefragCommand
		, VerifyCommand
		, UpdateStatsCommand
		, Fragmentation
		, Pages
		, FragmentationCount)
	select IA.DatabaseID
		, IA.DatabaseName
		, IA.SchemaID
		, IA.SchemaName
		, IA.ObjectID
		, IA.ObjectName
		, IA.IndexID
		, IA.IndexName
		, IA.CommandType
		, IA.DefragCommand
		, IA.VerifyCommand
		, IA.UpdateStatsCommand
		, IA.Fragmentation
		, IA.Pages
		, IA.FragmentationCount
	from #IndexAnalysis as IA with (nolock)
	left outer join [Admin].dbo.IndexDefrag_Queue as IDQ with (nolock)
		on IA.DatabaseID = IDQ.DatabaseID
		and IA.SchemaID = IDQ.SchemaID
		and IA.ObjectID = IDQ.ObjectID
		and IA.IndexID = IDQ.IndexID
		and IA.CommandType = IDQ.CommandType
	where IDQ.DatabaseID is null
		and IDQ.SchemaID is null
		and IDQ.ObjectID is null
		and IDQ.IndexID is null
		and IA.Completed = 0

	select @RecordCount = @@rowcount

	if (@RecordCount > 0 and @Debug = 1)
	begin
		select @DebugMessage = 'Queue: Loaded ' + cast(@RecordCount as varchar)  + ' remaining commands into the queue for next time.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end
else
begin
	if @Debug = 1
	begin
		select @DebugMessage = 'Finished: Everything is done.  TIME: ' + convert(varchar(30),getdate(),121)
		raiserror(@DebugMessage,0,42) with nowait
	end
end

-----------------------------------------------------------------------------------------
-- Clean up.
-----------------------------------------------------------------------------------------
drop table #Parameters
drop table #ExcludedDatabases
drop table #ExcludedObjects
drop table #ExcludedIndexes
drop table #ExecuteDays
drop table #DatabaseList
drop table #IndexAnalysis
drop table #PostFragmentation
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
GO
