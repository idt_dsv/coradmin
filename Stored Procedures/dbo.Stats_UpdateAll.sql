SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


create procedure [dbo].[Stats_UpdateAll] as

set nocount on

declare @dbname nvarchar(256);
declare	@command nvarchar(256);

declare dbccus cursor for
select rtrim(name) from [master].[dbo].[sysdatabases]
where (name not in ('model', 'tempdb'))
	and (status & 32 <> 32)
	and (status & 64 <> 64)
	and (status & 128 <> 128)
	and (status & 256 <> 256)
	and (status & 512 <> 512)
	and (status & 1024 <> 1024)
	and (status & 2048 <> 2048)
	and (status & 4096 <> 4096)
	and (status & 32768 <> 32768)
open dbccus
fetch dbccus into @dbname
while @@fetch_status = 0
begin
	set @command='use '+quotename(@dbname)+' exec sp_updatestats'
	exec(@command)
	fetch dbccus into @dbname
end

close dbccus
deallocate dbccus

set nocount off


GO
