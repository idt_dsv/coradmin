SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[Disks_Upsert] (
	@DriveLetter char(1),
	@SizeMB int,
	@Label nvarchar(32) = null
)
as begin
	update Disks set SizeMB = @SizeMB, DateUpdated=current_timestamp, Label=@Label where DriveLetter = @DriveLetter;

	if @@rowcount = 0
		insert into Disks (DriveLetter, SizeMB, MonitorType, WarnPct, ErrPct, Label) values (@DriveLetter, @SizeMB, 'pct', 80, 90, @Label);
end

GO
