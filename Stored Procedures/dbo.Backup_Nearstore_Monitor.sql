SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE procedure [dbo].[Backup_Nearstore_Monitor]
AS


DECLARE @MonitorResult XML
------------------------------------------------------------------------------
EXECUTE dbo.glados_ServerAlertsMarkInactive 'Nearstore_PointInTimeRecovery';
------------------------------------------------------------------------------
	SELECT @MonitorResult = (
		SELECT 
			'Nearstore_PointInTimeRecovery' as Provider
			,'Server = ' + @@SERVERNAME + ', Database = ' + bn.DatabaseName as Name
			,current_timestamp as FailDateTime
			,'The Database ' + bn.DatabaseName + ' is behind in copying backup files to nearstore.  Current Point In Time Recovery is ' + CAST(bn.PointInTimeRecovery as varchar(50)) as ErrorInfo
			,case bn.RecoveryModel
				when 'SIMPLE' then 
					case
						when bn.PointInTimeRecovery < dateadd(hh,-28,GETDATE()) and bn.PointInTimeRecovery > dateadd(hh,-32,GETDATE()) then 'INFO'
						when bn.PointInTimeRecovery < dateadd(hh,-32,GETDATE()) then 'WARN'
						else 'GOOD'
					end
				when 'BULK_LOGGED' then
					case
						when bn.PointInTimeRecovery < dateadd(hh,-8,GETDATE()) and bn.PointInTimeRecovery > dateadd(hh,-8,GETDATE()) then 'INFO'
						when bn.PointInTimeRecovery < dateadd(hh,-12,GETDATE()) then 'WARN'
						else 'GOOD'
					end
				when 'FULL' then
					case
						when bn.PointInTimeRecovery < dateadd(hh,-8,GETDATE()) and bn.PointInTimeRecovery > dateadd(hh,-8,GETDATE()) then 'INFO'
						when bn.PointInTimeRecovery < dateadd(hh,-12,GETDATE()) then 'WARN'
						else 'GOOD'
					end
			 end as ErrorLevel
		
		FROM Admin.dbo.Backup_Nearstore_PITR as bn with (nolock)
		WHERE bn.DontBackup <> 1
		AND case bn.RecoveryModel
				when 'SIMPLE' then 
					case
						when bn.PointInTimeRecovery < dateadd(hh,-28,GETDATE()) and bn.PointInTimeRecovery > dateadd(hh,-32,GETDATE()) then 'INFO'
						when bn.PointInTimeRecovery < dateadd(hh,-32,GETDATE()) then 'WARN'
						else 'GOOD'
					end
				when 'BULK_LOGGED' then
					case
						when bn.PointInTimeRecovery < dateadd(hh,-8,GETDATE()) and bn.PointInTimeRecovery > dateadd(hh,-8,GETDATE()) then 'INFO'
						when bn.PointInTimeRecovery < dateadd(hh,-12,GETDATE()) then 'WARN'
						else 'GOOD'
					end
				when 'FULL' then
					case
						when bn.PointInTimeRecovery < dateadd(hh,-8,GETDATE()) and bn.PointInTimeRecovery > dateadd(hh,-8,GETDATE()) then 'INFO'
						when bn.PointInTimeRecovery < dateadd(hh,-12,GETDATE()) then 'WARN'
						else 'GOOD'
					end
			end in ('INFO','WARN')
		for xml raw('Alert'), root('Alerts')
	)

---------------------------------------------------------------------------------------
EXECUTE dbo.glados_ServerAlertsUpsertXML @MonitorResult
EXECUTE dbo.glados_ArchiveServerAlerts 'Nearstore_PointInTimeRecovery'
---------------------------------------------------------------------------------------






GO
