SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/****** Object:  StoredProcedure [dbo].[glados_DBAlerts]    Script Date: 06/21/2011 10:24:53 ******/

CREATE procedure [dbo].[glados_DBAlerts]
as
/*
	Proc: glados_DBAlerts
	Purpose: 

	When		Who	What
	05/16/2011	BGR	Initial Release
	06/21/2011	BGR	Added glados_ServerAlerts to return
	11/16/2011  JAW changed to allert on bad state as opposed to absence of good state
					Added Info level alert for recovering /restoring.
*/
--Insert into #alerts to return alerts
create table #alerts(
	Provider nvarchar(128),
	Name nvarchar(128),
	FailDateTime datetime,
	ErrorInfo nvarchar(max),
	ErrorLevel char(4)
)

insert into #alerts (Provider, Name, FailDateTime, ErrorInfo, ErrorLevel)
select 'DBStatusMonitor', db.name, current_timestamp, db.state_desc, 'ERR'
from sys.databases db with(nolock)
where db.state_desc IN ('SUSPECT','EMERGENCY','OFFLINE')
/*UPDATED JW 11/16/2011*/

select
	Provider,
	Name,
	FailDateTime,	
	ErrorInfo,
	ErrorLevel
from #alerts
union all
select 
	salert.Provider, 
	salert.Name, 
	salert.LastFailure, 
	salert.ErrorInfo, 
	salert.ErrorLevel
from dbo.glados_ServerAlerts salert with(nolock)
where isCurrent=1;
GO
GRANT EXECUTE ON  [dbo].[glados_DBAlerts] TO [glados]
GO
