SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE proc [dbo].[usp_Backup_Step1_FindDBs]
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 10/23/2009

Purpose: 
	Populate ADMIN.dbo.Backup_Databases table with info about all current dbs.  
	This process will catch any newly created dbs and slate them to be backed up.
	
Revisions:
	01/15/2010 - Andrew Gould
		Added a check for Read_Only and Offline databases to automatically flag
		them to not be backed up.
	1/28/2011 - Travis Tittle
		Added to the Read_Only and Offline database check, to also email DataServices
		if a Database changes state other than the state it was in previously.  The
		Restoring state however is not tracked.
	5/02/2011 - Travis Tittle
		Added check for existing recovery model change.  If changed, updated in 
		dbo.BackupDatabases as well.  If changed from SIMPLE to FULL, Fully Backed Up 
		Later in Step 4.
		Added check for databases that have been removed from the server, but still have 
		a record within dbo.BackupDatabases.  This process will email us if one is found.
	6/06/2011 - Travis Tittle
	    Removed DontBackup flag if database is readonly or another state other than ONLINE.
	    Instead, we are only not backing up if flagged as so, or if status is not ONLINE.
	6/23/2011 - Travis Tittle
		Incorporated GLaDOS Alerting.
	6/27/2011 - Travis Tittle
		Added Database Restore check.  If newly restored, database will be backed up.
	12/13/2011 - Travis Tittle
		Added ReadOnly check.  This is to backup RO databases seperately from regular Full
		Backup schedule.
	02/21/2012 - Travis Tittle
		Added Bulk-Logged Recovery Model Change.
	03/18/2014 - Travis Tittle
		Added better check for Detach/Reattach, database state changes, drop/recreates within 15 minutes.
To Do:
	None
	
Notes:
	Sets local and remote paths for newly added dbs to current path defaults.  
	Forces tempdb backup to off.
------------------------------------------------------------------------------*/
--------------------------------------------------------------------------------
-- Create Temp Table
--------------------------------------------------------------------------------
create table #RecovModel
   (Model_ID int identity(1,1),
    DatabaseName varchar(100) null,
    RecoveryModel varchar(50) null)
    
create table #Restored
   (RestoreID int identity(1,1),
    DatabaseName varchar(100),
    Restore_Date datetime,
    Newly_Restored bit)
--------------------------------------------------------------------------------
-- Declare variables.
--------------------------------------------------------------------------------
declare @LocalPathID int
declare @RemotePathID int
declare @ReadOnlyPathID int
declare @Subject varchar(100)
declare @Subject2 varchar(100)
declare @FailDateTime datetime
declare @Provider varchar(50)

select @Subject = 'A Database Has Changed its State'
select @Subject2 = 'A Database Has Been Removed From the Server'

select @FailDateTime = GETDATE()
select @Provider = 'BackupProvider'
--------------------------------------------------------------------------------
-- Lookup PathIDs for current Local and remote default paths.
--------------------------------------------------------------------------------
select @LocalPathID = PathID
from ADMIN.dbo.Backup_Paths with (nolock)
where Location = 'Local'
	and IsDefault = 1

select @RemotePathID = PathID
from ADMIN.dbo.Backup_Paths with (nolock)
where Location = 'Remote'
	and IsDefault = 1
	
select @ReadOnlyPathID = PathID
from ADMIN.dbo.Backup_Paths with (nolock)
where Location = 'ReadOnly'
	and IsDefault = 1

--------------------------------------------------------------------------------
-- Load new databases into the table.
--------------------------------------------------------------------------------
insert into ADMIN.dbo.Backup_Databases
	(LocalPathID
	, RemotePathID
	, DatabaseName
	, RecoveryModel
	, FolderName
	, Database_Status
	, Database_Create_Date
	, ReadOnlyPathID)
select @LocalPathID
	, @RemotePathID
	, D.[name] as DatabaseName
	, D.recovery_model_desc as RecoveryModel
	, D.[name] as FolderName
	, D.state_desc as Database_Status
	, D.create_date
	, @ReadOnlyPathID
from Master.sys.Databases as D with (nolock)
left outer join ADMIN.dbo.Backup_Databases as DB with (nolock)
	on D.[name] = DB.DatabaseName
where DB.DatabaseName is null
AND D.source_database_id IS NULL

--------------------------------------------------------------------------------
-- Make sure TempDB doesn't get backed up.
--------------------------------------------------------------------------------

update ADMIN.dbo.Backup_Databases
set DontBackup = 1
where DatabaseName = 'tempdb'

--------------------------------------------------------------------------------
-- Flags removed databases to not be backed up.
--------------------------------------------------------------------------------
update ADMIN.dbo.Backup_Databases
set DontBackup = 1
from ADMIN.dbo.Backup_Databases as DB with (nolock)
left outer join Master.sys.Databases as D with (nolock)
	on DB.DatabaseName = D.[name]
where D.[name] is null

----------------------------------------------------------------------------------
-- If an Existing Database, Update dbo.Backup_Databases if Recovery Model has changed
-- to match sys.databases, and reset LastLogBackupFile so a Full Database Backup will take place
-- if the change was from SIMPLE to FULL
----------------------------------------------------------------------------------
insert #RecovModel
   (DatabaseName,
    RecoveryModel)
select Name,
       recovery_model_desc
from master.sys.databases with (nolock)

-----------------------------------------------------

update bd
   set bd.RecoveryModel = rm.RecoveryModel,
       bd.LastLogBackupFile = CASE WHEN bd.RecoveryModel = 'SIMPLE' THEN '-1' ELSE bd.LastLogBackupFile END,
       bd.LastBackupDate = '1900-01-01 00:00:00.000' 
from ADMIN.dbo.Backup_Databases as bd with (nolock)
JOIN #RecovModel as rm with (nolock)
   ON rm.DatabaseName = bd.DatabaseName
where rm.RecoveryModel <> bd.RecoveryModel

--------------------------------------------------------------------------------
-- Compare System Database Create Date with metadata.  If different, flag for new Full Backup.  This will cover
-- Restores where logical names change(crazy), Drop/Recreates, and detach/reattach databases.
--------------------------------------------------------------------------------

update bd 
   set bd.Database_Create_Date = s.create_date,
       bd.LastLogBackupFile = '-1',
       bd.LastBackupDate = '1900-01-01 00:00:00.000'
from Admin.dbo.Backup_Databases as bd with (nolock)
inner join Master.sys.databases as s with (nolock)
   on s.Name = bd.DatabaseName
where bd.Database_Create_Date <> s.create_date

--------------------------------------------------------------------------------
-- If a database has been recently restored and the logical name hasn't changed(again, crazy), mark as such so a new database
-- backup can be taken.
--------------------------------------------------------------------------------
insert into #Restored
   (DatabaseName,
    Restore_Date)
select
    destination_database_name,
    max(restore_date) as Restore_Date
from msdb..restorehistory with (nolock)
group by destination_database_name

update r
   set r.Newly_Restored = 1
from #Restored as r with (nolock)
inner join Admin.dbo.Backup_Databases as bd with (nolock)
   on bd.DatabaseName = r.DatabaseName
WHERE bd.Database_Restore_Date <> r.Restore_Date
or bd.Database_Restore_Date IS NULL

-----------------------------------------------------

update bd 
   set bd.Database_Restore_Date = r.Restore_Date,
       bd.LastLogBackupFile = '-1',
       bd.LastBackupDate = '1900-01-01 00:00:00.000'
from Admin.dbo.Backup_Databases as bd with (nolock)
inner join #Restored as r with (nolock)
   on r.DatabaseName = bd.DatabaseName
where r.Newly_Restored = 1

-------------------------------------------------------------------------------
--Flag any database that has changed from a bad state to ONLINE for a new Full Backup
------------------------------------------------------------------------------
UPDATE DB
   SET DB.DontBackup = 0,
	   DB.Database_Status = D.state_desc,
       DB.LastLogBackupFile = '-1',
       DB.LastBackupDate = '1900-01-01 00:00:00.000'
FROM Master.sys.Databases as D with (nolock)
INNER JOIN ADMIN.dbo.Backup_Databases as DB with (nolock)
   ON DB.DatabaseName = D.name
WHERE Database_Status <> D.state_desc COLLATE SQL_Latin1_General_CP1_CI_AS
AND D.state_desc = 'ONLINE'
AND DB.Database_Status IN ('SUSPECT','EMERGENCY','OFFLINE') 


UPDATE DB
   SET Database_Status = D.state_desc
FROM Master.sys.Databases as D with (nolock)
INNER JOIN ADMIN.dbo.Backup_Databases as DB with (nolock)
   ON DB.DatabaseName = D.name

--------------------------------------------------------------------------------
-- Flag any databases previously RO but now RW for new Full Backup
--------------------------------------------------------------------------------

update bd
set bd.DontBackup = 0,
    bd.BackupGroup = 1,
    bd.LastLogBackupFile = '-1',
    bd.LastBackupDate = '1900-01-01 00:00:00.000',
    bd.ReadOnlyBackupFile = '-1',
    bd.ReadOnlyBackupDate = '1900-01-01 00:00:00.000'
FROM Admin.dbo.Backup_Databases as bd with (nolock)
JOIN Master.sys.databases as sd                           
   ON sd.name = bd.DatabaseName                           
where bd.DontBackup = 1
and sd.is_read_only = 0                                 
and sd.is_in_standby = 0
and bd.BackupGroup = 2
and bd.DatabaseName <> 'tempdb'

--------------------------------------------------------------------------------
-- Make sure all Databases Read-Only/Standby don't get backed up.
--------------------------------------------------------------------------------

update bd
set bd.DontBackup = 1,
    bd.BackupGroup = 2
FROM Admin.dbo.Backup_Databases AS bd with (nolock)
JOIN Master.sys.databases AS sd
   ON sd.name = bd.DatabaseName
where sd.is_read_only = 1
and sd.is_in_standby = 0

update bd
set bd.DontBackup = 1,
    bd.BackupGroup = 1
FROM Admin.dbo.Backup_Databases AS bd with (nolock)
JOIN Master.sys.databases AS sd
   ON sd.name = bd.DatabaseName
where sd.is_in_standby = 1

--------------------------------------------------------------------------------
-- Alert Glados of databases found in metadata, but no longer valid
--------------------------------------------------------------------------------

if (SELECT COUNT (*) from ADMIN.dbo.Backup_Databases as BD with (nolock)
	left outer join Master.sys.Databases as D with (nolock)
	on BD.DatabaseName = D.[name]
    where D.[name] is null) > 0
begin
    if exists (select * from Admin.dbo.glados_ServerAlerts where [ErrorInfo] = @Subject2)
    begin
       update Admin.dbo.glados_ServerAlerts
       set LastFailure = @FailDateTime,
           DateUpdated = @FailDateTime
       where Provider = @Provider
       and ErrorInfo = @Subject2
    end
    else 
    begin
       insert into dbo.glados_ServerAlerts
       (Provider, 
        Name, 
        FirstFailure, 
        LastFailure, 
        ErrorInfo, 
        ErrorLevel, 
        IsCurrent, 
        DateCreated, 
        DateUpdated)
       select
        @Provider,
        BD.DatabaseName,
        @FailDateTime,
        @FailDateTime,
        @Subject2,
        'WARN',
        1,
        @FailDateTime,
        @FailDateTime
       from ADMIN.dbo.Backup_Databases as BD with (nolock)
         left outer join Master.sys.Databases as D with (nolock)
	     on BD.DatabaseName = D.[name]
         where D.[name] is null;
             
    end
end
else
begin
delete dbo.glados_ServerAlerts
	output 
		deleted.ServerAlertID, 		
		deleted.provider,
		deleted.Name,
		deleted.FirstFailure,
		deleted.LastFailure,
		deleted.ErrorInfo,
		deleted.ErrorLevel,
		deleted.IsCurrent,
		deleted.DateCreated,
		deleted.DateUpdated
	into dbo.glados_ServerAlertsArchive
	from dbo.glados_ServerAlerts al
	where al.Provider=@Provider
	and al.ErrorInfo = @Subject2
end






















GO
