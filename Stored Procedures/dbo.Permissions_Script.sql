SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE proc [dbo].[Permissions_Script]
	@CommandType	varchar(25) = 'All'
	, @Source		varchar(150) = null
	, @Destination	varchar(150) = null
	, @Refresh		bit = 0

as

set nocount on

/*-------------------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 3/25/2011

Purpose:
	This proc organizes and displays the results from [Admin].dbo.Permissions_Load.

Objects:
	Tables:
	[Admin].dbo.Permissions_Server
	[Admin].dbo.Permissions_Databases
	[Admin].dbo.Permissions_Script
	[Admin].dbo.Script_Paths

	Stored Procs:
	[Admin].dbo.Permissions_Load

Paramaters:
	The output can be controlled by the @CommandType flag and the values passed with it for @Source and @Destination.
	@CommandType Options:
		'All'				- Prints out all permissions for server and databases.
		'Database'			- Prints out all permissions for the passed @Source database.
		'CloneDatabase'		- Creates commands to clone all permissions from the @Source database to the @Destination database.
		'Login'				- Prints out all permissions for the passed @Source login.
		'CloneLogin'		- Creates commands to clone all permissions from the @Source login to the @Destination login.
		'User'				- Prints out all permissions for the passed @Source user.
		'CloneUser'			- Creates commands to clone all permissions from the @Source user to the @Destination user.
		'OrphanedUsers'		- Prints out all orphaned users.
		'FixOrphanedUsers'	- Creates commands to fix orphaned users.
		'MismatchedNames'	- Creates commands to fix mismatched user\server names.
		'NoPermissions'		- Prints out all logins with no database specific permissions as well as a search command for more details.
		'Search'			- Effectively a wildcard search.  Useful for finding specific permissions such as 'db_owner'.
		'Inheritance'		- Lists all explicit and inherited permissions for the @Source account.
	@Refresh Options:
		0 - Default, this just reads the current data.
		1 - Forces a refresh of the data by running [Admin].dbo.Permissions_Script.

-------------------------------------------------------------------------------------------*/

---------------------------------------------------------------------------------------------
-- Create temp tables.
---------------------------------------------------------------------------------------------
create table #Databases
	(LoopID				int identity(1,1)
	, DatabaseName		varchar(150))

create table #UserIssues
	(DisplayOrder		int
	, DatabaseName		varchar(150)
	, PrincipalName		varchar(150)
	, Command			varchar(max))

create table #Search
	(DisplayOrder		int
	, PermissionType	varchar(50)
	, Scope				varchar(150)
	, Permission		varchar(max)
	, PrincipalName		varchar(150)
	, InheritedFrom		varchar(150))

create table #Inheritance
	(DisplayOrder		int
	, Scope				varchar(150)
	, PrincipalName		varchar(150)
	, GrantedTo			varchar(150))

/*-- Development ----------------------------------------------------------------------------
declare @CommandType	varchar(25)
	, @Source			varchar(150)
	, @Destination		varchar(150)
	, @Refresh			bit

select @CommandType		= 'All'
	, @Source			= null
	, @Destination		= null
	, @Refresh			= 0
-------------------------------------------------------------------------------------------*/

---------------------------------------------------------------------------------------------
-- Declare variables.
---------------------------------------------------------------------------------------------
declare @DatabaseName	varchar(150)
	, @LastLoadDate		datetime

-- Loop variables
declare @Start			int
	, @End				int

---------------------------------------------------------------------------------------------
-- Refresh data if its older than 24 hours or the @Refresh flag is set to 1.
---------------------------------------------------------------------------------------------
select @LastLoadDate = max(LoadDate)
from [Admin].dbo.Permissions_Server with (nolock)

if (@LastLoadDate is null or @Refresh = 1 or (datediff(hour,@LastLoadDate,getdate())) > 24)
	exec [Admin].dbo.Permissions_Load

-- Load list of all online databases except tempdb.
insert into #Databases
	(DatabaseName)
select '[' + name + ']'
from master.sys.databases with (nolock)
where name <> 'tempdb'
	and state_desc = 'ONLINE'

/*******************************************************************************************/
---------------------------------------------------------------------------------------------
-- VIEW RESULTS BY COMMANDTYPE.
---------------------------------------------------------------------------------------------
/*******************************************************************************************/

---------------------------------------------------------------------------------------------
-- Display all permissions on the server by default.
---------------------------------------------------------------------------------------------
if @CommandType = 'All'
begin
	print '/*****************************************************'
	print '-- Server permissions.'
	print '*****************************************************/'

	select Command as '-- Permissions'
	from [Admin].dbo.Permissions_Server with (nolock)
	order by DisplayOrder
		, PrincipalName
		, Command

	print '/*****************************************************'
	print '-- Database permissions.'
	print '*****************************************************/'

	select @Start = min(LoopID)
		, @End = max(LoopID)
	from #Databases

	while @Start <= @End
	begin
		select @DatabaseName = DatabaseName
		from #Databases with (nolock)
		where LoopID = @Start

		select Command as '-- Permissions'
		from [Admin].dbo.Permissions_Databases with (nolock)
		where DatabaseName = @DatabaseName
		order by DisplayOrder
			, PrincipalName
			, Command

		select @Start = @Start + 1
	end
end
---------------------------------------------------------------------------------------------
-- Display permissions for single database.
---------------------------------------------------------------------------------------------
else if @CommandType = 'Database'
begin
	print '/*****************************************************'
	print '-- Permissions for database ''' + isnull(@Source,'NULL') + '''.'
	print '*****************************************************/'

	if (select count(*) from [Admin].dbo.Permissions_Databases where DatabaseName = '[' + @Source + ']') > 0
	begin
		select Command as '-- Permissions'
		from [Admin].dbo.Permissions_Databases with (nolock)
		where DatabaseName = '[' + @Source + ']'
		order by DisplayOrder
			, PrincipalName
			, Command
	end
	else if @Source is null
		print 'If @CommandType is set to ''Database'', a @Source must be selected as well.'
	else
		print 'No permissions for database ''' + @Source + ''' exist.  Please make sure you spelled the database name correctly.'
end
---------------------------------------------------------------------------------------------
-- Commands to clone a database's permissions.
---------------------------------------------------------------------------------------------
else if @CommandType = 'CloneDatabase'
begin
	print '/*****************************************************'
	print '-- Cloning permissions from database ''' + isnull(@Source,'NULL') + ''' to database ''' + @Destination + '''.'
	print '*****************************************************/'

	if ((select count(*) from [Admin].dbo.Permissions_Databases where DatabaseName = '[' + @Source + ']') > 0 and @Destination is not null)
	begin
		select replace(Command,@Source,@Destination) as '-- Permissions'
		from [Admin].dbo.Permissions_Databases with (nolock)
		where DatabaseName = '[' + @Source + ']'
		order by DisplayOrder
			, PrincipalName
			, Command
	end
	else if @Source is null
		print 'If @CommandType is set to ''CloneDatabase'', a @Source must be selected as well.'
	else if @Destination is null
		print 'If @CommandType is set to ''CloneDatabase'', a @Destination must be selected as well.'
	else
		print 'No permissions for database ''' + @Source + ''' exist.  Please make sure you spelled the database name correctly.'
end
---------------------------------------------------------------------------------------------
-- Display permissions of a single login.
---------------------------------------------------------------------------------------------
else if @CommandType = 'Login'
begin
	print '/*************************************************'
	print '-- Permissions for login ''' + isnull(@Source,'NULL') + '''.'
	print '*************************************************/'

	if (select count(*) from [Admin].dbo.Permissions_Server where PrincipalName = @Source) > 0
	begin
		select Command as '-- Permissions'
		from [Admin].dbo.Permissions_Server with (nolock)
		where PrincipalName = @Source
			or PermissionType = 'Header'
		order by DisplayOrder
			, PrincipalName
			, Command

		select @Start = min(LoopID)
			, @End = max(LoopID)
		from #Databases as D with (nolock)
		inner join [Admin].dbo.Permissions_Databases as P with (nolock)
			on D.DatabaseName = P.DatabaseName
		inner join [Admin].dbo.Permissions_Server as SP with (nolock)
			on P.PrincipalSID = SP.PrincipalSID
		where SP.PrincipalName = @Source

		while @Start <= @End
		begin
			select @DatabaseName = DatabaseName
			from #Databases with (nolock)
			where LoopID = @Start

			select P.Command as '-- Permissions'
			from [Admin].dbo.Permissions_Databases as P with (nolock)
			where P.DatabaseName = @DatabaseName
				and P.PrincipalSID in 
					(select SP.PrincipalSID 
					from [Admin].dbo.Permissions_Server as SP with (nolock) 
					where SP.PrincipalName = @Source)
				or (P.DatabaseName = @DatabaseName
					and P.PermissionType = 'Header')
			order by P.DisplayOrder
				, P.Command

			select @Start = min(LoopID)
			from #Databases as D with (nolock)
			inner join [Admin].dbo.Permissions_Databases as P with (nolock)
				on D.DatabaseName = P.DatabaseName
			inner join [Admin].dbo.Permissions_Server as SP with (nolock)
				on P.PrincipalSID = SP.PrincipalSID
			where SP.PrincipalName = @Source
				and LoopID > @Start
		end
	end
	else if @Source is null
		print 'If @CommandType is set to ''Login'', a @Source must be selected as well.'
	else
		print 'Login ''' + @Source + ''' not found.  Please make sure you spelled it correctly.'
end
---------------------------------------------------------------------------------------------
-- Display permissions of a single login.
---------------------------------------------------------------------------------------------
else if @CommandType = 'CloneLogin'
begin
	print '/*************************************************'
	print '-- Cloning permissions from login ''' + isnull(@Source,'NULL') + ''' to login ''' + @Destination + '''.'
	print '*************************************************/'

	if (select count(*) from [Admin].dbo.Permissions_Server where PrincipalName = @Source) > 0
	begin
		select replace(Command,@Source,@Destination) as '-- Permissions'
		from [Admin].dbo.Permissions_Server with (nolock)
		where PrincipalName = @Source
			or PermissionType = 'Header'
		order by DisplayOrder
			, PrincipalName
			, Command

		select @Start = min(LoopID)
			, @End = max(LoopID)
		from #Databases as D with (nolock)
		inner join [Admin].dbo.Permissions_Databases as P with (nolock)
			on D.DatabaseName = P.DatabaseName
		inner join [Admin].dbo.Permissions_Server as SP with (nolock)
			on P.PrincipalSID = SP.PrincipalSID
		where SP.PrincipalName = @Source

		while @Start <= @End
		begin
			select @DatabaseName = DatabaseName
			from #Databases with (nolock)
			where LoopID = @Start

			select replace(P.Command,isnull(P.PrincipalName,@Source),@Destination) as '-- Permissions'
			from [Admin].dbo.Permissions_Databases as P with (nolock)
			where P.DatabaseName = @DatabaseName
				and P.PrincipalSID in 
					(select SP.PrincipalSID 
					from [Admin].dbo.Permissions_Server as SP with (nolock) 
					where SP.PrincipalName = @Source)
				or (P.DatabaseName = @DatabaseName
					and P.PermissionType = 'Header')
			order by P.DisplayOrder
				, P.Command

			select @Start = min(LoopID)
			from #Databases as D with (nolock)
			inner join [Admin].dbo.Permissions_Databases as P with (nolock)
				on D.DatabaseName = P.DatabaseName
			inner join [Admin].dbo.Permissions_Server as SP with (nolock)
				on P.PrincipalSID = SP.PrincipalSID
			where SP.PrincipalName = @Source
				and LoopID > @Start
		end
	end
	else if @Source is null
		print 'If @CommandType is set to ''CloneLogin'', a @Source must be selected as well.'
	else if @Destination is null
		print 'If @CommandType is set to ''CloneLogin'', a @Destination must be selected as well.'
	else
		print 'Login ''' + @Source + ''' not found.  Please make sure you spelled it correctly.'
end
---------------------------------------------------------------------------------------------
-- Display permissions of a single user.
---------------------------------------------------------------------------------------------
else if @CommandType = 'User'
begin
	print '/*************************************************'
	print '-- Permissions for user ''' + isnull(@Source,'NULL') + '''.'
	print '*************************************************/'

	if (select count(*) from [Admin].dbo.Permissions_Databases where PrincipalName = @Source) > 0
	begin
		select @Start = min(LoopID)
			, @End = max(LoopID)
		from #Databases as D with (nolock)
		inner join [Admin].dbo.Permissions_Databases as P with (nolock)
			on D.DatabaseName = P.DatabaseName
		where P.PrincipalName = @Source

		while @Start <= @End
		begin
			select @DatabaseName = DatabaseName
			from #Databases with (nolock)
			where LoopID = @Start

			select Command as '-- Permissions'
			from [Admin].dbo.Permissions_Databases with (nolock)
			where DatabaseName = @DatabaseName
				and PrincipalName = @Source
				or (DatabaseName = @DatabaseName
					and PermissionType = 'Header')
			order by DisplayOrder
				, Command

			select @Start = min(LoopID)
			from #Databases as D with (nolock)
			inner join [Admin].dbo.Permissions_Databases as P with (nolock)
				on D.DatabaseName = P.DatabaseName
			where P.PrincipalName = @Source
				and LoopID > @Start
		end
	end
	else if @Source is null
		print 'If @CommandType is set to ''User'', a @Source must be selected as well.'
	else
		print 'No permissions for user ''' + @Source + ''' exist.  Please make sure you spelled the user name correctly.'
end
---------------------------------------------------------------------------------------------
-- Commands to clone a user's permissions.
---------------------------------------------------------------------------------------------
else if @CommandType = 'CloneUser'
begin
	print '/*************************************************'
	print '-- Cloning permissions from user ''' + isnull(@Source,'NULL') + ''' to user ''' + @Destination + '''.'
	print '*************************************************/'

	if ((select count(*) from [Admin].dbo.Permissions_Databases where PrincipalName = @Source) > 0 and @Destination is not null)
	begin
		select @Start = min(LoopID)
			, @End = max(LoopID)
		from #Databases as D with (nolock)
		inner join [Admin].dbo.Permissions_Databases as P with (nolock)
			on D.DatabaseName = P.DatabaseName
		where P.PrincipalName = @Source

		while @Start <= @End
		begin
			select @DatabaseName = DatabaseName
			from #Databases with (nolock)
			where LoopID = @Start

			select replace(Command,@Source,@Destination) as '-- Permissions'
			from [Admin].dbo.Permissions_Databases with (nolock)
			where DatabaseName = @DatabaseName
				and PrincipalName = @Source
				or (DatabaseName = @DatabaseName
					and PermissionType = 'Header')
			order by DisplayOrder
				, Command

			select @Start = min(LoopID)
			from #Databases as D with (nolock)
			inner join [Admin].dbo.Permissions_Databases as P with (nolock)
				on D.DatabaseName = P.DatabaseName
			where P.PrincipalName = @Source
				and LoopID > @Start
		end
	end
	else if @Source is null
		print 'If @CommandType is set to ''CloneUser'', a @Source must be selected as well.'
	else if @Destination is null
		print 'If @CommandType is set to ''CloneUser'', a @Destination must be selected as well.'
	else
		print 'No permissions for user ''' + @Source + ''' exist.  Please make sure you spelled the user name correctly.'
end
---------------------------------------------------------------------------------------------
-- List all orphaned users, group by each database.
---------------------------------------------------------------------------------------------
else if @CommandType = 'OrphanedUsers'
begin
	print '/*************************************************'
	print '-- Displaying all orphaned users.'
	print '*************************************************/'

	-- Grab every user who's SID doesn't exist locally.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, PrincipalName)
	select 1 as DisplayOrder
		, D.DatabaseName
		, D.PrincipalName
	from [Admin].dbo.Permissions_Databases as D with (nolock)
	left outer join [Admin].dbo.Permissions_Server as S with (nolock)
		on D.PrincipalSID = S.PrincipalSID
	where D.PrincipalName not in ('guest','public')
		and D.PermissionType = 'Add Users'
		and S.PrincipalSID is null
	group by D.DatabaseName
		, D.PrincipalName
	order by D.DatabaseName
		, D.PrincipalName

	-- Add spaces for readablilty.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, PrincipalName)
	select 0 as DisplayOrder
		, DatabaseName
		, '' as PrincipalName
	from #UserIssues
	group by DatabaseName

	-- Load a comment heading with database name for readablitly.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, PrincipalName)
	select 1 as DisplayOrder
		, DatabaseName
		, '-- ' + DatabaseName as PrincipalName
	from #UserIssues
	group by DatabaseName

	select PrincipalName as '-- Orphaned Users'
	from #UserIssues
	order by DatabaseName
		, DisplayOrder
end
---------------------------------------------------------------------------------------------
-- List all fixable orphaned users, group by each database.
---------------------------------------------------------------------------------------------
else if @CommandType = 'FixOrphanedUsers'
begin
	print '/*************************************************'
	print '-- Fixing orphaned users.'
	print '*************************************************/'

	-- Grab every user who's SID doesn't exist locally but name does.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, PrincipalName
		, Command)
	select 3 as DisplayOrder
		, D.DatabaseName
		, D.PrincipalName
		, 'ALTER USER [' + D.PrincipalName + '] WITH LOGIN = [' + D.PrincipalName + ']' as Command
	from [Admin].dbo.Permissions_Databases as D with (nolock)
	left outer join [Admin].dbo.Permissions_Server as S with (nolock)
		on D.PrincipalSID = S.PrincipalSID
	inner join [Admin].dbo.Permissions_Server as SName with (nolock)
		on D.PrincipalName = SName.PrincipalName
	where D.PrincipalName not in ('guest','public')
		and D.PermissionType = 'Add Users'
		and S.PrincipalSID is null
	group by D.DatabaseName
		, D.PrincipalName
	order by D.DatabaseName
		, D.PrincipalName

	-- Add spaces for readablilty.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, Command)
	select 0 as DisplayOrder
		, DatabaseName
		, '' as Command
	from #UserIssues
	group by DatabaseName

	-- Load use database commands.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, Command)
	select 1 as DisplayOrder
		, DatabaseName
		, 'USE ' + DatabaseName as Command
	from #UserIssues
	group by DatabaseName

	-- Load a comment heading for readablitly.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, Command)
	select 2 as DisplayOrder
		, DatabaseName
		, '-- Fix Users ----------------------------------------------------------------' as Command
	from #UserIssues
	group by DatabaseName

	select Command as '-- Orphaned Users - Fixable'
	from #UserIssues
	order by DatabaseName
		, DisplayOrder
end
---------------------------------------------------------------------------------------------
-- List all logins which having a matching server SID but a different name.
---------------------------------------------------------------------------------------------
else if @CommandType = 'MismatchedNames'
begin
	print '/*************************************************'
	print '-- Fixing all login and user name related issues.'
	print '*************************************************/'

	select '-- Drop Logins --------------------------------------------------------------' as '-- Logins that don''t exist in Active Directory'
	union
	select 'DROP LOGIN [' + WL.PrincipalName + ']'
	from [Admin].dbo.Permissions_WindowsLogins as WL with (nolock)
	left outer join [Admin].dbo.Permissions_ActiveDirectoryGroupMembership as ADGM with (nolock)
		on WL.ActiveDirectorySDDL = ADGM.ActiveDirectorySDDL
	where ADGM.ActiveDirectorySDDL is null
		and WL.PrincipalName like 'IDT-CORALVILLE\%'

	select '-- Update Login''s Name ------------------------------------------------------' as '-- Mismatched Login Names'
	union
	select 'ALTER LOGIN [' + WL.PrincipalName + '] WITH NAME = [' + ADGM.PrincipalName + ']'
	from [Admin].dbo.Permissions_WindowsLogins as WL with (nolock)
	inner join [Admin].dbo.Permissions_ActiveDirectoryGroupMembership as ADGM with (nolock)
		on WL.ActiveDirectorySDDL = ADGM.ActiveDirectorySDDL
	where WL.PrincipalName <> ADGM.PrincipalName
	group by WL.PrincipalName
		, ADGM.PrincipalName

	-- Grab every user who's SID doesn't exist locally but name does.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, PrincipalName
		, Command)
	select 3 as DisplayOrder
		, D.DatabaseName
		, D.PrincipalName
		, 'ALTER USER [' + D.PrincipalName + '] WITH LOGIN = [' + S.PrincipalName + ']' as Command
	from [Admin].dbo.Permissions_Databases as D with (nolock)
	inner join [Admin].dbo.Permissions_Server as S with (nolock)
		on D.PrincipalSID = S.PrincipalSID
	where D.PrincipalName <> 'dbo'
		and D.PermissionType = 'Add Users'
		and D.PrincipalName <> S.PrincipalName
	group by D.DatabaseName
		, D.PrincipalName
		, S.PrincipalName
	order by D.DatabaseName
		, D.PrincipalName

	-- Add spaces for readablilty.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, Command)
	select 0 as DisplayOrder
		, DatabaseName
		, '' as Command
	from #UserIssues
	group by DatabaseName

	-- Load use database commands.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, Command)
	select 1 as DisplayOrder
		, DatabaseName
		, 'USE ' + DatabaseName as Command
	from #UserIssues
	group by DatabaseName

	-- Load a comment heading for readablitly.
	insert into #UserIssues
		(DisplayOrder
		, DatabaseName
		, Command)
	select 2 as DisplayOrder
		, DatabaseName
		, '-- Update User''s Name -------------------------------------------------------' as Command
	from #UserIssues
	group by DatabaseName

	select Command as '-- Mismatched User Names'
	from #UserIssues
	order by DatabaseName
		, DisplayOrder
end
---------------------------------------------------------------------------------------------
-- List all logins which don't have any database specific permissions.
---------------------------------------------------------------------------------------------
else if @CommandType = 'NoPermissions'
begin
	print '/*************************************************'
	print '-- Logins with no database specific permissions.'
	print '*************************************************/'

	select S.PrincipalName as '-- Login names'
	from [Admin].dbo.Permissions_Server as S with (nolock)
	left outer join [Admin].dbo.Permissions_Databases as D with (nolock)
		on S.PrincipalSID = D.PrincipalSID
	where D.PrincipalSID is null
		and S.PrincipalName is not null
	group by S.PrincipalName
	order by S.PrincipalName

	select 'exec [Admin].dbo.Permissions_Script @CommandType = ''Login'', @Source = ''' + S.PrincipalName + '''' as '-- Run for more details about the login.'
	from [Admin].dbo.Permissions_Server as S with (nolock)
	left outer join [Admin].dbo.Permissions_Databases as D with (nolock)
		on S.PrincipalSID = D.PrincipalSID
	where D.PrincipalSID is null
		and S.PrincipalName is not null
	group by S.PrincipalName
	order by S.PrincipalName
end
---------------------------------------------------------------------------------------------
-- Wildcard search against commands.
-- Can be used to find specific permissions like 'db_owner'.
-- For best results make sure there are no name mismatches;'
-- exec [Admin].dbo.Permissions_Script @CommandType = ''MismatchedNames''.'
---------------------------------------------------------------------------------------------
else if @CommandType = 'Search'
begin
	print '/*************************************************'
	print '-- Searching for ''' + isnull(@Source,'NULL') + '''.'
	print '*************************************************/'

	select 'exec [Admin].dbo.Permissions_Script @CommandType = ''MismatchedNames''' as '-- For best results make sure there are no name mismatches.'

	insert into #Search
		(DisplayOrder
		, PermissionType
		, Scope
		, Permission
		, PrincipalName)
	select 0 as DisplayOrder
		, 'EXPLICIT' as PermissionType
		, 'SERVER' as Scope
		, Command as Permission
		, PrincipalName
	from [Admin].dbo.Permissions_Server with (nolock)
	where Command like '%' + @Source + '%'
	group by Command
		, PrincipalName

	insert into #Search
		(DisplayOrder
		, PermissionType
		, Scope
		, Permission
		, PrincipalName)
	select 0 as DiplayOrder
		, 'EXPLICIT' as PermissionType
		, DatabaseName as Scope
		, Command as Permission
		, PrincipalName
	from [Admin].dbo.Permissions_Databases with (nolock)
	where Command like '%' + @Source + '%'
	group by DatabaseName
		, Command
		, PrincipalName

	if (select count(*) from #Search) > 0
	begin
		; -- SQL Role Inheritence
		with CTE (DisplayOrder, Scope, Permission, PrincipalName, InheritedFrom)
		as
		(
			select 1 as DisplayOrder
				, S.Scope
				, S.Permission
				, PD.PrincipalName
				, S.PrincipalName as InheritedFrom
			from [Admin].dbo.Permissions_Databases as PD with (nolock)
			inner join #Search as S with (nolock)
				on PD.DatabaseName = S.Scope
				and PD.Command like '%@rolename = ''' + S.PrincipalName + '''%'
			where PD.PermissionType = 'Role Membership'

			union all

			select Parent.DisplayOrder + 1
				, Parent.Scope
				, Parent.Permission
				, Child.PrincipalName
				, Parent.PrincipalName as InheritedFrom
			from [Admin].dbo.Permissions_Databases as Child with (nolock)
			inner join CTE as Parent
				on Child.DatabaseName = Parent.Scope
				and Child.Command like '%@rolename = ''' + Parent.PrincipalName + '''%'
			where Child.PermissionType = 'Role Membership'
		)
		insert into #Search
			(DisplayOrder
			, PermissionType
			, Scope
			, Permission
			, PrincipalName
			, InheritedFrom)
		select DisplayOrder
			, 'INHERITED' as PermissionType
			, Scope
			, Permission
			, PrincipalName
			, InheritedFrom
		from CTE

		; -- Windows Group Inheritence
		with CTE (DisplayOrder, Scope, Permission, PrincipalName, InheritedFrom)
		as
		(
			select S.DisplayOrder + 1
				, S.Scope
				, S.Permission
				, ADGM.PrincipalName
				, ADGM.GroupName as InheritedFrom
			from [Admin].dbo.Permissions_ActiveDirectoryGroupMembership as ADGM with (nolock)
			inner join #Search as S with (nolock)
				on ADGM.GroupName = S.PrincipalName

			union all

			select Parent.DisplayOrder + 1
				, Parent.Scope
				, Parent.Permission
				, Child.PrincipalName
				, Child.GroupName as InheritedFrom
			from [Admin].dbo.Permissions_ActiveDirectoryGroupMembership as Child with (nolock)
			inner join CTE as Parent
				on Child.GroupName = Parent.PrincipalName
		)
		insert into #Search
			(DisplayOrder
			, PermissionType
			, Scope
			, Permission
			, PrincipalName
			, InheritedFrom)
		select DisplayOrder
			, 'INHERITED' as PermissionType
			, Scope
			, Permission
			, PrincipalName
			, InheritedFrom
		from CTE

		select PermissionType
			, Scope
			, Permission
			, PrincipalName
			, InheritedFrom
		from #Search
		order by Scope
			, Permission
			, DisplayOrder
			, InheritedFrom
			, PrincipalName
	end
	else if @Source is null
		print 'If @CommandType is set to ''Search'', a @Source must be selected as well.'
	else
		print 'Nothing matching ''' + @Source + ''' was found.  Please make sure you spelled the search name correctly.'

end
---------------------------------------------------------------------------------------------
-- Lists all explicit and inherited permissions for requested account.
-- For best results make sure there are no name mismatches;'
-- exec [Admin].dbo.Permissions_Script @CommandType = ''MismatchedNames''.'
---------------------------------------------------------------------------------------------
else if @CommandType = 'Inheritance'
begin
	print '/*************************************************'
	print '-- Inheritance of ''' + isnull(@Source,'NULL') + '''.'
	print '*************************************************/'

	select 'exec [Admin].dbo.Permissions_Script @CommandType = ''MismatchedNames''' as '-- For best results make sure there are no name mismatches.'

	insert into #Inheritance
		(DisplayOrder
		, Scope
		, PrincipalName)
	select 0 as DisplayOrder
		, 'SERVER' as Scope
		, PrincipalName
	from [Admin].dbo.Permissions_ActiveDirectoryGroupMembership with (nolock)
	where PrincipalName = @Source
	group by PrincipalName

	if (select count(*) from #Inheritance with (nolock)) = 0
	begin
		insert into #Inheritance
			(DisplayOrder
			, Scope
			, PrincipalName)
		select 0 as DisplayOrder
			, 'SERVER' as Scope
			, PrincipalName
		from [Admin].dbo.Permissions_Server with (nolock)
		where PermissionType = 'Add Logins'
			and PrincipalName = @Source
		group by PrincipalName
	end

	insert into #Inheritance
		(DisplayOrder
		, Scope
		, PrincipalName)
	select 0
		, DatabaseName as Scope
		, PrincipalName
	from [Admin].dbo.Permissions_Databases with (nolock)
	where PermissionType in ('Add Users','Create Roles')
		and PrincipalName = @Source

	if (select count(*) from #Inheritance) > 0
	begin
		;
		with CTE (DisplayOrder, GroupName, GrantedTo)
		as
		(
			select I.DisplayOrder + 1
				, ADGM.GroupName
				, ADGM.PrincipalName as GrantedTo
			from #Inheritance as I with (nolock)
			inner join [Admin].dbo.Permissions_ActiveDirectoryGroupMembership as ADGM with (nolock)
				on I.PrincipalName = ADGM.PrincipalName
			where I.Scope = 'SERVER'

			union all

			select Child.DisplayOrder + 1
				, Parent.GroupName
				, Parent.PrincipalName as GrantedTo
			from [Admin].dbo.Permissions_ActiveDirectoryGroupMembership as Parent with (nolock)
			inner join CTE as Child
				on Parent.PrincipalName = Child.GroupName
		)
		insert into #Inheritance
			(DisplayOrder
			, Scope
			, PrincipalName
			, GrantedTo)
		select DisplayOrder
			, 'SERVER' as Scope
			, GroupName
			, GrantedTo
		from CTE
		where GroupName not in ('WINDOWS_LOGIN','WINDOWS_GROUP')

		;
		with CTE (DisplayOrder, Scope, GroupName, GrantedTo)
		as
		(
			select I.DisplayOrder + 1
				, I.Scope
				, substring(PD.Command
					,charindex('''',PD.Command)+1
					,charindex('''',PD.Command,charindex('''',PD.Command)+1)-charindex('''',PD.Command)-1) as GroupName
				, I.PrincipalName as GrantedTo
			from #Inheritance as I with (nolock)
			inner join [Admin].dbo.Permissions_Databases as PD with (nolock)
				on I.PrincipalName = PD.PrincipalName
				and I.Scope = PD.DatabaseName
			where PD.PermissionType = 'Role Membership'

			union all

			select Child.DisplayOrder + 1
				, Child.Scope
				, substring(Parent.Command
					,charindex('''',Parent.Command)+1
					,charindex('''',Parent.Command,charindex('''',Parent.Command)+1)-charindex('''',Parent.Command)-1) as GroupName
				, Parent.PrincipalName as GrantedTo
			from [Admin].dbo.Permissions_Databases as Parent with (nolock)
			inner join CTE as Child
				on Child.Scope = Parent.DatabaseName
				and Child.GroupName = Parent.PrincipalName
			where Parent.PermissionType = 'Role Membership'
		)
		insert into #Inheritance
			(DisplayOrder
			, Scope
			, PrincipalName
			, GrantedTo)
		select DisplayOrder
			, Scope
			, GroupName
			, GrantedTo
		from CTE

		select case(I.PrincipalName)
				when @Source then 'EXPLICIT'
				else 'INHERITED' end as PermissionType
			, I.Scope
			, PS.Command as Permission
			, I.PrincipalName
			, I.GrantedTo
		from #Inheritance as I with (nolock)
		left outer join [Admin].dbo.Permissions_Server as PS with (nolock)
			on I.PrincipalName = PS.PrincipalName
			and PS.PermissionType <> 'Add Logins'
		where Scope = 'SERVER'
		order by I.DisplayOrder
			, I.PrincipalName
			, PS.DisplayOrder

		select case(I.PrincipalName)
				when @Source then 'EXPLICIT'
				else 'INHERITED' end as PermissionType
			, I.Scope
			, PD.Command as Permission
			, I.PrincipalName
			, I.GrantedTo
		from #Inheritance as I with (nolock)
		inner join [Admin].dbo.Permissions_Databases as PD with (nolock)
			on I.PrincipalName = PD.PrincipalName
			and I.Scope = PD.DatabaseName
			and PD.PermissionType not in ('Add Users','Create Roles')
		order by I.Scope
			, I.DisplayOrder
			, I.PrincipalName
			, PD.DisplayOrder
			, PD.Command
	end
	else if @Source is null
		print 'If @CommandType is set to ''Inheritance'', a @Source must be selected as well.'
	else
		print 'Nothing matching ''' + @Source + ''' was found.  Please make sure you spelled the account name correctly.'

end
---------------------------------------------------------------------------------------------
-- Unrecognized @CommandType.
---------------------------------------------------------------------------------------------
else
begin
	print 'Unrecognized CommandType ''' + @CommandType + '''.'
	print 'Available options are:
	''All''				- Prints out all permissions for server and databases.
	''Database''		- Prints out all permissions for the passed @Source database.
	''CloneDatabase''	- Creates commands to clone all permissions from the @Source database to the @Destination database.
	''Login''			- Prints out all permissions for the passed @Source login.
	''CloneLogin''		- Creates commands to clone all permissions from the @Source login to the @Destination login.
	''User''			- Prints out all permissions for the passed @Source user.
	''CloneUser''		- Creates commands to clone all permissions from the @Source user to the @Destination user.
	''OrphanedUsers''	- Prints out all orphaned users.
	''FixOrphanedUsers''- Creates commands to fix orphaned users.
	''MismatchedNames''	- Creates commands to fix mismatched user\server names.
	''NoPermissionS''	- Prints out all logins with no database specific permissions as well as a search command for more details.
	''Search''			- Effectively a wildcard search.  Useful for finding specific permissions such as ''db_owner''.
	''Inheritance''		- Lists all explicit and inherited permissions for the @Source account.'
end
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
GO
