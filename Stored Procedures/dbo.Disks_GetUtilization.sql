SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[Disks_GetUtilization] as
begin
	declare @FixedDrives table (
		drive char(1),
		mbfree int
	);
	insert into @FixedDrives (drive,mbfree)
	execute xp_fixeddrives;

	select f.drive as Drive
		,f.mbfree as FreeMB
		,d.sizemb as SizeMB
		,(d.sizemb-f.mbfree)/(cast(d.sizemb as decimal(10,2))) as PercentFull
	from @FixedDrives f inner join admin.dbo.Disks d 
		on f.drive=d.DriveLetter;
end
GO
