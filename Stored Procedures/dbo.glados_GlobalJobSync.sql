SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








CREATE proc [dbo].[glados_GlobalJobSync]
as

set nocount on

/*-------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 01/08/2013

Purpose:
	This proc runs locally every 15 minutes and loads all missing/disabled/nonscheduled Global Jobs into
	GLaDOS as information messages.

Objects:
	dbo.JobInventory_Global
-------------------------------------------------------------------------*/

-- Create Temp Table
create table #BadJob
   (jobid int identity(1,1),
    jobname varchar(500))
    
    
CREATE TABLE #NoFutureSchedule
   (ID int identity(1,1),
    JobName varchar(150),
    NoFutureSchedule bit)
    
-- Declare variables
declare @XML xml

-----------------------------------------------------
--Update Status of non MSX\TSX jobs
-----------------------------------------------------      

UPDATE ji
   SET ji.status = s.enabled
FROM msdb.dbo.sysjobs as s with (nolock)
JOIN Admin.dbo.JobInventory_Global as ji with (nolock)
   ON ji.JobName = s.name
WHERE ji.MSX_TSX = 0
-----------------------------------------------------
--Update Existing Jobs Missing a Schedule
-----------------------------------------------------   
   
INSERT INTO #NoFutureSchedule
   (JobName,
    NoFutureSchedule)        
SELECT sj.name,
       0 as NoFutureSchedule
FROM msdb.dbo.sysjobs as sj with (nolock)
LEFT JOIN msdb.dbo.sysjobschedules as sjs with (nolock)
   ON sjs.job_id = sj.job_id
WHERE sjs.next_run_date is null 

UPDATE ji
   SET ji.FutureSchedule = nfs.NoFutureSchedule
FROM #NoFutureSchedule as nfs
JOIN Admin.dbo.JobInventory_Global as ji with (nolock)
   ON ji.JobName = nfs.JobName
WHERE ji.MSX_TSX = 0

-----------------------------------------------------
--Find Global Jobs missing
-----------------------------------------------------   

insert into #BadJob
   (jobname)
select jig.jobname
from dbo.JobInventory_Global as jig with (nolock)
left outer join msdb.dbo.sysjobs as s with (nolock)
   on s.Name = jig.JobName
where s.Name is null
and jig.ignore <> 1

-----------------------------------------------------
--Find Global Jobs missing
-----------------------------------------------------  

insert into #BadJob
   (jobname)
select jig.jobname
from Admin.dbo.JobInventory_Global as jig with (nolock)
where jig.Status = 0
and jig.Ignore <> 1
or jig.FutureSchedule = 0
and jig.Ignore <> 1

-- Lookup all jobs that are currently disabled and build the list as XML.
select @XML =
(select 'Global Job Discrepancy' as Provider
	, jobname as Name
	, getdate() as FailDateTime
	, 'Missing/Disabled/Nonscheduled Global Jobs to investigate' as ErrorInfo
	, 'WARN' as ErrorLevel
from #BadJob
for xml raw('Alert'), root('Alerts'))

-- Flag all Disabled Jobs as no longer current.
exec [Admin].dbo.glados_ServerAlertsMarkInactive @Provider = 'Global Job Discrepancy'

-- Update current Disabled Jobs and insert new ones.
exec [Admin].dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML = @XML

-- Archive and remove any Disabled Jobs that are no longer current.
exec [Admin].dbo.glados_ArchiveServerAlerts @Provider = 'Global Job Discrepancy'
---------------------------------------------------------------------------
---------------------------------------------------------------------------







GO
