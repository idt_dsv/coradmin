SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[glados_LastLogBackup]

as

set nocount on

/*-------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 12/09/2011

Purpose:
	This proc checks to see if the last log backup for all FULL databases
	has run within the provided threshold.

Objects:
	[Admin].dbo.Backup_ExclusionList
	master.sys.databases
	msdb.dbo.backupset
	GLaDOS

Revisions:
	12/09/2011 - Andrew Gould
		Added state_desc check to make sure this process only looks at databases
		that are online.

	12/19/2011 - Andrew Gould
		Added in logic to ignore databases that are in read_only and/or
			standby.
		Fixed a logic issue that would ommit databases who's log had never
			been backed up.

	12/22/2011 - Andrew Gould
		Added in exclusion logic.  Any database that is loaded into
			[Admin].dbo.Backup_ExclusionList will report a WARN instead of ERR.

---------------------------------------------------------------------------
-------------------------------------------------------------------------*/

---------------------------------------------------------------------------
-- Create temporary tables.
---------------------------------------------------------------------------
create table #LastLogBackup
	(DatabaseName			varchar(500)
	, LastLogBackup			datetime)

create table #Results
	(DatabaseName			varchar(500)
	, LastLogBackup			datetime
	, Excluded				bit default 0
	, ExcludedBy			varchar(30)
	, ExcludedOn			datetime
	, ExcludedReason		varchar(max))

---------------------------------------------------------------------------
-- Declare and set variables.
---------------------------------------------------------------------------
declare @Now				datetime
	, @LogBackupThreshold	int
	, @CutOffDate			datetime
	, @XML					xml

select @Now					= getdate()
	, @LogBackupThreshold	= 120

select @CutOffDate			= dateadd(minute,-1 * @LogBackupThreshold,@Now)

---------------------------------------------------------------------------
-- Grab the last log backup for all databases (note this includes database
-- that no longer exist on the server).
---------------------------------------------------------------------------
insert into #LastLogBackup
	(DatabaseName
	, LastLogBackup)
select BS.database_name
	, max(backup_start_date) as LastLogBackup
from msdb.dbo.backupset as BS with (nolock)
where BS.is_copy_only = 0
	and BS.type = 'L'
	and BS.backup_finish_date is not null
group by BS.database_name
order by BS.database_name

---------------------------------------------------------------------------
-- Load of all databases who's last log backup was outside the provided 
-- threshold.  Then flag any that are being excluded.
---------------------------------------------------------------------------
insert into #Results
	(DatabaseName
	, LastLogBackup)
select D.name as DatabaseName
	, LLB.LastLogBackup
from master.sys.databases as D with (nolock)
left outer join #LastLogBackup as LLB with (nolock)
	on D.name = LLB.DatabaseName
where D.state_desc = 'ONLINE'
	and D.recovery_model_desc = 'FULL'
	and D.is_read_only = 0
	and D.is_in_standby = 0 -- Not in Standby
	and (LLB.LastLogBackup < @CutOffDate
		or LLB.LastLogBackup is null)

update #Results
set Excluded = 1
	, ExcludedBy = BEL.ExcludedBy
	, ExcludedOn = BEL.ExcludedOn
	, ExcludedReason = BEL.Reason
from #Results as R
inner join [Admin].dbo.Backup_ExclusionList as BEL with (nolock)
	on R.DatabaseName = BEL.DatabaseName

---------------------------------------------------------------------------
-- Build an XML document of the results.
---------------------------------------------------------------------------
select @XML =
(select 'Current TLog Backups' as Provider
	, DatabaseName as Name
	, getdate() as FailDateTime
	, case
		when Excluded = 1 then
			'Excluded by: ' + ExcludedBy
			+ ' | Excluded on: ' + convert(varchar,ExcludedOn,121)
			+ ' | Reason: ' + ExcludedReason
		when LastLogBackup is not null then
			'TLog backups are ' + cast(datediff(minute,LastLogBackup,@Now) as varchar) + ' minutes old!'
			+ ' Last log backup was ' + convert(varchar,LastLogBackup,121)
		else
			'TLog has never been backed up!'
	end as ErrorInfo
	, case
		when Excluded = 1 then 
			'WARN'
		else 
			'ERR' 
	end as ErrorLevel
from #Results with (nolock)
for xml raw('Alert'), root('Alerts'))

---------------------------------------------------------------------------
-- Sync up with GLaDOS.
---------------------------------------------------------------------------
-- Flag all Disabled Jobs as no longer current.
exec [Admin].dbo.glados_ServerAlertsMarkInactive @Provider = 'Current TLog Backups'

-- Update current Disabled Jobs and insert new ones.
exec [Admin].dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML = @XML

-- Archive and remove any Disabled Jobs that are no longer current.
exec [Admin].dbo.glados_ArchiveServerAlerts @Provider = 'Current TLog Backups'

---------------------------------------------------------------------------
-- Clean up.
---------------------------------------------------------------------------
drop table #LastLogBackup
drop table #Results
---------------------------------------------------------------------------
---------------------------------------------------------------------------
GO
