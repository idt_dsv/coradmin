SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



 /*JW CHANGED '_' to '-' based on http://msdn.microsoft.com/en-us/library/ms190362.aspx 
 "If you use the TRACE_FILE_ROLLOVER option, we recommend that you do not use underscore characters in the original trace file name. If you do use underscores, the following behavior occurs:
SQL Server Profiler does not automatically load or prompt you to load the rollover files (if either of these file rollover options are configured).
The fn_trace_gettable function does not load rollover files (when specified by using the number_files argument) where the original file name ends with an underscore and a numeric value. (This does not apply to the underscore and number that are automatically appended when a file rolls over.)
"
And Added Trace to the end of the file name as the Timestamp can be confused with a rollover number. 
 */
CREATE      PROCEDURE [dbo].[dba_SQLProfilerTrace_sp]
		@minutes int,
		@location nvarchar(128)
AS

SET NOCOUNT ON

DECLARE @rc int,
		@TraceID int,
		@maxfilesize bigint,
		@stoptime datetime,
		@tracename varchar(50)

SET		@maxfilesize = 100
SET		@stoptime = DATEADD(MINUTE, @minutes, GETDATE())
SET		@tracename = @@SERVERNAME + '-' + 
                CAST(DATEPART(year, GETDATE()) AS CHAR(4))
                + CASE WHEN LEN(CAST(DATEPART(month, GETDATE()) AS CHAR(2))) = 1 THEN '0' ELSE '' END
                + CAST(DATEPART(month, GETDATE()) AS VARCHAR(2))
                + CASE WHEN LEN(CAST(DATEPART(day, GETDATE()) AS CHAR(2))) = 1 THEN '0' ELSE '' END
                + CAST(DATEPART(day, GETDATE()) AS VARCHAR(2))
                + '-'
                + CASE WHEN LEN(CAST(DATEPART(hour, GETDATE()) AS CHAR(2))) = 1 THEN '0' ELSE '' END
                + CAST(DATEPART(hour, GETDATE()) AS VARCHAR(2))
                + CASE WHEN LEN(CAST(DATEPART(minute, GETDATE()) AS CHAR(2))) = 1 THEN '0' ELSE '' END
                + CAST(DATEPART(minute, GETDATE()) AS VARCHAR(2))
         + 'Trace'

SET		@location = @location + @tracename

EXEC @rc = master..sp_trace_create
		@TraceID output,
		@options = 2,
		@tracefile = @location,
		@maxfilesize = @maxfilesize,
		@stoptime = @stoptime

if (@rc != 0) goto error

-- Set the events
declare @on bit
set @on = 1
exec master..sp_trace_setevent @TraceID, 10, 1, @on
exec master..sp_trace_setevent @TraceID, 10, 34, @on -- added ObjectName column
exec master..sp_trace_setevent @TraceID, 10, 3, @on
exec master..sp_trace_setevent @TraceID, 10, 8, @on
exec master..sp_trace_setevent @TraceID, 10, 10, @on
exec master..sp_trace_setevent @TraceID, 10, 11, @on
exec master..sp_trace_setevent @TraceID, 10, 13, @on
exec master..sp_trace_setevent @TraceID, 10, 14, @on
exec master..sp_trace_setevent @TraceID, 10, 16, @on
exec master..sp_trace_setevent @TraceID, 10, 17, @on
exec master..sp_trace_setevent @TraceID, 10, 18, @on
exec master..sp_trace_setevent @TraceID, 22, 1, @on
exec master..sp_trace_setevent @TraceID, 22, 34, @on -- added ObjectName column
exec master..sp_trace_setevent @TraceID, 22, 3, @on
exec master..sp_trace_setevent @TraceID, 22, 8, @on
exec master..sp_trace_setevent @TraceID, 22, 10, @on
exec master..sp_trace_setevent @TraceID, 22, 11, @on
exec master..sp_trace_setevent @TraceID, 22, 13, @on
exec master..sp_trace_setevent @TraceID, 22, 14, @on
exec master..sp_trace_setevent @TraceID, 22, 16, @on
exec master..sp_trace_setevent @TraceID, 22, 17, @on
exec master..sp_trace_setevent @TraceID, 22, 18, @on
exec master..sp_trace_setevent @TraceID, 12, 1, @on
exec master..sp_trace_setevent @TraceID, 12, 34, @on -- added ObjectName column
exec master..sp_trace_setevent @TraceID, 12, 3, @on
exec master..sp_trace_setevent @TraceID, 12, 8, @on
exec master..sp_trace_setevent @TraceID, 12, 10, @on
exec master..sp_trace_setevent @TraceID, 12, 11, @on
exec master..sp_trace_setevent @TraceID, 12, 13, @on
exec master..sp_trace_setevent @TraceID, 12, 14, @on
exec master..sp_trace_setevent @TraceID, 12, 16, @on
exec master..sp_trace_setevent @TraceID, 12, 17, @on
exec master..sp_trace_setevent @TraceID, 12, 18, @on

-- Exclude profiler
exec master..sp_trace_setfilter @TraceID, 10, 0, 7, N'SQL Profiler'

-- Set the trace status to start
exec master..sp_trace_setstatus @TraceID, 1

-- display trace id for future references
select TraceID=@TraceID

goto finish

SET NOCOUNT OFF

error: 
select ErrorCode=@rc

finish:





GO
