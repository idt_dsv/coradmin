SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[glados_PointInTimeRecovery]

as

set nocount on

/*-------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 12/02/2011

Purpose:
	This proc runs locally every 2 hours to see if any databases aren't
	maintaining current backups.

Objects:
	[Admin].dbo.Backup_ExclusionList
	master.sys.databases
	msdb.dbo.backupset
	GLaDOS

Revisions:
	12/07/2011 - Andrew Gould
		Broke the full backup lookup step apart so it grabs the last date
		and then looks up the last lsn instead of grabbing max() for both.
		This resolves the "lsn rolling over" issue.

		Changed the full backup from using the first_lsn to checkpoint_lsn.
		99% of the time they match, but not always.

		Added an isnull() check to differential backup date to handle the
		case of tlog backups against a full with no current differentials.

		Added in databases that have never been backed up and are currently
		in read_write.

		Process no longer ignores read_only databases.

	12/09/2011 - Andrew Gould
		Added state_desc check to make sure this process only looks at
		databases that are online.

		Fixed an issue that was preventing databases from being listed.

	12/19/2011 - Andrew Gould
		Updated logical to ignore databases that are in standby.

	12/22/2011 - Andrew Gould
		Removed read_only databases from this check.  A seperate process
		will verify we have current backups for read_only databases
		on Nearstore.

	12/27/2011 - Andrew Gould
		Added in exclusion logic.  Any database that is loaded into
		[Admin].dbo.Backup_ExclusionList will report RPT instead of WARN.

	01/03/2012 - Andrew Gould
		Fixed a bug in the checkpoint_lsn lookup, it could grab the wrong
		lsn if the database managed to be backed up twice at the same
		exact moment (i.e. a full\dif fires at the same time as a log)

To Do:
	Add in threshold lookups from [Admin].dbo.Backup_Databases.
-------------------------------------------------------------------------*/

---------------------------------------------------------------------------
-- Create temp tables.
---------------------------------------------------------------------------
create table #Full
	(DatabaseName			varchar(500)
	, RecoveryModel			nvarchar(60)
	, LastFullBackup		datetime
	, checkpoint_lsn		numeric(25,0))

create table #Dif
	(DatabaseName			varchar(500)
	, LastDifBackup			datetime
	, differential_base_lsn	numeric(25,0))

create table #Log
	(LogID					int identity(1,1)
	, DatabaseName			varchar(500)
	, BackupStartDate		datetime
	, first_lsn				numeric(25,0)
	, last_lsn				numeric(25,0))

create table #LastLogBackupInChain
	(DatabaseName			varchar(500)
	, LastLogBackupInChain	datetime)

create table #PointInTimeRecovery
	(DatabaseName			varchar(500)
	, RecoveryModel			nvarchar(60)
	, PointInTimeRecovery	datetime
	, HoursOutOfSync		int
	, Excluded				bit default 0
	, ExcludedBy			varchar(30)
	, ExcludedOn			datetime
	, ExcludedReason		varchar(max))

---------------------------------------------------------------------------
-- Declare and set variables.
---------------------------------------------------------------------------
declare @Now				datetime
	, @FullCutOff			int
	, @SimpleCutOff			int
	, @XML					xml

select @Now					= getdate()
	, @FullCutOff			= 2
	, @SimpleCutOff			= 36

---------------------------------------------------------------------------
-- Grab the last full backup info for all databases.
---------------------------------------------------------------------------
insert into #Full
	(DatabaseName
	, RecoveryModel
	, LastFullBackup)
select BS.database_name
	, BS.recovery_model
	, max(BS.backup_start_date) as LastFullBackup
from master.sys.databases as D with (nolock)
inner join msdb.dbo.backupset as BS with (nolock)
	on D.name = BS.database_name
	and D.recovery_model_desc = BS.recovery_model collate SQL_Latin1_General_CP1_CI_AS
where D.state_desc = 'ONLINE'
	and D.is_in_standby = 0 -- Not in Standby
	and D.is_read_only = 0 -- Not Read_Only
	and BS.is_copy_only = 0
	and BS.type = 'D'
	and BS.backup_finish_date is not null
group by BS.database_name
	, BS.recovery_model

-- Load any databases that have never been backed up.
insert into #Full
	(DatabaseName
	, RecoveryModel)
select D.name
	, D.recovery_model_desc
from master.sys.databases as D with (nolock)
where D.state_desc = 'ONLINE'
	and D.is_in_standby = 0 -- Not in Standby
	and D.is_read_only = 0 -- Not Read_Only
	and D.name <> 'tempdb'
	and D.name not in
(
	select F.DatabaseName
	from #Full as F with (nolock)
)

-- Lookup the checkpoint_lsn of the last full backup.
-- This lookup is required as the lsn rolls over after numeric(25,0)
update #Full
set checkpoint_lsn = BS.checkpoint_lsn
from #Full as F
inner join msdb.dbo.backupset as BS with (nolock)
	on F.DatabaseName = BS.database_name
	and F.LastFullBackup = BS.backup_start_date
where BS.type = 'D'

---------------------------------------------------------------------------
-- Grab the last dif backup info for all databases in #Full.
---------------------------------------------------------------------------
insert into #Dif
	(DatabaseName
	, LastDifBackup
	, differential_base_lsn)
select BS.database_name
	, max(BS.backup_start_date) as LastDifBackup
	, max(BS.differential_base_lsn) as differential_base_lsn
from #Full as F
inner join msdb.dbo.backupset as BS with (nolock)
	on F.DatabaseName = BS.database_name
	and F.checkpoint_lsn = BS.differential_base_lsn
where BS.is_copy_only = 0
	and BS.type = 'I'
	and BS.backup_finish_date is not null
group by BS.database_name

---------------------------------------------------------------------------
-- Grab all the log backups info that are part of the current chain for
-- all the databases in #Full.
---------------------------------------------------------------------------
insert into #Log
	(DatabaseName
	, BackupStartDate
	, first_lsn
	, last_lsn)
select BS.database_name
	, BS.backup_start_date
	, BS.first_lsn
	, BS.last_lsn
from #Full as F
inner join msdb.dbo.backupset as BS with (nolock)
	on F.DatabaseName = BS.database_name
	and F.checkpoint_lsn = BS.database_backup_lsn
where BS.is_copy_only = 0
	and BS.type = 'L'
	and BS.backup_finish_date is not null
-- The order is crutial here for the self join to look for a broken backup chain!
order by BS.database_name
	, BS.backup_start_date

---------------------------------------------------------------------------
-- Find the last log in the chain for each database.
---------------------------------------------------------------------------
insert into #LastLogBackupInChain
	(DatabaseName
	, LastLogBackupInChain)
select A.DatabaseName
	, min(A.BackupStartDate) as LastLogBackupInChain
from #Log as A
left outer join #Log as B
	on A.LogID + 1 = B.LogID
	and A.DatabaseName = B.DatabaseName
where A.last_lsn <> B.first_lsn
	or B.LogID is null
group by A.DatabaseName

---------------------------------------------------------------------------
-- Calculate each database's point in time recovery.
---------------------------------------------------------------------------
insert into #PointInTimeRecovery
	(DatabaseName
	, RecoveryModel
	, PointInTimeRecovery)
select F.DatabaseName
	, F.RecoveryModel
	, case
		-- A differential backup isn't required for point in time recovery as
		-- long as the tlog backup chain is intact and goes back to the full
		-- backup.  The isnull(X,'1900-01-01') allows the case statement to
		-- handle this situation.
		when L.LastLogBackupInChain > isnull(D.LastDifBackup,'1900-01-01') then L.LastLogBackupInChain
		when D.LastDifBackup > F.LastFullBackup then D.LastDifBackup
		else F.LastFullBackup
	end as PointInTimeRecovery
from #Full as F
left outer join #Dif as D
	on F.DatabaseName = D.DatabaseName
left outer join #LastLogBackupInChain as L
	on F.DatabaseName = L.DatabaseName

-- See how far all the databases are out of sync.
update #PointInTimeRecovery
set HoursOutOfSync = datediff(hh,PointInTimeRecovery,@Now)

update #PointInTimeRecovery
set Excluded = 1
	, ExcludedBy = BEL.ExcludedBy
	, ExcludedOn = BEL.ExcludedOn
	, ExcludedReason = BEL.Reason
from #PointInTimeRecovery as PITR
inner join [Admin].dbo.Backup_ExclusionList as BEL with (nolock)
	on PITR.DatabaseName = BEL.DatabaseName

---------------------------------------------------------------------------
-- Build all the databases that are out of sync into XML.
---------------------------------------------------------------------------
select @XML =
(select 'Point in Time Recovery' as Provider
	, DatabaseName as Name
	, @Now as FailDateTime
	, 'Recovery Model: ' + RecoveryModel
		+ case
			when Excluded = 1 then
				' | Excluded by: ' + ExcludedBy
				+ ' | Excluded on: ' + convert(varchar,ExcludedOn,121)
				+ ' | Reason: ' + ExcludedReason
			when PointInTImeRecovery is not null then
				' | Hours out of Sync: ' + cast(HoursOutOfSync as varchar)
				+ ' | PiT: ' + convert(varchar,PointInTimeRecovery,121) + '.'
			else
				' | Database is not being backed up!'
		end as ErrorInfo
	, case
		when Excluded = 1 then 
			'RPT'
		else 
			'WARN' 
	end as ErrorLevel
from #PointInTimeRecovery
where PointInTimeRecovery is null
	or
	(
		RecoveryModel = 'Full'
		and HoursOutOfSync > @FullCutOff
	)
	or
	(
		-- Typically this will be SIMPLE but will also cover BULK-LOGGED.
		RecoveryModel <> 'Full'
		and HoursOutOfSync > @SimpleCutOff
	)
for xml raw('Alert'), root('Alerts'))

---------------------------------------------------------------------------
-- Sync up GLaDOS.
---------------------------------------------------------------------------
-- Flag all Point in Time Recovery records as no longer current.
exec [Admin].dbo.glados_ServerAlertsMarkInactive @Provider = 'Point in Time Recovery'

-- Update current Point in Time Recovery records and insert new ones.
exec [Admin].dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML = @XML

-- Archive and remove any Point in Time Recovery records that are no longer current.
exec [Admin].dbo.glados_ArchiveServerAlerts @Provider = 'Point in Time Recovery'

---------------------------------------------------------------------------
-- Clean up.
---------------------------------------------------------------------------
drop table #Full
drop table #Dif
drop table #Log
drop table #LastLogBackupInChain
drop table #PointInTimeRecovery
---------------------------------------------------------------------------
---------------------------------------------------------------------------
GO
