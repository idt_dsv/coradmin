SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[glados_ServerAlertsUpsertXML]
	@ServerAlertsXML XML
as
/*
	When		Who		What
	2011-11-30	BGR		Initial Release
	2011-12-01	APG		Added LastFailure = current_timestamp to the update statement.
	2012-01-30	JAW		Added COALESCE current_timestamp to avoid Cannot insert null error.
*/

begin
	--too clever for my own good here...MERGE isn't supported on our SQL Server 2005 boxes. Revel in disabled merge glory!
	--merge dbo.glados_Serveralerts as target
	--using
	--	(select Alerts.Alert.value('@Provider[1]','nvarchar(256)') as Provider
	--		,Alerts.Alert.value('@Name[1]','nvarchar(256)') as Name
	--		,Alerts.Alert.value('@FailDateTime[1]','datetime') as FailDateTime
	--		,Alerts.Alert.value('@ErrorInfo[1]','nvarchar(max)') as ErrorInfo
	--		,Alerts.Alert.value('@ErrorLevel[1]','char(4)') as ErrorLevel
	--	from @ServerAlertsXML.nodes('//Alerts/Alert') as Alerts(Alert)) as source
	--on (target.provider=source.provider and target.name=source.name)
	--when matched then
	--	update set LastFailure = source.FailDateTime,
	--		ErrorInfo = source.ErrorInfo,
	--		IsCurrent = 1,
	--		DateUpdated = current_timestamp,
	--		ErrorLevel = source.ErrorLevel
	--when not matched then
	--	insert (Provider, Name, FirstFailure, LastFailure, ErrorInfo, ErrorLevel, IsCurrent)
	--	values (source.Provider, source.Name, source.FailDateTime, source.FailDateTime, source.ErrorInfo, source.ErrorLevel, 1);
	
	--and now we do it another way...
	
	declare @UpdatedAlerts table (Provider nvarchar(256), Name nvarchar(256));

	update glados_ServerAlerts 
		set LastFailure = current_timestamp
			, ErrorInfo = source.ErrorInfo
			, IsCurrent = 1
			, DateUpdated = current_timestamp
			, ErrorLevel = source.ErrorLevel
	output inserted.Provider, inserted.Name into @UpdatedAlerts
	from glados_Serveralerts as target inner join 
		(select Alerts.Alert.value('@Provider[1]','nvarchar(256)') as Provider
			,Alerts.Alert.value('@Name[1]','nvarchar(256)') as Name
			,Alerts.Alert.value('@FailDateTime[1]','datetime') as FailDateTime
			,Alerts.Alert.value('@ErrorInfo[1]','nvarchar(max)') as ErrorInfo
			,Alerts.Alert.value('@ErrorLevel[1]','char(4)') as ErrorLevel
		from @ServerAlertsXML.nodes('//Alerts/Alert') as Alerts(Alert)) as source
		on (target.provider=source.provider and target.name=source.name)
	
	insert 
		into glados_Serveralerts 
			(Provider
			, Name
			, FirstFailure
			, LastFailure
			, ErrorInfo
			, ErrorLevel
			, IsCurrent)
	select Alerts.Alert.value('@Provider[1]','nvarchar(256)') as Provider
			,Alerts.Alert.value('@Name[1]','nvarchar(256)') as Name
			,COALESCE(Alerts.Alert.value('@FailDateTime[1]','datetime'),current_timestamp) as FailDateTime
			,current_timestamp as LastFailure
			,Alerts.Alert.value('@ErrorInfo[1]','nvarchar(max)') as ErrorInfo
			,Alerts.Alert.value('@ErrorLevel[1]','char(4)') as ErrorLevel
			,1 as IsCurrent
	from @ServerAlertsXML.nodes('//Alerts/Alert') as Alerts(Alert)
	--warning: left anti semi join ahead
	left outer join @UpdatedAlerts exclusion on Alerts.Alert.value('@Provider[1]','nvarchar(256)') = exclusion.Provider and Alerts.Alert.value('@Name[1]','nvarchar(256)') = exclusion.Name
	where exclusion.Provider is null
	and exclusion.Name is null

end;
GO
