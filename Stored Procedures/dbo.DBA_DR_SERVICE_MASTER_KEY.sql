
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[DBA_DR_SERVICE_MASTER_KEY]
AS
/*
3/2/2011 John Wood 
Backup SERVICE MASTER KEYs to \\nearstore\SQLBackups\Daily\MasterKeys
Revision 2
7/16/2014 John Updated for consolidated script locations
*/

DECLARE 
	@CMD VARCHAR(1000),
	@Path VARCHAR(100),
	@Path2 VARCHAR(100),
	@Password VARCHAR(16),
	@ServerName VARCHAR(256),
	@DateString	CHAR(12)

	SELECT @DateString=(REPLACE(CONVERT(CHAR(10),GETDATE(),102),'.','')+REPLACE(CAST(CAST(GETDATE() AS TIME) AS CHAR(5)),':',''))

SELECT @SERVERNAME=REPLACE ( @@SERVERNAME , '\' , '_' );
--SELECT @PATH = '\\nearstore\SQLBackups\Daily\MasterKeys\Current\'+@Servername+'_'+CAST((DATEPART(YYYY, GETDATE())*10000+DATEPART(MM,GETDATE())*100+DATEPART(DD,GetDate())) AS CHAR(8))+'.snk'
SELECT @PATH = BackupPath+'DR_SCRIPTS\DB_masterKey_'+@DateString+'.Snk' FROM [ADMIN].[dbo].[Backup_paths] WHERE Location='local'
SELECT @Password='H1tth3F4n!'

SELECT @CMD='BACKUP SERVICE MASTER KEY TO FILE = '+CHAR(39)+@PATH+CHAR(39)+' 
    ENCRYPTION BY PASSWORD = '+CHAR(39)+@password+CHAR(39)+';'
 
SELECT @Path,@Password,@CMD

EXECUTE (@CMD);

GO
