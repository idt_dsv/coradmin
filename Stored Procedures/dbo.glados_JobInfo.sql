SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[glados_JobInfo]
as
/*
	Proc: glados_JobInfo
	Purpose: This procedure pulls information about jobs from sysjobservers 
	and sysjobs_view. It returns name, last run date and time and error info
	based on last run status for all ENABLED jobs.

	When		Who	What
	05/16/2011	BGR	Rewrite from 2002
	02/26/2013	TDT	Changed sp to check overall job step(step 0) for failure.  Before it was checking any step
					greater than 0.  Unfortunately the join was on datetime, which is written in the							sysjobhistory table as the overall job step time.
*/
execute as login='sa';

create table #job_status (
	job_id uniqueidentifier,
	last_run_date nvarchar(20),
	last_run_time nvarchar(20),
	next_run_date nvarchar(20),
	next_run_time nvarchar(20),
	next_run_schedule_id int,
	requested_to_run int,
	request_source int,
	request_source_id varchar(50),
	running int,
	current_step int,
	current_retry_attempts int,
	state int
)

create table #errorMessages (job_id uniqueidentifier,message_id int, ErrorLevel char(4));

declare @DefaultAlertMessageID int;
declare @ErrorMessage nvarchar(max);

select @DefaultAlertMessageID = AlertMessageID
from dbo.glados_AlertMessages with (nolock)
where Prefix = 'DEFAULT';

if @@rowcount < 1
	set @ErrorMessage = @ErrorMessage + N'Error! No default alert message defined.';

insert into #errorMessages (job_id, message_id, ErrorLevel)
select sj.job_id, coalesce(am.AlertMessageID, @DefaultAlertMessageID), coalesce(am.ErrorLevel, 'INFO')
from msdb.dbo.sysjobs sj with(nolock)
	left outer join dbo.glados_AlertMessages am on (sj.[name] like am.prefix + '%');

insert into #job_status
execute master.dbo.xp_sqlagent_enum_jobs 1, 'sa';

update #job_status 
	set last_run_time = right ('000000' + last_run_time, 6),
	next_run_time = right ('000000' + next_run_time, 6)

select
	'JobMonitor' as Provider,
	case
		when js.running = 0 then a.[name]
		else a.[name] + ' - Job is Currently Executing'
	end 'Name',
	case
		when js.last_run_date > 0 then
			convert (datetime,
				substring (js.last_run_date, 1, 4)
				+ '-'
				+ substring (js.last_run_date, 5, 2)
				+ '-'
				+ substring (js.last_run_date, 7, 2)
				+ ' '
				+ substring (js.last_run_time, 1, 2)
				+ ':'
				+ substring (js.last_run_time, 3, 2)
				+ ':'
				+ substring (js.last_run_time, 5, 2)
				+ '.000',
				121
			)
		else
			null
	end FailDateTime,	
	case
		when b.last_run_outcome = 0 AND js.running = 0 then ja.AlertMessage + ' [ERROR MESSAGE] ' + h.message
		else null
	end 'ErrorInfo',
	em.ErrorLevel
from msdb.dbo.sysjobservers b with (nolock)
	inner join msdb.dbo.sysjobhistory as h with (nolock)
		on (b.job_id = h.job_id
		and b.last_run_date = h.run_date
		and b.last_run_time = h.run_time)
	inner join msdb.dbo.sysjobs a with (nolock)
		on (b.job_id = a.job_id)
	inner join #job_status js with (nolock)
		on (a.job_id = js.job_id)
	inner join #errorMessages em with (nolock)
		on (a.job_id = em.job_id)
	inner join dbo.glados_AlertMessages ja with (nolock)
		on (em.message_id = ja.AlertMessageID)
where a.enabled <> 0
	and h.run_status = 0
	and h.step_id = 0
and case 
		when b.last_run_outcome = 0 AND js.running = 0 THEN ja.AlertMessage
        else null
	end is not null;

drop table #job_status;
drop table #errorMessages;

revert;

GO
GRANT EXECUTE ON  [dbo].[glados_JobInfo] TO [glados]
GO
