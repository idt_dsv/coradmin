SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE proc [dbo].[usp_Backup_Step3_BackupMidDayFulls]

as

set nocount on

/*------------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 07/17/12

Purpose: 
	Backup each database and verify the backup if new database in mid day, or if database came out of read_only
	or if recovery model changes from Simple to Full or Bulk_Logged.
	
Revisions:

	02/06/2014 - Travis Tittle
		Removed Compression flag, as New Build Doc requires Compression already be set in sys.configurations.

To Do:
	None
	
Notes:

------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp table.
--------------------------------------------------------------------------------

create table #FullBackup
	(BackupID int identity(1,1)
	, BackupType varchar(10)
	, DatabaseID int
	, LocalPathID int
	, DatabaseName varchar(100)
	, BackupFullCommand varchar(500)
	, VerificationFullCommand varchar(500))
	
create table #MD
	(MDID int identity(1,1)
	, MDCommand varchar(500))
	
create table #DelCommand
    (DeleteID int identity(1,1)
    , DeleteCommand varchar(300))
	
--------------------------------------------------------------------------------
-- Declare Variables.
--------------------------------------------------------------------------------

declare @Start int
	, @End int
	, @StartDel int
	, @EndDel int
	, @DatabaseID int
	, @BackupFullCommand varchar(500)
	, @VerificationFullCommand varchar(500)
	, @BackupFullError int
	, @VerificationFullError int
	, @Today datetime
	, @StartMD int
	, @EndMD int
	, @MDCommand varchar(500)
	, @DeleteCommand varchar(300)
	
select @BackupFullError = -1
select @VerificationFullError = -1
select @Today = getdate()

--------------------------------------------------------------------------------
-- Remove Last Local Full Backup Prior to Full Backup for capacity concerns.
--------------------------------------------------------------------------------	
insert into #DelCommand
   (DeleteCommand)
select 'xp_cmdshell ''del '
      + '"'
      + D.BackupFile
      + '"'
      + ''''
from Admin.dbo.Backup_Databases as D with (nolock)
where D.NeedsFull = 1
and D.BackupFile <> '-1'
--------------------------------------------------------------------------------
select @StartDel = min(DeleteID) from #DelCommand with (nolock)
select @EndDel = max(DeleteID) from #DelCommand with (nolock)
	
while @StartDel <= @EndDel
begin
	select @DeleteCommand = DeleteCommand
	from #DelCommand with (nolock)
	where DeleteID = @StartDel
	
	exec (@DeleteCommand)
	
    select @StartDel = @StartDel + 1
end		
--------------------------------------------------------------------------------
-- If Full Recovery Model and Full Backup has yet to be created, or if Database
-- has been newly changed to a Full Recovery Model, edit metadata to alert step a full needs created first.
-----------------------------------------------------

update ADMIN.dbo.Backup_Databases
set BackupFile = P.BackupPath
		+ D.FolderName 
		+ '\'
		+ D.DatabaseName
		+ '_'
		-- This jumbled mess takes a date and trims of the seconds and miliseconds and removes all formating.
		+ left(replace(replace(replace(convert(varchar,@Today,121),' ',''),':',''),'-',''),12) 
		+ '.bak'
from ADMIN.dbo.Backup_Databases as D with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on D.LocalPathID = P.PathID
where D.NeedsFull = 1
and D.DontBackup = 0

insert into #FullBackup
	(BackupType
	, DatabaseID
	, LocalPathID
	, DatabaseName
	, BackupFullCommand
	, VerificationFullCommand)
select 'DATABASE'
    , DatabaseID
    , LocalPathID
    , DatabaseName
	, 'BACKUP DATABASE [' 
		+ DatabaseName 
		+ '] TO DISK = '''
		+ BackupFile
		+ ''''
	, 'RESTORE VERIFYONLY FROM DISK = '''
		+ BackupFile
		+ ''''
from ADMIN.dbo.Backup_Databases as D with (nolock)
where D.NeedsFull = 1
and D.DontBackup = 0

--------------------------------------------------------------------------------
-- Build a list of create LOCAL directory tree commands.
--------------------------------------------------------------------------------
insert into #MD
	(MDCommand)
select 'xp_cmdshell ''md "' + P.BackupPath + FB.DatabaseName + '"'''
from #FullBackup as FB with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on FB.LocalPathID = P.PathID

--------------------------------------------------------------------------------
-- Walk through #MD executing the commands.
--------------------------------------------------------------------------------	
select @StartMD = min(MDID) from #MD with (nolock)
select @EndMD = max(MDID) from #MD with (nolock)

while @StartMD <= @EndMD
begin
	select @MDCommand = MDCommand
	from #MD with (nolock)
	where MDID = @StartMD
	
	exec(@MDCommand)
	
	select @StartMD = @StartMD + 1
end

--------------------------------------------------------------------------------
-- Walk through #FullBackup.  Attempt to backup the database, if that works attempt
-- to verify it.  If verification passes update ADMIN.dbo.Backup_Databases
-- with the time of completion.  Else throw an error.
--------------------------------------------------------------------------------
select @Start = min(BackupID) from #FullBackup with (nolock)
select @End = max(BackupID) from #FullBackup with (nolock)
	
while @Start <= @End
begin
	select @DatabaseID = DatabaseID
		, @BackupFullCommand = BackupFullCommand
		, @VerificationFullCommand = VerificationFullCommand
	from #FullBackup with (nolock)
	where BackupID = @Start
	
	exec (@BackupFullCommand)
	select @BackupFullError = @@error
	
	if @BackupFullError = 0
	begin
		exec (@VerificationFullCommand)
		select @VerificationFullError = @@error
		
		if @VerificationFullError = 0
		begin
			update ADMIN.dbo.Backup_Databases
			set LastBackupDate = getdate(),
			    NeedsFull = 0
			where DatabaseID = @DatabaseID
		end
		else
		begin
			insert into ADMIN.dbo.Backup_ErrorLog
				(Command
				, Error
				, ErrorDate)
			select @VerificationFullCommand
				, 'ERROR: ' + @VerificationFullError
				, @Today
		end
	end
	else
	begin
		insert into ADMIN.dbo.Backup_ErrorLog
			(Command
			, Error
			, ErrorDate)
		select @BackupFullCommand
			, 'ERROR: ' + @BackupFullError
			, @Today
	end
	
	select @Start = @Start + 1
end


--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #FullBackup
drop table #MD


GO
