SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO








/*****************************************************************
Created By: Travis Tittle
Date: 04/25/2012
Purpose: Use this stored proc to apply or re-apply database permissions. 

Associated Tables: admin.dbo.Permissions_Databases
*****************************************************************/


CREATE PROCEDURE [dbo].[Permissions_AddDBPermissions]    @Database varchar(100)

AS

/***************************************************
Create Temp Tables
***************************************************/
CREATE TABLE #AddPermissionCommands
(CommandID int identity(1,1),
 DatabaseName varchar(100) null,
 Command varchar(500) null)
 
/***************************************************
Declare Variables
***************************************************/ 
DECLARE @Start int
DECLARE @End int
DECLARE @Command varchar(500)

/***************************************************
Build Commands
***************************************************/  
INSERT INTO #AddPermissionCommands
(Databasename,
 Command)
SELECT @Database,
       'USE '
       + pd.DatabaseName
       + ' '
       + pd.Command
FROM Admin.dbo.Permissions_Databases as pd with (nolock)
WHERE pd.DatabaseName = '[' + @Database + ']'
AND pd.PermissionType <> 'Header'
ORDER BY DisplayOrder

/***************************************************
Run Through Commands
***************************************************/  
SELECT @Start = min(CommandID) FROM #AddPermissionCommands with (nolock)
SELECT @End = max(CommandID) FROM #AddPermissionCommands with (nolock)
	
WHILE @Start <= @End
BEGIN
	SELECT @Command = Command
	FROM #AddPermissionCommands with (nolock)
	WHERE CommandID = @Start
	
	EXEC (@Command)
	
    SELECT @Start = @Start + 1
END










GO
