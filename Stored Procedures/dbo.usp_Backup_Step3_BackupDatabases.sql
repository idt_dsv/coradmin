SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE proc [dbo].[usp_Backup_Step3_BackupDatabases]
	@Debug int = 0
as

set nocount on

/*------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 10/23/2009

Purpose: 
	Backup each database and verify the backup.
	
Revisions:
	10/27/2009 - Andrew Gould
		Added transaction log backups to process.
		Added better error handling.
	3/23/2011 - Barry Randall
		Added compression for 2008 R2 servers
	4/28/2011 - Travis Tittle
		Added Brackets to Backup Database Command to capture entire Database Name
		Changed timeframe of when to Backup from later than 20hrs to later than 150hrs(6.5days)
	6/06/2011 - Travis Tittle
		Removed timeframe.  Now backups run no matter what.
		No longer attempting to back up databases with any other state than ONLINE.
	6/14/2011 - Travis Tittle
		Removed reliance on Backup Device.
	6/27/2011 - Travis Tittle
		Added Delete step prior to Full Backup.
	12/13/2011 - Travis Tittle
		Added BackupGroup.
	02/06/2014 - Travis Tittle
		Removed Compression flag, as New Build Doc requires Compression already be set in sys.configurations.
To Do:
	None
	
Notes:
	For a database to be listed to be backed up the following three conditions
	must be met.
	1.) It must be flagged to be backed up (DontBackup = 0)
	2.) It must be ONLINE.
	3.) It must have a BackupFile.
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp table.
--------------------------------------------------------------------------------
create table #Backup
	(BackupID int identity(1,1)
	, BackupType varchar(10)
	, DatabaseID int
	, BackupCommand varchar(500)
	, VerificationCommand varchar(500))
	
create table #DelCommand
    (DeleteID int identity(1,1)
    , DeleteCommand varchar(300))

--------------------------------------------------------------------------------
-- Declare Variables.
--------------------------------------------------------------------------------
declare @Start int
	, @End int
	, @Start2 int
	, @End2 int
	, @DeleteCommand varchar(300)
	, @DatabaseID int
	, @BackupCommand varchar(500)
	, @VerificationCommand varchar(500)
	, @BackupError int
	, @VerificationError int
	, @Today datetime

select @BackupError = -1
select @VerificationError = -1
select @Today = getdate()
	
	
--------------------------------------------------------------------------------
-- Remove Last Local Full Backup Prior to Full Backup for capacity concerns.
--------------------------------------------------------------------------------	
insert into #DelCommand
   (DeleteCommand)
select 'xp_cmdshell ''del '
      + '"'
      + BackupFile
      + '"'
      + ''''
from Admin.dbo.Backup_Databases with (nolock)
where DontBackup = 0
	and BackupFile <> '-1'
	and Database_Status = 'ONLINE'
	and BackupGroup = 1
--------------------------------------------------------------------------------
select @Start = min(DeleteID) from #DelCommand with (nolock)
select @End = max(DeleteID) from #DelCommand with (nolock)
	
while @Start <= @End
begin
	select @DeleteCommand = DeleteCommand
	from #DelCommand with (nolock)
	where DeleteID = @Start
	
	exec (@DeleteCommand)
	
    select @Start = @Start + 1
end

--------------------------------------------------------------------------------
-- Loads backup and restore commands into #Backup for database backups.
-- Note: The database must be flagged to be backed up (DontBackup = 0)
-- and have a status of ONLINE.
--------------------------------------------------------------------------------
update ADMIN.dbo.Backup_Databases
set BackupFile = P.BackupPath
		+ D.FolderName 
		+ '\'
		+ D.DatabaseName
		+ '_'
		-- This jumbled mess takes a date and trims of the seconds and miliseconds and removes all formating.
		+ left(replace(replace(replace(convert(varchar,@Today,121),' ',''),':',''),'-',''),12) 
		+ '.bak'
from ADMIN.dbo.Backup_Databases as D with (nolock)
inner join ADMIN.dbo.Backup_Paths as P with (nolock)
	on D.LocalPathID = P.PathID
where D.DontBackup = 0
and D.Database_Status = 'ONLINE'
and D.BackupGroup = 1

insert into #Backup
	(BackupType
	, DatabaseID
	, BackupCommand
	, VerificationCommand)
select 'DATABASE'
    , DatabaseID
	, 'BACKUP DATABASE [' 
		+ DatabaseName 
		+ '] TO DISK = '''
		+ BackupFile
		+ ''''
	, 'RESTORE VERIFYONLY FROM DISK = '''
		+ BackupFile
		+ ''''
from ADMIN.dbo.Backup_Databases as D with (nolock)
where DontBackup = 0
	and BackupFile <> '-1'
	and Database_Status = 'ONLINE'
	and BackupGroup = 1


--------------------------------------------------------------------------------
-- ***DEBUG***
-- If debug flag is set, return the list of commands to run, but don't
-- execute them.
--------------------------------------------------------------------------------	
if @Debug = 1
begin
	select * from #Backup with (nolock)

	return
end

--------------------------------------------------------------------------------
-- Walk through #Backup.  Attempt to backup the database, if that works attempt
-- to verify it.  If verification passes update ADMIN.dbo.Backup_Databases
-- with the time of completion.  Else throw an error.
--------------------------------------------------------------------------------
select @Start2 = min(BackupID) from #Backup with (nolock)
select @End2= max(BackupID) from #Backup with (nolock)
	
while @Start2 <= @End2
begin
	select @DatabaseID = DatabaseID
		, @BackupCommand = BackupCommand
		, @VerificationCommand = VerificationCommand
	from #Backup with (nolock)
	where BackupID = @Start2
	
	exec (@BackupCommand)
	select @BackupError = @@error
	
	if @BackupError = 0
	begin
		exec (@VerificationCommand)
		select @VerificationError = @@error
		
		if @VerificationError = 0
		begin
			update ADMIN.dbo.Backup_Databases
			set LastBackupDate = getdate()
			where DatabaseID = @DatabaseID
		end
		else
		begin
			insert into ADMIN.dbo.Backup_ErrorLog
				(Command
				, Error
				, ErrorDate)
			select @VerificationCommand
				, 'ERROR: ' + @VerificationError
				, @Today
		end
	end
	else
	begin
		insert into ADMIN.dbo.Backup_ErrorLog
			(Command
			, Error
			, ErrorDate)
		select @BackupCommand
			, 'ERROR: ' + @BackupError
			, @Today
	end
	
	select @Start2 = @Start2 + 1
end

--------------------------------------------------------------------------------
-- Clean up.
--------------------------------------------------------------------------------
drop table #Backup
drop table #DelCommand
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------









GO
