SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE proc [dbo].[Script_Load]
	@Script varchar(100)
	, @Debug int = 0

as

set nocount on
/*-------------------------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 6/17/2011

Purpose:
	This copies the newest version of the passed proc locally from the repository.

Objects:
	[Admin].dbo.Script_Paths

-------------------------------------------------------------------------------------------*/

/*-------------------------------------------------------------------------------------------
-- Permanent table.
---------------------------------------------------------------------------------------------
create table [Admin].dbo.Script_Paths
	(ScriptPathID			int identity(1,1) primary key
	, Location				varchar(100)
	, ScriptPath			varchar(500))

insert into [Admin].dbo.Script_Paths
	(Location
	, ScriptPath)
select 'Local'
	, 'C:\DBAScripts'
union
select 'Repository'
	, '\\PRODSQL4\DataServices\ScriptRepository'
---------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------*/

---------------------------------------------------------------------------------------------
-- Declare variables.
---------------------------------------------------------------------------------------------
declare @LocalScriptPath	varchar(100)
	, @RepositoryScriptPath	varchar(500)
	, @SQL					varchar(max)

---------------------------------------------------------------------------------------------
-- Lookup local and repository script paths.
---------------------------------------------------------------------------------------------
select @LocalScriptPath	= ScriptPath
from [Admin].dbo.Script_Paths with (nolock)
where Location = 'Local'

select @RepositoryScriptPath = ScriptPath
from [Admin].dbo.Script_Paths with (nolock)
where Location = 'Repository'

---------------------------------------------------------------------------------------------
-- Search for script locally.
---------------------------------------------------------------------------------------------
select @SQL = 'xp_cmdshell ''echo D|xcopy /Y ' + @RepositoryScriptPath + '\' + @Script + ' ' + @LocalScriptPath + ''''

if @Debug = 1
begin
	print '-- Copying script from repository.'
	print @SQL
	print ''
end
else
begin
	exec(@SQL)
end
---------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------
GO
