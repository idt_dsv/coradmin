SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE proc [dbo].[glados_GlobalBackupCheck]
as

set nocount on

/*-------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 08/28/2013

Purpose:
	This proc runs locally every 15 minutes and loads all databases that aren't being backed up, but should be.

Revisions: 11/1/13 - tt - Added catch for OFFLINE databases.	

Objects:
-------------------------------------------------------------------------*/

-- Create Temp Table
create table #DatabaseList
   (databaseid int identity(1,1),
    databasename varchar(150))
    
-- Declare variables
declare @XML xml

-- Get database names that should be backed up
insert into #DatabaseList
   (databasename)
select bd.databasename
from dbo.Backup_Databases as bd with (nolock)
inner join sys.databases as sd with (nolock)
   on sd.name = bd.databasename
where bd.DontBackup = 1
and bd.DatabaseName <> 'tempdb'
and (sd.is_read_only = 0 and sd.is_in_standby = 0)
and sd.state_desc = 'ONLINE'

--Need to then check exclusion list
delete from #DatabaseList
from #DatabaseList as dl
join Admin.dbo.Backup_NoBackup_Exclusions as b
   on b.databasename = dl.databasename

-- Lookup all non backed up databases and build the list as XML.
select @XML =
(select 'Missing Database Backup' as Provider
	, databasename as Name
	, getdate() as FailDateTime
	, 'Need to change DontBackup flag in dbo.backup_databases' as ErrorInfo
	, 'WARN' as ErrorLevel
from #DatabaseList
for xml raw('Alert'), root('Alerts'))

-- Flag all Disabled Jobs as no longer current.
exec [Admin].dbo.glados_ServerAlertsMarkInactive @Provider = 'Missing Database Backup'

-- Update current Disabled Jobs and insert new ones.
exec [Admin].dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML = @XML

-- Archive and remove any Disabled Jobs that are no longer current.
exec [Admin].dbo.glados_ArchiveServerAlerts @Provider = 'Missing Database Backup'
---------------------------------------------------------------------------
---------------------------------------------------------------------------








GO
