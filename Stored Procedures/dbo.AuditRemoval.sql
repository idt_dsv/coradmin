SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[AuditRemoval] (@DatabaseName varchar(150), @RetentionPeriod int)

AS

/*------------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 09/27/2012

Purpose: 
	An Audit Record Archiving(Deleting) strategy.  This is to help limit the amount of data we save in our 
	Audit.ChangeHistory table.
	
Revisions:
	
Notes:  There are multiple Auditing strategies currently throughout our environment.  This pariticular
        Archiving(Deleting) strategy was created strictly for the Audit.ChangeHistory Auditing process.
        
Objects Involved:
        Table - Audit.ChangeHistory
        Table - Admin.dbo.Audit_RemovalLog
        
Testing:   
--DECLARE @RetentionPeriod int
--DECLARE @DatabaseName varchar(150)

--SET @RetentionPeriod = 30
--SET @DatabaseName = 'Admin'
---------------------------
	
------------------------------------------------------------------------------*/

--------------------------------------------------------------------------------
-- Create temp tables.
--------------------------------------------------------------------------------

CREATE TABLE #RetentionCommand
   (CommandID int identity(1,1),
    Command varchar(500))

--------------------------------------------------------------------------------
-- Declare Variables
--------------------------------------------------------------------------------

DECLARE @Command varchar(500)
DECLARE @RetentionDate datetime
DECLARE @DeleteCount int

SET @RetentionDate = dateadd(dd,-@RetentionPeriod,GETDATE())

--------------------------------------------------------------------------------
-- Fail if Databasename is NULL or RetentionDate is NULL
--------------------------------------------------------------------------------
IF @DatabaseName IS NULL
OR @RetentionDate IS NULL

   BEGIN
   PRINT 'Must specify a database and retention period to use this stored procedure'
   END
--------------------------------------------------------------------------------
-- Else if DatabaseName and Retention Period exist, run through Delete script
--------------------------------------------------------------------------------

ELSE IF @DatabaseName IS NOT NULL
     AND @RetentionPeriod IS NOT NULL

  BEGIN
     INSERT INTO #RetentionCommand
        (Command)
     SELECT 'DELETE FROM '
            + @DatabaseName
            + '.Audit.ChangeHistory'
            + ' WHERE UpdateDtm <= '
            + CHAR(39)
            + CAST(@RetentionDate AS varchar(25))
            + CHAR(39)
            
     SELECT @Command = Command
     FROM #RetentionCommand 
      
     EXEC (@Command)
     
     SELECT @DeleteCount = @@rowcount

     INSERT INTO Admin.dbo.Audit_RemovalLog               
      (DatabaseName,
       RecordsRemoved,
       RemovalDate)
     SELECT @DatabaseName,
          @DeleteCount,
          GETDATE()
  END 


DROP TABLE #RetentionCommand
GO
