SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [dbo].[Disks_Monitor] (@MonitorResult xml output) as
begin
	declare @FixedDrives table (
		drive char(1),
		mbfree int
	);
	insert into @FixedDrives (drive,mbfree)
	execute xp_fixeddrives;

	set @MonitorResult = (
		select 
			'DriveSpace' as Provider
			,f.drive as Name
			,current_timestamp as FailDateTime
			,'Drive (' + isnull(d.Label,'No Label Set') +') is ' + left(cast((d.sizemb-f.mbfree)/(cast(d.sizemb as decimal(10,2)))*100 as varchar(20)),5) + '% full (' + cast(mbfree as varchar(20)) + ' MB free)' as ErrorInfo
			,case MonitorType
				when 'pct' then 
					case
						when (d.sizemb-f.mbfree)/(cast(d.sizemb as decimal(10,2))) between isnull(WarnPct,80)/100.0 and isnull(ErrPct,90)/100.0 then 'WARN'
						when (d.sizemb-f.mbfree)/(cast(d.sizemb as decimal(10,2))) >= isnull(ErrPct,90)/100.0 then 'ERR'
						else 'GOOD'
					end
				when 'mb' then
					case
						when f.mbfree between isnull(ErrMbFree,5000) and isnull(WarnMbFree,10000) then 'WARN'
						when f.mbFree <= isnull(ErrMbFree,5000) then 'ERR'
						else 'GOOD'
					end
			end as ErrorLevel
		
		from @FixedDrives f inner join admin.dbo.Disks d 
			on f.drive=d.DriveLetter
		where case MonitorType
				when 'pct' then 
					case
						when (d.sizemb-f.mbfree)/(cast(d.sizemb as decimal(10,2))) between isnull(WarnPct,80)/100.0 and isnull(ErrPct,90)/100.0 then 'WARN'
						when (d.sizemb-f.mbfree)/(cast(d.sizemb as decimal(10,2))) >= isnull(ErrPct,90)/100.0 then 'ERR'
						else 'GOOD'
					end
				when 'mb' then
					case
						when f.mbfree between isnull(ErrMbFree,5000) and isnull(WarnMbFree,10000) then 'WARN'
						when f.mbFree <= isnull(ErrMbFree,5000) then 'ERR'
						else 'GOOD'
					end
			end in ('WARN','ERR')
		for xml raw('Alert'), root('Alerts')
	)
end

GO
