SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[glados_ServerAlertsMarkInactive]
	@Provider nvarchar(128)	
as
begin
	update dbo.glados_ServerAlerts set IsCurrent = 0 
	from dbo.glados_ServerAlerts
	where Provider=@Provider;
end;


GO
GRANT EXECUTE ON  [dbo].[glados_ServerAlertsMarkInactive] TO [IDT-CORALVILLE\srv_PRODSQL3]
GO
