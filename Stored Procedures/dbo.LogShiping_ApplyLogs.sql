SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[LogShiping_ApplyLogs]
	(@GroupID INTEGER =1 )
	AS 
/*
Log Shipping Step 3 Restore the shipped logs to database. 
Revision 
John Wood 3/25/2011

Revision 2: 4/26/2011 Added GroupId Logic to increase Processing controls.
Revision 2.5: 4/27/2011 Renamed and converted to new [Admin]Database --john
Revision 3 5/26/2011 Changed logic to support [ADMIN].dbo.LogShipQueue (FilePath) to contain the full path instead of file name
Revision 4 2/01/2012 Incorporate Retry logic.
*/
SET NOCOUNT ON ;


CREATE TABLE #LogApplier
	(
	ID INTEGER IDENTITY (1,1),
	QueueId INTEGER,
	Command VARCHAR(1000)
	);
	
DECLARE 
	@CMD VARCHAR(1000),
	@LoopAt INTEGER,
	@LoopMax INTEGER,
	@QueueId INTEGER,
	@ErrorText NVARCHAR(2048);
	
-----------------------------------------------------
--Build Commands and Order Restores
-----------------------------------------------------
WITH LastSuccess(Shippment, LastApplied)
 AS 
 (SELECT 
	LSQ.ShippmentID
	,MAX(LSQ.FileDate)
FROM  ADMIN.dbo.LogShipQueue AS LSQ
WHERE LSQ.AppliedDateTime IS NOT NULL 
GROUP BY LSQ.ShippmentID)

INSERT INTO #LogApplier
	(QueueId, Command)
SELECT 
	LSQ.QueueId,
	'RESTORE LOG ['+Ls.TargetDatabase+'] FROM DISK='+CHAR(39)+LS.SourceFolder+'\'+LSQ.FilePath+CHAR(39)+'  WITH STANDBY='+CHAR(39)+LS.TargetStandByFile+CHAR(39)++';'
FROM [ADMIN].dbo.LogShipment AS LS 
LEFT OUTER JOIN LastSuccess AS V
	ON LS.ShippmentID=V.Shippment
INNER JOIN [ADMIN].dbo.LogShipQueue AS LSQ
	ON LS.ShippmentID=LSQ.ShippmentID
WHERE /*(LSQ.ERROR IS NULL OR LSQ.Error=0)
	AND*/ AppliedDateTime IS NULL 
	AND (LS.MaintenanceMode IS NULL OR LS.MaintenanceMode=0)
	AND LSQ.FileDate<=DATEADD(mi, -1*LS.MinProcessingDelayMin,GETDATE())
	AND (LS.SkipStandBy =0  OR LS.SkipStandBy IS NULL)
	AND LS.GroupID=@GroupID
	AND LSQ.FileDate>V.LastApplied
ORDER BY LSQ.ShippmentID,LSQ.FilePath;

------------------------------------------------------
--Handle older versions and non standby Restores
------------------------------------------------------

WITH LastSuccess(Shippment, LastApplied)
 AS 
 (SELECT 
	LSQ.ShippmentID
	,MAX(LSQ.FileDate)
FROM  ADMIN.dbo.LogShipQueue AS LSQ
WHERE LSQ.AppliedDateTime IS NOT NULL 
GROUP BY LSQ.ShippmentID)

INSERT INTO #LogApplier
	(QueueId, Command)
SELECT 
	LSQ.QueueId,
	'RESTORE LOG ['+Ls.TargetDatabase+'] FROM DISK='+CHAR(39)+LS.SourceFolder+'\'+LSQ.FilePath+CHAR(39)+'  WITH NORECOVERY;'
FROM [ADMIN].dbo.LogShipment AS LS 
INNER JOIN [ADMIN].dbo.LogShipQueue AS LSQ
	ON LS.ShippmentID=LSQ.ShippmentID
LEFT OUTER JOIN LastSuccess AS V
	ON LS.ShippmentID=V.Shippment
WHERE /*(LSQ.ERROR IS NULL OR LSQ.Error=0)
	AND*/ AppliedDateTime IS NULL 
	AND (LS.MaintenanceMode IS NULL OR LS.MaintenanceMode=0)
	AND LSQ.FileDate<=DATEADD(mi, -1*LS.MinProcessingDelayMin,GETDATE())
	AND LS.SkipStandBy =1 
	--AND LS.GroupID=@GroupID
	AND LSQ.FileDate>V.LastApplied
ORDER BY LSQ.ShippmentID,LSQ.FilePath;

-----------------------------------------------------
--Loop Control
-----------------------------------------------------
SELECT @LoopAt = MIN(ID) , @LoopMax=MAX(Id)
FROM #LogApplier;
-----------------------------------------------------
PRINT CAST(@LoopMax AS VARCHAR(10))+' Logs To Apply'
-----------------------------------------------------
--APPLY Logs 
-----------------------------------------------------
--Select * from #LogApplier

WHILE @LoopAt<=@LoopMax
	BEGIN
		SELECT 
			@CMD=Command,
			@queueID=QueueId
		FROM #LogApplier 
		WHERE ID=@LoopAt;
--SELECT @CMD;
		BEGIN TRY
			EXECUTE (@CMD)
		END TRY
		
		BEGIN CATCH 
			UPDATE [ADMIN].dbo.LogShipQueue
				SET ERROR=1,
					ErrorText=ERROR_MESSAGE(),
					ErrorCommand = @Cmd,
					RetrysAtempted = COALESCE(RetrysAtempted+1,0)
			WHERE QueueId=@QueueID
		END CATCH
			
		UPDATE [ADMIN].dbo.LogShipQueue
		SET AppliedDateTime=GETDATE(),
			ERROR=0
		WHERE QueueId=@QueueID
		AND (ERROR <> 1 OR ERROR IS NULL);
		
		SELECT @LoopAt=@LoopAt+1 , @CMD='',@queueID=-1;	
	END
----------------------------------------------------------
DROP TABLE #LogApplier;
GO
