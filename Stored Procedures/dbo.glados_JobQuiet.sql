SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE proc [dbo].[glados_JobQuiet]
as

set nocount on

/*-------------------------------------------------------------------------
Created By: Andrew Gould
Created On: 12/02/2011

Purpose:
	This proc runs locally once a night and loads all quiet jobs into
	GLaDOS as information messages.

Revisions:
	12/14/2011 - Andrew Gould
		Added in exclusions for Replication Jobs, Report Server Jobs and
		a new job category Known Quiet Jobs.

Objects:
	msdb.dbo.sysjobservers
	msdb.dbo.sysjobs
	GLaDOS
-------------------------------------------------------------------------*/

---------------------------------------------------------------------------
-- Create temp tables.
---------------------------------------------------------------------------
create table #Quiet
	(JobName varchar(500)
	, JobDescription varchar(max)
	, last_run_date char(8)
	, last_run_time char(6)
	, LastRan datetime)

---------------------------------------------------------------------------
-- Declare and set variables.
---------------------------------------------------------------------------
-- Since SQL 2005 and older don't know about the date data type we have to
-- use datetime and some string shenanigans to remove the time.
declare @MonthAgo datetime
	, @MonthAgoINT int
	, @XML xml

select @MonthAgo = dateadd(month,-1,getdate())

select @MonthAgoINT = replace(left(convert(varchar,@MonthAgo,121),10),'-','')

---------------------------------------------------------------------------
-- Build and XML list of all jobs that haven't ran in a month.
---------------------------------------------------------------------------
insert into #Quiet
	(JobName
	, JobDescription
	, last_run_date
	, last_run_time)
select SJ.Name
	, SJ.[description]
	, SO.last_run_date
	, SO.last_run_time
from msdb.dbo.sysjobservers as SO with (nolock)
inner join msdb.dbo.sysjobs as SJ with (nolock)
	on SO.job_id = SJ.job_id
	and SO.server_id = SJ.originating_server_id
inner join msdb.dbo.syscategories as SC with (nolock)
	on SJ.category_id = SC.category_id
where SC.name not like 'REPL-%'			-- Ignore all replication jobs.
	and SC.name <> 'Report Server'		-- Ignore all report server schedules.
	and SC.name <> 'Known Quiet Jobs'	-- Ignore all jobs in this category.
	and last_run_date < @MonthAgoINT

-- Since sysjobservers stores the run date and run time
-- as an int we need to append leading zeros for anything
-- that happens between midnight (0) and 10am (100000).
while (select min(len(last_run_date)) from #Quiet) < 8
begin
	update #Quiet
	set last_run_date = '0' + last_run_date
	where len(last_run_date) < 8
end

while (select min(len(last_run_time)) from #Quiet) < 6
begin
	update #Quiet
	set last_run_time = '0' + last_run_time
	where len(last_run_time) < 6
end

-- Convert the ints for last_run_date and last_run_time
-- into a datetime format.
update #Quiet
set LastRan = left(last_run_date,4)
	+ '-' + substring(right(last_run_date,4),1,2)
	+ '-' + right(last_run_date,2)
	+ ' ' + left(last_run_time,2)
	+ ':' + substring(right(last_run_time,4),1,2)
	+ ':' + right(last_run_time,2)
where last_run_date <> '00000000'

-- Lookup all jobs that havent' ran in a month and build the list as XML.
select @XML =
(select 'Quiet Jobs' as Provider
	, JobName as Name
	, getdate() as FailDateTime
	, JobDescription
		+ '  Last ran: ' + isnull(convert(varchar,LastRan,121),'Never')
		+ '.' as ErrorInfo
	, 'RPT' as ErrorLevel
from #Quiet
for xml raw('Alert'), root('Alerts'))

---------------------------------------------------------------------------
-- Clear, set then archive Quiet Job GLaDOS alerts.
---------------------------------------------------------------------------
-- Flag all Disabled Jobs as no longer current.
exec [Admin].dbo.glados_ServerAlertsMarkInactive @Provider = 'Quiet Jobs'

-- Update current Disabled Jobs and insert new ones.
exec [Admin].dbo.glados_ServerAlertsUpsertXML @ServerAlertsXML = @XML

-- Archive and remove any Disabled Jobs that are no longer current.
exec [Admin].dbo.glados_ArchiveServerAlerts @Provider = 'Quiet Jobs'

---------------------------------------------------------------------------
-- Clean up.
---------------------------------------------------------------------------
drop table #Quiet
---------------------------------------------------------------------------
---------------------------------------------------------------------------
GO
