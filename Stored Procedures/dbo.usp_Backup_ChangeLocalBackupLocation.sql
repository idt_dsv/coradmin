SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE PROCEDURE [dbo].[usp_Backup_ChangeLocalBackupLocation]
   @DatabaseName varchar(50),
   @Path varchar(500)

AS

/*------------------------------------------------------------------------------
Created By: Travis Tittle
Created On: 1/7/2011

Purpose: 
	If DataServices is ever in need to backup database files to a different location,
	whether due to insufficient space in main backup directory, or grouping reasons.
	
Revisions:
	-

To Do:
	None
	
Notes:
	This Stored Procedure relies heavily on the Backup Solution Wrapper Proc.  If a 
	database is created, or old, and a change of backup location is needed, this 
	process will backup to specified location upon next backup(Simple,Full).  If this
	is a new database, yet to go through the Wrapper Proc, it builds the necessary data
	within the dbo.Backup_Paths and dbo.Backup_Databases tables to ensure the backups
	work properly.
	
	***DO NOT run this if Database Backup Job is currently running.***
	
	***Note you will need to change the @DatabaseName and @Path to the Database and
	   Destination that you choose***
------------------------------------------------------------------------------*/
------------------------------------------------------------------------------
--Runs if Wrapper Proc has already created record of Database, Backup Device, etc.
------------------------------------------------------------------------------
IF (SELECT COUNT (*) FROM Admin.dbo.Backup_Databases WHERE Databasename = @DatabaseName) > 0

   BEGIN
       
       ------------------------------------------------------------------------
       -- Insert new Backup Path into dbo.Backup_Paths
       ------------------------------------------------------------------------
       INSERT INTO ADMIN.dbo.Backup_Paths
       (Location, 
        BackupPath, 
        ServerName, 
        IsDefault, 
        IsArchived, 
        DaysToKeepBackups)
        VALUES  ('Secondary', @Path, NULL, '1', '0', '1')
   
        -----------------------------------------------------------------------
        --Update the dbo.Backup_Databases table to point database to new backup path location.
        -----------------------------------------------------------------------
 
       UPDATE dbo.Backup_Databases
       SET LocalPathID = bp.PathID
       FROM dbo.Backup_Paths AS bp WITH (NOLOCK)
       WHERE bp.Location LIKE '%Secondary%'  -- LIKE the destination in dbo.Backup_Paths
       AND DatabaseName = @DatabaseName
 
   
   END
   
   ELSE
------------------------------------------------------------------------------
--Runs if Wrapper Proc has not ran yet.  Need to create record and prepare for Wrapper Proc
--to create Backup Device
------------------------------------------------------------------------------   
   BEGIN
        
        EXEC dbo.usp_Backup_Step0_WrapperProc 'Database'
        
        INSERT INTO ADMIN.dbo.Backup_Paths
       (Location, 
        BackupPath, 
        ServerName, 
        IsDefault, 
        IsArchived, 
        DaysToKeepBackups)
        VALUES  ('Secondary', @Path, NULL, '1', '0', '1')  

        -----------------------------------------------------------------------
        --Update the dbo.Backup_Databases table to point database to new backup path location.
        -----------------------------------------------------------------------
 
        UPDATE dbo.Backup_Databases
        SET LocalPathID = bp.PathID
        FROM dbo.Backup_Paths AS bp WITH (NOLOCK)
        WHERE bp.Location LIKE '%Secondary%'  -- LIKE the destination in dbo.Backup_Paths
        AND DatabaseName = @DatabaseName


   END
 
GO
