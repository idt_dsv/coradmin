SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[Backup_MasterServiceKey]
AS
/*
3/2/2011 John Wood 
Backup SERVICE MASTER KEYs to \\nearstore\SQLBackups\Daily\MasterKeys

Revisions:
	12/08/2011 - Andrew Gould
		Moving proc to [Admin] and updating naming convention.
	12/22/2011 - John Wood
		Added time to filename. this should prvenet multirun issues.
	4/9/2014 - Cory Blythe
		Changed @PATH from nearstore to Corbackup
*/

DECLARE 
	@CMD VARCHAR(1000),
	@Path VARCHAR(100),
	@Path2 VARCHAR(100),
	@Password VARCHAR(16),
	@ServerName VARCHAR(256)

SELECT @SERVERNAME=REPLACE ( @@ServerName , '\' , '_' );
SELECT @PATH = '\\CorBackup\SQLBackups\Daily\MasterKeys\Current\'
	+@Servername
	+'_'+CAST((DATEPART(YYYY, GETDATE())*10000+DATEPART(MM,GETDATE())*100+DATEPART(DD,GetDate())) AS CHAR(8))
	+'_'+CAST(DATEPART(HH, GETDATE())*100+DATEPART(Minute,GETDATE()) AS CHAR(4))
	+'.snk'
SELECT @Password='H1tth3F4n!'



SELECT @CMD='BACKUP SERVICE MASTER KEY TO FILE = '+CHAR(39)+@PATH+CHAR(39)+' 
    ENCRYPTION BY PASSWORD = '+CHAR(39)+@password+CHAR(39)+';'
 
 --SELECT @Path,@Password,@CMD

EXECUTE (@CMD);


GO
