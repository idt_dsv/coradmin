SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[OpenTransactionMonitor]
/*
  Procedure OpenTransactionMonitor
  
  Looks for long-running transactions. Long-running is defined in dbo.OpenTranConfig per database.
  Databases are automagically added with a default alert at 30 minutes.
  
  Date        Who       What
  20110715    BGR       Initial Release
  20120120    Wood      Converted to use standard glados_ServerAlertsUpsertXML Also will alert at AlertMinutes/2
  20120125	  Wood		Adding Exception Capabilities Based on login and database.
*/
AS
SET NOCOUNT ON;
DECLARE @XML XML

/*Load Databases*/
INSERT INTO dbo.OpenTranConfig (dbname, AlertMinutes) 
SELECT sd.name, 30 
	FROM sys.databases AS sd 
	LEFT OUTER JOIN dbo.OpenTranConfig AS OTC
		ON SD.name=otc.DbName
	WHERE otc.DBname IS NULL;

EXECUTE dbo.glados_ServerAlertsMarkInactive 'OpenTransaction';

SELECT @XML=(
	SELECT
	'OpenTransaction'AS Provider
	,'Long-Running Open Transaction Alert! spid: '+CAST(ses.session_id AS NVARCHAR(128)) AS Name
	,'spid: '+CAST(ses.session_id AS NVARCHAR(128))+CHAR(10)+CHAR(13)+
	 'Database: '+Db_Name(tDt.database_id)+CHAR(10)+CHAR(13)+
	 'Login: '+CHAR(39)+ses.login_name+CHAR(39)+CHAR(10)+CHAR(13)+
	 'Host: '+CHAR(39)+ses.host_name+CHAR(39)+CHAR(10)+CHAR(13)+
	 'Program: '+CHAR(39)+ses.program_name+CHAR(39)+CHAR(10)+CHAR(13)+
	 'Duration: '+CAST(DATEDIFF(MINUTE, tat.transaction_begin_time, CURRENT_TIMESTAMP) AS VARCHAR(10))+' minutes.'
	 AS ErrorInfo
	,CURRENT_TIMESTAMP AS FailDateTime
		, 
		CASE 
		WHEN otc.AlertMinutes<= DATEDIFF(MINUTE, tat.transaction_begin_time, CURRENT_TIMESTAMP)
			THEN 'Warn'
		WHEN (otc.AlertMinutes/2) <= DATEDIFF(MINUTE, tat.transaction_begin_time, CURRENT_TIMESTAMP)
			THEN 'Info'
		END AS ErrorLevel
		,tat.transaction_begin_time
		,DATEDIFF(MINUTE, tat.transaction_begin_time, CURRENT_TIMESTAMP /*ANSI GetDate*/ ) AS duration
		,ote.ExceptionId
	FROM sys.dm_tran_active_transactions AS TAT
	INNER JOIN sys.dm_tran_database_transactions tDt 
		ON TAT.transaction_id=TDt.transaction_id
	INNER JOIN sys.dm_tran_session_transactions tst WITH(NOLOCK)
		ON tst.transaction_id = tat.transaction_id
	INNER JOIN sys.dm_exec_sessions ses WITH(NOLOCK)
		ON ses.session_id=tst.session_id
	INNER JOIN sys.dm_exec_connections con WITH(NOLOCK)
		ON ses.session_id = con.session_id
	INNER JOIN dbo.OpenTranConfig otc WITH(NOLOCK)
		ON DB_NAME(tDt.database_id) = otc.dbname
	LEFT OUTER JOIN dbo.OpenTranConfigExceptions AS OTE
		ON otc.DbName=OTE.DbName
		AND ses.login_name=OTE.LoginName
	WHERE tat.transaction_begin_time IS NOT NULL
		AND (otc.AlertMinutes/2) <= DATEDIFF(MINUTE, tat.transaction_begin_time, CURRENT_TIMESTAMP /*ANSI GetDate*/ )
		AND OTE.ExceptionId IS NULL
		AND (Db_Name(tDt.database_id)<>'TempDB' OR Db_Name(tDt.database_id)IS NULL)
	FOR XML RAW('Alert'), root('Alerts')
	)
--select @XML
EXECUTE dbo.glados_ServerAlertsUpsertXML @XML
EXECUTE dbo.glados_ArchiveServerAlerts 'OpenTransaction'



GO
