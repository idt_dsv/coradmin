SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[Glados_LongRunningJobs]	
	AS

SET NOCOUNT ON;
/*
John Wood 
7/22/2011 Trend job Run Times 
and alert if they are running 1 standard deviation longer than expected

Step 1 Collect history into Admin.dbo.IDT_Jobhistory
Step 2 Spot outliers
Step 3 Cleanup Glados
Step 4 Report to Glados

Original Limited Deploy 7/27/2011
Original Full Deploy 8/2/2011
8/22/2011 Omit Replication jobs. AND only allert on job that take more than 5 min.
Revision 3*/	

/*Collect History*/
INSERT INTO Admin.dbo.IDT_Jobhistory
	(Job_Id
	, Run_Status
	, Run_Date
	, Run_Time
	, Run_Duration
	, Start_DateTime)
SELECT 
	SJH.job_id
	,SJH.run_status
	,SJH.Run_Date
	,SJH.run_time
	,SJH.Run_Duration
	,CAST((SUBSTRING(CAST(SJH.Run_Date AS VARCHAR(20)),5,2)+'/'+SUBSTRING(CAST(SJH.Run_Date AS VARCHAR(20)),7,2)+'/'+SUBSTRING(CAST(SJH.Run_Date AS VARCHAR(20)),1,4) )+' '+
	(CASE 
		WHEN SJH.run_time<10
		THEN '00:00:0'+CAST(SJH.run_time AS CHAR(1))
		WHEN SJH.run_time<100
		THEN '00:00:'+CAST(SJH.run_time AS CHAR(2))
		WHEN SJH.run_time<1000
		THEN '00:0'+SUBSTRING(CAST(SJH.run_time AS CHAR(3)),1,1)+':'+SUBSTRING(CAST(SJH.run_time AS CHAR(3)),2,2)
		WHEN SJH.run_time<10000
		THEN '00:'+SUBSTRING(CAST(SJH.run_time AS CHAR(4)),1,2)+':'+SUBSTRING(CAST(SJH.run_time AS CHAR(4)),3,2)
		WHEN SJH.run_time<100000
		THEN '0'+SUBSTRING(CAST(SJH.run_time AS CHAR(5)),1,1)+':'+SUBSTRING(CAST(SJH.run_time AS CHAR(5)),2,2)+':'+SUBSTRING(CAST(SJH.run_time AS CHAR(5)),4,2)
		WHEN SJH.run_time<1000000
		THEN SUBSTRING(CAST(SJH.run_time AS CHAR(6)),1,2)+':'+SUBSTRING(CAST(SJH.run_time AS CHAR(6)),3,2)+':'+SUBSTRING(CAST(SJH.run_time AS CHAR(6)),5,2)
	END) AS DATETIME)
FROM msdb.dbo.sysjobhistory AS SJH WITH (NOLOCK)
LEFT OUTER JOIN Admin.dbo.IDT_Jobhistory AS IJH WITH (NOLOCK)
	ON SJH.job_id=IJH.job_id
	AND SJH.Run_Date=IJH.Run_Date
	AND SJH.run_time=IJH.run_time
LEFT OUTER JOIN MSDB.dbo.sysjobs AS SJ WITH (NOLOCK)
	ON SJ.job_id=SJH.job_id
LEFT OUTER JOIN msdb.dbo.syscategories AS CAT WITH (NOLOCK)
	ON SJ.category_id=CAT.category_id
WHERE sjh.Step_id=0 /*Outcome only*/
	AND IJH.IDT_JobhistoryID IS NULL
	AND CAT.name NOT LIKE 'REPL-%'

UPDATE Admin.dbo.IDT_Jobhistory
	SET 
		SecRun=CAST(SUBSTRING(REVERSE(REPLACE(CAST(REVERSE(CAST(RUN_Duration AS VARCHAR(8)))AS CHAR(8)),' ','0')),1,2)*360 AS INT)
				+CAST(SUBSTRING(REVERSE(REPLACE(CAST(REVERSE(CAST(RUN_Duration AS VARCHAR(8)))AS CHAR(8)),' ','0')),3,2)*60 /*CONVERT MINUTES TO SEC 60*/AS INT)
				+CAST(SUBSTRING(REVERSE(REPLACE(CAST(REVERSE(CAST(RUN_Duration AS VARCHAR(8)))AS CHAR(8)),' ','0')),5,2) AS INT)
WHERE End_DateTime IS NULL
	AND SecRun IS NULL;


UPDATE Admin.dbo.IDT_Jobhistory
	SET End_DateTime=DATEADD(SECOND,SecRun,Start_DateTime)
WHERE End_DateTime IS NULL;


/*SPOT OUTLIERS*/
SELECT 
	Sj.name AS [Name]
	,'Jobs Running Long' As [Provider]
	,CHAR(39)+Sj.name+CHAR(39)
		+' Has Been Running Since '
		+CHAR(13)
		+CONVERT(VARCHAR(30), sja.START_Execution_date, 120)
		+' Expected\Current Runtime ='
		+CAST(AVG(IJH.Secrun)
		+STDEVP(IJH.Secrun) AS VARCHAR(20))
		+'\'+CAST(DATEDIFF(Second,sja.START_Execution_date,GETDATE())AS VARCHAR(10))  AS [AlertInfo]
	,CASE
		WHEN DATEDIFF(Second,sja.START_Execution_date,GETDATE())<3
			THEN 'GOOD' /*IF It takes less than a second We Don't care*/
		WHEN DATEDIFF(Second,sja.START_Execution_date,GETDATE())>(AVG(IJH.Secrun)+(STDEVP(IJH.Secrun)*3))
			THEN 'ERR' /*GETDATE())>(AVG(IJH.Secrun)+(STDEVP(IJH.Secrun)*3) IF >= 0 Second job forever in error*/
		WHEN DATEDIFF(Second,sja.START_Execution_date,GETDATE())>(AVG(IJH.Secrun)+STDEVP(IJH.Secrun))
			THEN 'WARN'
		ELSE 'GOOD'
	END AS [Status]
	,IJH.SecRun AS SecRun
INTO #FEEDER
FROM MSDB.dbo.sysjobactivity  AS SJA WITH (NOLOCK)
INNER JOIN MSDB.dbo.sysjobs AS sj WITH (NOLOCK)
	ON sja.job_id=sj.job_ID
INNER JOIN msdb.dbo.syscategories AS CAT WITH (NOLOCK)
	ON SJ.Category_ID=cat.Category_ID
LEFT OUTER JOIN Admin.dbo.IDT_Jobhistory AS IJH WITH (NOLOCK)
	ON SJA.Job_id=IJH.Job_id
WHERE 
	sja.stop_execution_date IS NULL
	AND sja.start_execution_date IS NOT NULL
	AND IJH.Run_Status=1 /*Only look at good runs for times to include.*/
	AND sja.start_execution_date>=(SELECT crdate FROM MASTER.sys.sysdatabases WHERE NAME='tempdb') /*It can't be running while the server reboots*/
	AND sj.name<>'DBAW30 - Glados - Monitor Job RunTimes' /*Monitoring itself is not a good idea.*/
GROUP BY 
	Sj.name
	,sja.START_Execution_date 
	,CAT.name 
	,IJH.SecRun

/*Clean up Glados*/
/*NOT using upsert to avoid loop*/
DELETE FROM ADMIN.[dbo].[glados_ServerAlerts]
	WHERE Provider='Jobs Running Long'
	AND IsCurrent = 0;

DELETE FROM Admin.dbo.IDT_Jobhistory
	WHERE Run_Date< CAST(CONVERT(VARCHAR(20), GETDATE()-30 ,112 )AS INTEGER);

/*Step 4 Report to Glados*/
UPDATE ADMIN.[dbo].[glados_ServerAlerts]
	SET IsCurrent=0 
WHERE Provider='Jobs Running Long';

UPDATE G
	SET IsCurrent = 1
		,ErrorInfo = T.AlertInfo
		,DateUpdated = GETDATE()		
--select * 
FROM ADMIN.[dbo].[glados_ServerAlerts] AS G
INNER JOIN #Feeder AS T
	ON G.[Provider]=T.[Provider]
		AND G.Name=T.Name
AND T.Status<>'GOOD' /*don't care about the jobs behaving as expected*/
;
DELETE FROM ADMIN.[dbo].[glados_ServerAlerts]
	WHERE Provider='Jobs Running Long'
	AND IsCurrent=0;

INSERT INTO ADMIN.[dbo].[glados_ServerAlerts]
	(
	NAME,
	PROVIDER,
	ErrorInfo,
	ErrorLevel,
	FirstFailure,
	IsCurrent
	)
SELECT 
	LEFT(T.Name,128),
	LEFT(T.PROVIDER,128),
	T.AlertInfo,
	LEFT(T.Status,4),
	GETDATE(),
	1
FROM ADMIN.[dbo].[glados_ServerAlerts] AS G
RIGHT OUTER JOIN #Feeder AS T
	ON G.[Provider]=T.[Provider]
	AND G.Name=T.Name
WHERE G.Provider IS NULL
AND Status<>'GOOD'
AND T.SecRun>=300;

--DROP TABLE #Feeder

RETURN 






GO
