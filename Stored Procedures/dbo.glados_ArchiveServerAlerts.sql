SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [dbo].[glados_ArchiveServerAlerts] 
	@Provider nvarchar(128)
as
begin

declare @RetentionDays integer
set @RetentionDays = 30

	delete dbo.glados_ServerAlerts
	output 
		deleted.ServerAlertID, 		
		deleted.provider,
		deleted.Name,
		deleted.FirstFailure,
		deleted.LastFailure,
		deleted.ErrorInfo,
		deleted.ErrorLevel,
		deleted.IsCurrent,
		deleted.DateCreated,
		deleted.DateUpdated
	into dbo.glados_ServerAlertsArchive
	from dbo.glados_ServerAlerts al
	where al.IsCurrent = 0
	and al.Provider=@Provider
	and al.DateUpdated between dateadd(dd, -1 * @RetentionDays, current_timestamp) and current_timestamp;
end



GO
GRANT EXECUTE ON  [dbo].[glados_ArchiveServerAlerts] TO [IDT-CORALVILLE\srv_PRODSQL3]
GO
