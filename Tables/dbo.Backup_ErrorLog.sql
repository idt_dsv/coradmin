CREATE TABLE [dbo].[Backup_ErrorLog]
(
[ErrorLogID] [int] NOT NULL IDENTITY(1, 1),
[Command] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Error] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorDate] [datetime] NULL CONSTRAINT [DF__Backup_Er__Error__24927208] DEFAULT (getdate()),
[Resolved] [bit] NULL CONSTRAINT [DF__Backup_Er__Resol__25869641] DEFAULT ((0)),
[del_marker] [bit] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Backup_ErrorLog] ADD CONSTRAINT [PK__Backup_E__D65247E222AA2996] PRIMARY KEY CLUSTERED  ([ErrorLogID]) ON [PRIMARY]
GO
