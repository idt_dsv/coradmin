CREATE TABLE [dbo].[OpenTranConfigExceptions]
(
[ExceptionId] [int] NOT NULL IDENTITY(1, 1),
[DbName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LoginName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpenTranConfigExceptions] ADD CONSTRAINT [PK__OpenTran__26981D887C1A6C5A] PRIMARY KEY CLUSTERED  ([ExceptionId]) ON [PRIMARY]
GO
