CREATE TABLE [dbo].[Script_Paths]
(
[ScriptPathID] [int] NOT NULL IDENTITY(1, 1),
[Location] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ScriptPath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Script_Paths] ADD CONSTRAINT [PK__Script_P__6DD9D41F5EBF139D] PRIMARY KEY CLUSTERED  ([ScriptPathID]) ON [PRIMARY]
GO
