CREATE TABLE [dbo].[glados_ServerAlertsArchive]
(
[ServerAlertArchiveID] [int] NOT NULL IDENTITY(1, 1),
[ServerAlertID] [int] NULL,
[Provider] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Name] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstFailure] [datetime] NULL,
[LastFailure] [datetime] NULL,
[ErrorInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorLevel] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsCurrent] [bit] NULL,
[DateCreated] [datetime] NULL,
[DateUpdated] [datetime] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[glados_ServerAlertsArchive] ADD CONSTRAINT [pk_glados_ServerAlertsArchive] PRIMARY KEY CLUSTERED  ([ServerAlertArchiveID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[glados_ServerAlertsArchive] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT SELECT ON  [dbo].[glados_ServerAlertsArchive] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT INSERT ON  [dbo].[glados_ServerAlertsArchive] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT DELETE ON  [dbo].[glados_ServerAlertsArchive] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT UPDATE ON  [dbo].[glados_ServerAlertsArchive] TO [IDT-CORALVILLE\srv_PRODSQL3]
GO
