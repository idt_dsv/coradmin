CREATE TABLE [dbo].[IDT_Jobhistory]
(
[IDT_JobhistoryID] [int] NOT NULL IDENTITY(1, 1),
[Job_Id] [uniqueidentifier] NOT NULL,
[Run_Status] [int] NULL,
[Run_Date] [int] NULL,
[Run_Time] [int] NULL,
[Run_Duration] [int] NULL,
[Start_DateTime] [datetime] NULL,
[End_DateTime] [datetime] NULL,
[SecRun] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IDT_Jobhistory] ADD CONSTRAINT [PK__IDT_Jobh__EA8450D814270015] PRIMARY KEY CLUSTERED  ([IDT_JobhistoryID]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IDT_Jobhistory_Job_Id] ON [dbo].[IDT_Jobhistory] ([Job_Id]) INCLUDE ([SecRun]) ON [PRIMARY]
GO
