CREATE TABLE [dbo].[DatabaseLog]
(
[DatabaseLogID] [int] NOT NULL IDENTITY(1, 1),
[PostTime] [datetime] NULL,
[DatabaseUser] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HostName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ApplicationName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Event] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Schema] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Object] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TSQL] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[XmlEvent] [xml] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[DatabaseLog] ADD CONSTRAINT [PK__Database__F06F75837A672E12] PRIMARY KEY CLUSTERED  ([DatabaseLogID]) ON [PRIMARY]
GO
