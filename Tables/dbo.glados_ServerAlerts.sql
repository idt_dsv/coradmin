CREATE TABLE [dbo].[glados_ServerAlerts]
(
[ServerAlertID] [int] NOT NULL IDENTITY(1, 1),
[Provider] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Name] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[FirstFailure] [datetime] NOT NULL CONSTRAINT [DF__glados_Se__First__0C85DE4D] DEFAULT (getdate()),
[LastFailure] [datetime] NOT NULL CONSTRAINT [DF__glados_Se__LastF__0D7A0286] DEFAULT (getdate()),
[ErrorInfo] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorLevel] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__glados_Se__Error__0E6E26BF] DEFAULT ('INFO'),
[IsCurrent] [bit] NOT NULL CONSTRAINT [DF__glados_Se__IsCur__0F624AF8] DEFAULT ((1)),
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF__glados_Se__DateC__10566F31] DEFAULT (getdate()),
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF__glados_Se__DateU__114A936A] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[glados_ServerAlerts] ADD CONSTRAINT [pk_glados_ServerAlerts] PRIMARY KEY CLUSTERED  ([ServerAlertID]) ON [PRIMARY]
GO
GRANT VIEW DEFINITION ON  [dbo].[glados_ServerAlerts] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT SELECT ON  [dbo].[glados_ServerAlerts] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT INSERT ON  [dbo].[glados_ServerAlerts] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT DELETE ON  [dbo].[glados_ServerAlerts] TO [IDT-CORALVILLE\srv_PRODSQL3]
GRANT UPDATE ON  [dbo].[glados_ServerAlerts] TO [IDT-CORALVILLE\srv_PRODSQL3]
GO
