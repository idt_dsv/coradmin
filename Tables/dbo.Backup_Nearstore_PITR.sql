CREATE TABLE [dbo].[Backup_Nearstore_PITR]
(
[DatabaseID] [int] NOT NULL IDENTITY(1, 1),
[DatabaseName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecoveryModel] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MostRecentFull] [datetime] NULL CONSTRAINT [DF__Backup_Ne__MostR__73852659] DEFAULT ('1900-01-01'),
[MostRecentDiff] [datetime] NULL CONSTRAINT [DF__Backup_Ne__MostR__74794A92] DEFAULT ('1900-01-01'),
[MostRecentLog] [datetime] NULL CONSTRAINT [DF__Backup_Ne__MostR__756D6ECB] DEFAULT ('1900-01-01'),
[PointInTimeRecovery] [datetime] NULL,
[DontBackup] [int] NULL CONSTRAINT [DF__Backup_Ne__DontB__7849DB76] DEFAULT ((0)),
[DateObtained] [datetime] NULL CONSTRAINT [DF__Backup_Ne__DateO__793DFFAF] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Backup_Nearstore_PITR] ADD CONSTRAINT [PK__Backup_N__2C9BE40F719CDDE7] PRIMARY KEY CLUSTERED  ([DatabaseID]) ON [PRIMARY]
GO
