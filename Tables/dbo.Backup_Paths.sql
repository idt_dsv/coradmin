CREATE TABLE [dbo].[Backup_Paths]
(
[PathID] [int] NOT NULL IDENTITY(1, 1),
[Location] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BackupPath] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ServerName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsDefault] [bit] NULL CONSTRAINT [DF__Backup_Pa__IsDef__6A30C649] DEFAULT ((0)),
[IsArchived] [bit] NULL CONSTRAINT [DF__Backup_Pa__IsArc__6B24EA82] DEFAULT ((0)),
[DaysToKeepBackups] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Backup_Paths] ADD CONSTRAINT [PK__Backup_P__CD67DC3968487DD7] PRIMARY KEY CLUSTERED  ([PathID]) ON [PRIMARY]
GO
