CREATE TABLE [dbo].[ConnectionTracking_Log]
(
[ConnectionID] [int] NOT NULL IDENTITY(1, 1),
[DatabaseName] [sys].[sysname] NOT NULL,
[ComputerName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MACAddress] [varchar] (12) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ProgramName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoginName] [varchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FirstConnection] [datetime] NULL,
[LastConnection] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ConnectionTracking_Log] ADD CONSTRAINT [PK__Connecti__404A64F315DA3E5D] PRIMARY KEY CLUSTERED  ([ConnectionID]) ON [PRIMARY]
GO
