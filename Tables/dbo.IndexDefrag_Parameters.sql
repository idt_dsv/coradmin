CREATE TABLE [dbo].[IndexDefrag_Parameters]
(
[ParameterID] [int] NOT NULL IDENTITY(1, 1),
[ParameterSetName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludedDatabases] [varchar] (2500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludedObjects] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExcludedIndexes] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MinFragmentation] [smallint] NULL CONSTRAINT [DF__IndexDefr__MinFr__440B1D61] DEFAULT ((5)),
[MinPages] [smallint] NULL CONSTRAINT [DF__IndexDefr__MinPa__44FF419A] DEFAULT ((128)),
[ImprovementThreshold] [smallint] NULL CONSTRAINT [DF__IndexDefr__Impro__45F365D3] DEFAULT ((5)),
[RebuildThreshold] [smallint] NULL CONSTRAINT [DF__IndexDefr__Rebui__46E78A0C] DEFAULT ((30)),
[TimeLimit] [int] NULL CONSTRAINT [DF__IndexDefr__TimeL__47DBAE45] DEFAULT ((240)),
[Delay] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__IndexDefr__Delay__48CFD27E] DEFAULT ('00:00:05'),
[ExecuteDays] [varchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ExecuteTime] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LastLoadDate] [datetime] NULL CONSTRAINT [DF__IndexDefr__LastL__49C3F6B7] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[IndexDefrag_Parameters] ADD CONSTRAINT [PK__IndexDef__F80C62974222D4EF] PRIMARY KEY CLUSTERED  ([ParameterID]) ON [PRIMARY]
GO
