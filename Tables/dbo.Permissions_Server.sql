CREATE TABLE [dbo].[Permissions_Server]
(
[DisplayOrder] [int] NULL,
[PermissionType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalSID] [varbinary] (85) NULL,
[Command] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoadDate] [datetime] NULL CONSTRAINT [DF__Permissio__LoadD__38996AB5] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
