CREATE TABLE [dbo].[Backup_NoBackup_Exclusions]
(
[Databaseid] [int] NOT NULL IDENTITY(1, 1),
[DatabaseName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Backup_NoBackup_Exclusions] ADD CONSTRAINT [PK__Backup_N__2C9AE81728ED12D1] PRIMARY KEY CLUSTERED  ([Databaseid]) ON [PRIMARY]
GO
