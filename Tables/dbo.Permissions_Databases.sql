CREATE TABLE [dbo].[Permissions_Databases]
(
[DisplayOrder] [int] NULL,
[DatabaseName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PermissionType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalSID] [varbinary] (85) NULL,
[Command] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoadDate] [datetime] NULL CONSTRAINT [DF__Permissio__LoadD__3A81B327] DEFAULT (getdate())
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_PrincipalSID] ON [dbo].[Permissions_Databases] ([PrincipalSID]) ON [PRIMARY]
GO
