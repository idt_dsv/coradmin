CREATE TABLE [dbo].[Permissions_WindowsLogins]
(
[GroupName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalSID] [varbinary] (85) NULL,
[ActiveDirectorySDDL] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoadDate] [datetime] NULL CONSTRAINT [DF__Permissio__LoadD__5AEE82B9] DEFAULT (getdate())
) ON [PRIMARY]
GO
