CREATE TABLE [dbo].[glados_AlertMessages]
(
[AlertMessageID] [int] NOT NULL IDENTITY(1, 1),
[Prefix] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlertMessage] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ErrorLevel] [char] (4) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[glados_AlertMessages] ADD CONSTRAINT [pk_glados_AlertMessages] PRIMARY KEY CLUSTERED  ([AlertMessageID]) ON [PRIMARY]
GO
