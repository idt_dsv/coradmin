CREATE TABLE [dbo].[Backup_ExclusionList]
(
[ExcludedID] [int] NOT NULL IDENTITY(1, 1),
[DatabaseName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ExcludedBy] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [DF__Backup_Ex__Exclu__47A6A41B] DEFAULT (suser_sname()),
[ExcludedOn] [datetime] NOT NULL CONSTRAINT [DF__Backup_Ex__Exclu__489AC854] DEFAULT (getdate()),
[Reason] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
