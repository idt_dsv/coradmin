CREATE TABLE [dbo].[Permissions_ActiveDirectoryGroupMembership]
(
[GroupName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[PrincipalName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FullName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ActiveDirectorySDDL] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LoadDate] [datetime] NULL CONSTRAINT [DF__Permissio__LoadD__59063A47] DEFAULT (getdate())
) ON [PRIMARY]
GO
