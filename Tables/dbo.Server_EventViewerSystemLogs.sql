CREATE TABLE [dbo].[Server_EventViewerSystemLogs]
(
[MachineName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[EntryType] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TimeWritten] [datetime] NULL,
[Source] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UserName] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Message] [varchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
