CREATE TABLE [dbo].[OpenTranConfig]
(
[otdbid] [int] NOT NULL IDENTITY(1, 1),
[DbName] [nvarchar] (128) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[AlertMinutes] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[OpenTranConfig] ADD CONSTRAINT [pk_OpenTranConfig] PRIMARY KEY CLUSTERED  ([otdbid]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [ncix_opentranconfig_dbname] ON [dbo].[OpenTranConfig] ([DbName]) INCLUDE ([AlertMinutes]) ON [PRIMARY]
GO
