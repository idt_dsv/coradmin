CREATE TABLE [dbo].[Backup_Databases]
(
[DatabaseID] [int] NOT NULL IDENTITY(1, 1),
[LocalPathID] [int] NULL,
[RemotePathID] [int] NULL,
[DatabaseName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecoveryModel] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DontBackup] [bit] NULL CONSTRAINT [DF__Backup_Da__DontB__37703C52] DEFAULT ((0)),
[FolderName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[BackupFile] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Backup_Da__Backu__3864608B] DEFAULT ((-1)),
[LastLogBackupFile] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Backup_Da__LastL__395884C4] DEFAULT ((-1)),
[LastBackupDate] [datetime] NULL CONSTRAINT [DF__Backup_Da__LastB__3A4CA8FD] DEFAULT ('1900-01-01'),
[LastLogBackupDate] [datetime] NULL CONSTRAINT [DF__Backup_Da__LastL__3B40CD36] DEFAULT ('1900-01-01'),
[BACKUP_DESCRIPTION] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Database_Status] [varchar] (25) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IsLogShipped] [bit] NULL CONSTRAINT [DF__Backup_Da__IsLog__3C34F16F] DEFAULT ((0)),
[LastDiffBackupFile] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NULL CONSTRAINT [DF__Backup_Da__LastD__3D2915A8] DEFAULT ((-1)),
[LastDiffBackupDate] [datetime] NULL CONSTRAINT [DF__Backup_Da__LastD__3E1D39E1] DEFAULT ('1900-01-01'),
[Database_Create_Date] [datetime] NULL,
[ReadOnlyPathID] [int] NULL,
[ReadOnlyBackupFile] [varchar] (300) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT [ROBackupFile] DEFAULT ('-1'),
[ReadOnlyBackupDate] [datetime] NOT NULL CONSTRAINT [ROBackupDate] DEFAULT ('1900-01-01'),
[BackupGroup] [int] NOT NULL CONSTRAINT [BackupGroupValue] DEFAULT ((1)),
[NeedsFull] [bit] NULL CONSTRAINT [DF__Backup_Da__Needs__04AFB25B] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Backup_Databases] ADD CONSTRAINT [PK__Backup_D__2C9BE40F208CD6FA] PRIMARY KEY CLUSTERED  ([DatabaseID]) ON [PRIMARY]
GO
