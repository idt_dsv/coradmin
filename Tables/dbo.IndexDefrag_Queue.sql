CREATE TABLE [dbo].[IndexDefrag_Queue]
(
[QueueID] [int] NOT NULL IDENTITY(1, 1),
[DatabaseID] [int] NULL,
[DatabaseName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SchemaID] [int] NULL,
[SchemaName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectID] [int] NULL,
[ObjectName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexID] [int] NULL,
[IndexName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommandType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefragCommand] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyCommand] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateStatsCommand] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Fragmentation] [float] NULL,
[Pages] [int] NULL,
[FragmentationCount] [int] NULL,
[LastLoadDate] [datetime] NULL CONSTRAINT [DF__IndexDefr__LastL__3F466844] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IndexDefrag_Queue] ADD CONSTRAINT [PK__IndexDef__8324E8F53D5E1FD2] PRIMARY KEY CLUSTERED  ([QueueID]) ON [PRIMARY]
GO
