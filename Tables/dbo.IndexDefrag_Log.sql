CREATE TABLE [dbo].[IndexDefrag_Log]
(
[LogID] [int] NOT NULL IDENTITY(1, 1),
[DatabaseID] [int] NULL,
[DatabaseName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[SchemaID] [int] NULL,
[SchemaName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ObjectID] [int] NULL,
[ObjectName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[IndexID] [int] NULL,
[IndexName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CommandType] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefragCommand] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[VerifyCommand] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[UpdateStatsCommand] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DefragStartTime] [datetime] NULL,
[DefragEndTime] [datetime] NULL,
[StatsStartTime] [datetime] NULL,
[StatsEndTime] [datetime] NULL,
[Pages] [int] NULL,
[PreFragmentation] [float] NULL,
[PostFragmentation] [float] NULL,
[LastLoadDate] [datetime] NULL CONSTRAINT [DF__IndexDefr__LastL__4E88ABD4] DEFAULT (getdate())
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[IndexDefrag_Log] ADD CONSTRAINT [PK__IndexDef__5E5499A84CA06362] PRIMARY KEY CLUSTERED  ([LogID]) ON [PRIMARY]
GO
