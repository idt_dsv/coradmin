CREATE TABLE [dbo].[Audit_RemovalLog]
(
[AuditRemovalID] [int] NOT NULL IDENTITY(1, 1),
[DatabaseName] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[RecordsRemoved] [int] NULL,
[RemovalDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Audit_RemovalLog] ADD CONSTRAINT [PK__Audit_Re__0A90C2FA11158940] PRIMARY KEY CLUSTERED  ([AuditRemovalID]) ON [PRIMARY]
GO
