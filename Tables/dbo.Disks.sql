CREATE TABLE [dbo].[Disks]
(
[DiskID] [int] NOT NULL IDENTITY(1, 1),
[DriveLetter] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[SizeMB] [int] NOT NULL,
[DateCreated] [datetime] NOT NULL CONSTRAINT [DF__Disks__DateCreat__4F47C5E3] DEFAULT (getdate()),
[DateUpdated] [datetime] NOT NULL CONSTRAINT [DF__Disks__DateUpdat__503BEA1C] DEFAULT (getdate()),
[MonitorType] [char] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[WarnPct] [int] NULL,
[ErrPct] [int] NULL,
[WarnMbFree] [int] NULL,
[ErrMbFree] [int] NULL,
[Label] [nvarchar] (32) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Disks] ADD CONSTRAINT [PK__Disks__1AC118BD4D5F7D71] PRIMARY KEY CLUSTERED  ([DiskID]) ON [PRIMARY]
GO
