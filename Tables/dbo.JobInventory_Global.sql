CREATE TABLE [dbo].[JobInventory_Global]
(
[InventoryID] [int] NOT NULL IDENTITY(1, 1),
[JobName] [varchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Description] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MSX_TSX] [bit] NULL,
[Status] [bit] NULL,
[FutureSchedule] [bit] NOT NULL CONSTRAINT [DF__JobInvent__Futur__2BC97F7C] DEFAULT ((1)),
[Ignore] [bit] NOT NULL CONSTRAINT [DF__JobInvent__Ignor__2CBDA3B5] DEFAULT ((0)),
[Ignore_Description] [varchar] (1000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[JobInventory_Global] ADD CONSTRAINT [PK__JobInven__F5FDE6D32334397B] PRIMARY KEY CLUSTERED  ([InventoryID]) ON [PRIMARY]
GO
