IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'DBA_Reporting')
CREATE LOGIN [DBA_Reporting] WITH PASSWORD = 'p@ssw0rd'
GO
CREATE USER [DBA_Reporting] FOR LOGIN [DBA_Reporting] WITH DEFAULT_SCHEMA=[DBA_Reporting]
GO
