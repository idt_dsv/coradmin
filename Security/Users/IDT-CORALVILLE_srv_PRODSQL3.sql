IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'IDT-CORALVILLE\srv_PRODSQL3')
CREATE LOGIN [IDT-CORALVILLE\srv_PRODSQL3] FROM WINDOWS
GO
CREATE USER [IDT-CORALVILLE\srv_PRODSQL3] FOR LOGIN [IDT-CORALVILLE\srv_PRODSQL3]
GO
