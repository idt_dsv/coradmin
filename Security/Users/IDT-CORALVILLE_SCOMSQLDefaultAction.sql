IF NOT EXISTS (SELECT * FROM master.dbo.syslogins WHERE loginname = N'IDT-CORALVILLE\SCOMSQLDefaultAction')
CREATE LOGIN [IDT-CORALVILLE\SCOMSQLDefaultAction] FROM WINDOWS
GO
CREATE USER [IDT-CORALVILLE\SCOMSQLDefaultAction] FOR LOGIN [IDT-CORALVILLE\SCOMSQLDefaultAction]
GO
